import { createStore, applyMiddleware} from 'redux'
import compose from 'recompose/compose'
import rootReducer from './reducers'
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'

const initialState = {}

export const history = createBrowserHistory()
const middleware = [routerMiddleware(history), thunk]

const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middleware),
      window.__REDUX_DEVTOOLS_EXTENSION__
        ? window.__REDUX_DEVTOOLS_EXTENSION__()
        : f => f
    )
  );
  
  export default store;