
import setAuthToken from '../../utils/setAuthToken'
import jwt_decode from 'jwt-decode'
import http from './http'

import {
    GET_ERRORS,
    SET_CURRENT_USER,
    USER_LOADING,
    SET_CURRENT_DRAWER,
    SET_CURRENT_TOOLBAR
} from "./types";

export const loginUser = userData => dispatch => {
    http.post("/api/security/signin", userData)
        .then((result) => {

            const { token } = result.data
            localStorage.setItem("jwtToken", token);
            setAuthToken(token);
            const decoded = jwt_decode(token);
            dispatch(setCurrentUser(decoded));

        }).catch((err) => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        });
}

// Set logged in user
export const setCurrentUser = decoded => {
    console.log("OUTPUT: decoded", decoded)
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    };
};

// User loading
export const setUserLoading = () => {
    return {
        type: USER_LOADING
    };
};

// Log user out
export const logoutUser = (userData) => dispatch => {

    http.post("/api/security/signup", userData)
        .then((result) => {
            if (result.status === 200) {
                // Remove token from local storage
                localStorage.removeItem("jwtToken");
                // Remove auth header for future requests
                setAuthToken(false);
                // Set current user to empty object {} which will set isAuthenticated to false
                dispatch(setCurrentUser({}));
            }
        }).catch((err) => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        })
};


export const setToolbar = dataToolbar => dispatch => {
    dispatch({
        type: SET_CURRENT_TOOLBAR,
        toolbar: dataToolbar
    })
}