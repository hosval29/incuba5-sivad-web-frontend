import {
    SET_CURRENT_TOOLBAR,
} from "../actions/types";

const initialState = {
    title: 'DashBoard',
    icon: 'DashBoardIcon'
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_CURRENT_TOOLBAR:
            return {
                ...state,
                title: action.toolbar.title,
                icon: action.toolbar.icon
            }
        default:
            return state;
    }
}