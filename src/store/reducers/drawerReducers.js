import {
    SET_CURRENT_DRAWER,
} from "../actions/types";

const open = true

export default function (state = open, action) {
    switch (action.type) {
        case SET_CURRENT_DRAWER:
            return action.open;
        default:
            return state;
    }
}