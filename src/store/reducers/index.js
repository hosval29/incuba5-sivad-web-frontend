import { combineReducers } from "redux";
import authReducer from "./authReducers";
import errorReducer from "./errorReducers";
import drawerReducer from "./drawerReducers"
import toolbarReducer from "./toolbarReducer"

export default combineReducers({
    auth: authReducer,
    errors: errorReducer,
    open: drawerReducer,
    toolbar: toolbarReducer
});