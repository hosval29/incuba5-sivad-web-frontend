//Modules
export const DASHBOARD = "DashBoard";
export const REPORTES = "Reportes";
export const LISTINSPECCIONES = "Listado Inspecciones";
export const GESTION_VEHICULOS = "Gestión Vehículos";
export const SEGURIDAD = "Seguridad";
export const OPERACIONAL = "Operacional";
export const SISTEMA = "Sistema";
export const ALERTREPORTS = "Notificaciones"

//Items Module Seguridad
export const GESTION_USER = "Gestión de Usuarios"
export const HISTORYLOG = "Histórico Inicio de Sesión"
export const USER = "Usuario"
export const USERS = "Usuarios"
export const MESSAGEDELETEROLE = "Está seguro que desea eliminar este Rol para este usuario?. Tenga en cuenta que el usuario ya no participara en la plataforma de la cual tiene acceso."

//Items Module Operational
export const STATESVEHICLES = "Estados Vehículos"
export const STATEVEHICLE = "Estado Vehículo"
export const TYPESCONTRATO = "Tipos Contrato"
export const TYPECONTRATO = "Tipo Contrato"
export const CATEGORYSLICCONDUCT = "Categorias Licencia Conducción"
export const CATEGORYLICCONDUCT = "Categoria Lic. Con."
export const TYPESINSPECTIONS = "Tipos Inspección"
export const TYPEINSPECTION = "Tipo Inspección Vehícular"
export const TYPESALERTS = "Tipos de Alertas"
export const TYPEALERT = "Tipo Alerta"
export const TYPESIDENTIFICATIONS = "Tipos Identificación"
export const TYPEIDENTIFICATION = "Tipo Idenficación"

//Items Module Sistema
export const REGIONS = "Regiones"
export const REGION = "Región"
export const CITIES = "Ciudades"
export const CITY = "Ciudad"
export const PARAMETERS = "Parametros"
export const PARAMETER = "Parametro"

//Items Modules Gestion Vehicles
export const VEHICLES = "Vehículos"
export const VEHICLE = "Vehículo"
export const LISTOWNERS = "Listado de Propietarios"
export const LISTDRIVERS = "Litado de Conductores"
export const OWNERS = "Propietarios"
export const OWNER = "Propietario"
export const DRIVERS = "Conductores"
export const DRIVER = "Conductor"
export const CREATEVEHICLE = "Crear Vehículo"
export const DATAVEHICLE = "Datos del Vehículo"
export const ALERTS = "Configuración de Alertas"
export const LEYENDOWNERVEHICLE = "Seleccione un Propietario si este vehículo pertenece a un Propietario del sistema. Si el vehículo pertenece a la empresa, seleccione el propietario Empresa (Por defecto).";
export const DETAILDATADRIVER = "Detalle Conductor"

//String Varios
export const ROLES = "Roles";
export const ROLE = "Rol";
export const PROFILES = "Perfiles"
export const PROFILE = "Perfil"
export const MODULES = "Módulos"
export const MODULE = "Módulo"
export const ITEMMODULE = "Item Módulo"
export const ITEMSMODULES = "Items Módulos"
export const GESTION = "Gestión"
export const STATUSINACTIVO = "Inactivo"
export const STATUSACTIVO = "Activo";
export const GESTIONBUTTON = [
    "Editar", "Eliminar", "Activar"
]
export const GESTIONBUTTONVEHICLE = [
    "Editar", "Cambiar Estado", "Inhabilitar Inspección", "Habilitar Inspección"
]
export const GESTIONBUTTONINSPECTION = [
    "Número Manifiesto"
]
export const CAMPOSOBLIGATORIOS = "(*) campos obligatorios"
export const MESSAGENODATA = "Lo sentimos, no se encontraron registros coincidentes"
export const DATABASIC = "Datos Básicos"
export const DATAOPERATIVE = "Datos Operacional"
export const DATALABORAL = "Datos Laboral"
export const CLOSEDIALOG = "Cerrar Dialog"
export const CLOSEDIALOGINSPECTIONENABLED = "Cerrar Dialog Inspection Enabled"

//Strins Form User
export const NAME = "Nombre"
export const APELLIDO = "Apellido"
export const EMAIL = "Correo"
export const PHONE = "Teléfono"
export const PASS = "Contraseña"
export const PASSOLD = "Contraseña Vieja"
export const PASSNEW = "Contraseña Nueva"
export const PASSCONFIRM = "Confirmar Contraseña"

//String Crud
export const CRUDADD = 'Agregar'
export const CRUDDELETE = 'Inactivar'
export const CRUDCONVERTUSER = 'Convertir Usuario'
export const CRUDACTIVAR = 'Activar'
export const CRUDERROR = "Error"
export const CRUDALERTNOACTIVE = "Opción Invalida"
export const MESSAGEOPTIONINVALID = "Esta opción no esta habilitada."
export const CRUDUPDATE = 'Actualizar'
export const CRUDREAD = 'Cargados'
export const CHANGESTATUS = "Cambiar Estado"
export const MESSAGECHANGESTATUS = "Esta seguro que deea cambiar el estado de este"
export const MESSAGEDELETE = 'Está seguro/a en eliminar este Item?, esto puedo generar conflictos en la operación del sistema. Sin embargo, Recuerde que ningún registro será eliminado de la plataforma, sino que entra en un estado de inhactivación.'
export const MESSAGEACTIVAR = 'Está seguro que desea activar de nuevo este '
export const MESSAGECONTINUAR = '¡Aceptar para continuar!.'
//String Button
export const BUTTONADD = 'Agregar'
export const BUTTONSAVE = "Guardar"
export const BUTTONACEPT = "Aceptar"
export const BUTTONOK = "OK"

//String Module Inspection
export const DETAILINSPECTION = "Detalle Inspección"
export const ID = 'ID'
export const DATE = "Fecha Realización"
export const USERGESTOR = "Usuario Gestor"
export const PLATEVEHICLE = "Placa Vehículo"
export const NOVELTIES = "Novedades"
export const NUMBERMANIFEST = "Número Manifiesto"
export const CLOSEDIALOGDETAILINSPECTION = "Detalle Inspeccion"
export const CLOSEDIALOGNUMBERMANIFEST = "Numero Manifesto"
export const CLOSEDIALOGNOVELTIESINSPECTION = "Novedade Inspecciones"

//String Module Drivers
export const CONVERTDRIVERUSER = "Conductor a Usuario"
export const MESSAGECONVERTUSER = "Estas en el proceso de convertir un conductor a un usuario de la plataforma. Ese Usuario se creara con los siguientes datos, sólo debes agregar su Contraseña."
export const MESSAGEEXISTSCONVERTUSER = "Este Conductor ya pertenece a un Usuario de la plataforma."
export const INPUTSFORMDRIVER = {
    homeAddres: "Dirección Casa",
    homePhone: "Teléfono Casa",
    cellPhoneNumber: "Número Celular",
    email: "Correo",
    placeBirth: "Lugar de Nacimiento",
    dateBirth: "Fecha de Nacimiento",
    acedemicTraining: "Formación Academica",
    typeRH: "Tipo RH",
    numLicencia: "Numero Licencia de Conducción",
    categoryLicCon: "Categoria Licencia de Conducción",
    dateExpirationLicCond: "Fecha Expiracion Licencia de Conduccción",
    typeContract: "Tipo de Contrato",
    dateInitContract: "Fecha Inicio de Contrato",
    dateEndContract: "Fecha Fin de Contrato",
    eps: "EPS",
    fondoPensiones: "Fondo de Pensiones",
    fondoCesantias: "Fondo de Cesantias",
    arl: "ARL"
}

//String Module Vehicle
export const CITYREGISTRATION = "Ciudad de Matrícula"
export const DETAILDATAVEHICLE = "Detalle Vehículo"
export const INSPECTIONENABLED = "Habilitar"
export const INSPECTIONDIABLED = "Inhabilitar"
export const TITLEINSPECTION = "Inspección"
export const LINKAGE = "Tipo de Vinculación"
export const MESSAGEENABLEDINSPECTION = "¿Está seguro que desea habilitar la inspección de este vehículo?"
export const MESSAGEDISABLEDINSPECTION = "¿Está seguro que desea inhabilitar la inspección de este vehículo?"

//String Module Notifications
export const DETAILLAERT = "Detalle Alerta"

//String Module Reports


//String Items Module Reports
export const VEHICLESINSPECTIONS = "Inspecciones Vehículares"
export const MILEAGEUPDATE = "Actualización Kilometrajes"
