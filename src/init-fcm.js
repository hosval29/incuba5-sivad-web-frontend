import * as firebase from "firebase/app";
import "firebase/messaging";
const initializedFirebaseApp = firebase.initializeApp({
    // Project Settings => Add Firebase to your web app
    messagingSenderId: "615351936142",
});
const messaging = initializedFirebaseApp.messaging();


messaging.usePublicVapidKey(
    // Project Settings => Cloud Messaging => Web Push certificates
    "BL02FOWQzV16hSWe9c5H3P5Dq64TwX9ROO0orW65nZivj4AoqZaVyEkOBliw8vfb9vi7W9hFS2iBlayaWzGkBsU"
);

export { messaging };