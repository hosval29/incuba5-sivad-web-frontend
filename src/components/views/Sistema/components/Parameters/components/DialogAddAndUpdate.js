//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material UI 
import {
    Dialog,
    FormHelperText,
    TextField,
    FormControlLabel,
    Switch,
    Button,
    Typography,
    IconButton,
    withStyles,
    Grid
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Material UI || Icons
import CloseIcon from '@material-ui/icons/Close';

//Utils
import http from '../../../../../../store/actions/http'
import { CRUDUPDATE, CRUDADD, CAMPOSOBLIGATORIOS } from '../../../../../../utils/constants'

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography color="secondary" variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
        justifyContent: 'space-between'
    }
}))(MuiDialogActions);

export class DialogAdd extends Component {

    state = {
        status: true,
        parameter: '',
        value: '',
        detail: '',
        errors: {},
        open: this.props.open
    };

    componentWillMount = () => {

        if (this.props.id) {
            this.getData(this.props.id)
        }
    }

    handleChange = e => {

        if (e.target.id === 'status') {
            this.setState({ status: e.target.checked });
        } else {
            this.setState({ [e.target.id]: e.target.value })
        }

    };

    handleClose = () => {
        this.props.handleOnClose(CRUDUPDATE)
    };

    handleSubmit = (id) => (e) => {

        const status = this.state.status ? 1 : 0;
        const dataAdd = {
            parameter: this.state.parameter,
            value: this.state.value.toString(),
            detail: this.state.detail,
            status: status.toString()
        }

        if (!id) {
            http.post('/api/parameters', dataAdd).then((result) => {
                this.handleClose()
                this.props.backAfterInsert(CRUDADD, result.data.message)
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
        } else {

            http.put(`/api/parameters/${id}`, dataAdd).then((result) => {
                if (result) {
                    this.handleClose()
                    this.props.backAfterInsert(CRUDUPDATE, result.data.message)
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
        }
    }

    getData = (id) => {

        if (id !== '') {
            http.get(`/api/parameters/${id}`).then((result) => {

                const dataUpdate = result.data.data
                let status = false
                if (dataUpdate.status === 1) {
                    status = true
                }
                this.setState({
                    parameter: dataUpdate.parameter,
                    value: dataUpdate.value,
                    detail: dataUpdate.detail,
                    status: status
                })

            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
        }
    }

    render() {
        const { id, title } = this.props;
        const { status, parameter, value, detail, errors, open } = this.state

        return (
            <Dialog
                fullWidth
                maxWidth="sm"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>{id ? CRUDUPDATE : CRUDADD} {title}</DialogTitle>
                <DialogContent>

                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="parameter"
                                name="parameter"
                                value={parameter}
                                label="Parametro"
                                error={errors.paramter ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.parameter}
                             
                            </Typography>
                            <br />

                            <TextField

                                margin="dense"
                                id="value"
                                name="value"
                                value={value}
                                label="Valor"
                                error={errors.value ? true : false }
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.value}
                              
                            </Typography>

                            <br />

                            <FormControlLabel
                                control={
                                    <Switch
                                        required
                                        id="status"
                                        checked={status}
                                        onChange={this.handleChange}
                                        value="status"
                                    />
                                }
                                label="Estado"
                            />
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField

                                margin="dense"
                                id="detail"
                                name="detail"
                                value={detail}
                                label="Detalle"
                                error={errors.detail ? true : false }
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.detail}
                           
                            </Typography>

                        </Grid>
                    </Grid>

                </DialogContent>
                {errors.message ? (<Typography align="center" color="error" variant="caption">
                    {errors.message}
                </Typography>) : null}
                <DialogActions>
                    <FormHelperText>{CAMPOSOBLIGATORIOS}</FormHelperText>
                    <Button onClick={this.handleSubmit(id)} color="secondary">
                        Agregar
                    </Button>
                </DialogActions>

            </Dialog>

        );
    }
}

DialogAdd.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    backAfterInsert: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    handleOnClose: PropTypes.func.isRequired
}

export default withStyles(styles)(DialogAdd);
