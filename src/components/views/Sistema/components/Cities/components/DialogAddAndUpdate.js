//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material UI 
import {
    Dialog,
    FormHelperText,
    TextField,
    FormControlLabel,
    Switch,
    Button,
    Typography,
    IconButton,
    withStyles,
    FormControl,
    Select,
    InputLabel,
    MenuItem,
    Grid
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Material UI || Icons
import CloseIcon from '@material-ui/icons/Close';

//Utils
import http from '../../../../../../store/actions/http'
import { CRUDUPDATE, CRUDADD, CAMPOSOBLIGATORIOS, REGION } from '../../../../../../utils/constants'

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography color="secondary" variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
        justifyContent: 'space-between'
    }
}))(MuiDialogActions);

export class DialogAdd extends Component {

    state = {
        status: true,
        city: '',
        idRegion: '',
        region: '',
        dataRegions: [],
        errors: {},
        open: this.props.open
    };

    componentWillMount = () => {
        this.getDataRegions()

        if (this.props.id) {
            this.getData(this.props.id)
        }
    }

    handleChange = e => {

        if (e.target.id === 'status') {
            this.setState({ status: e.target.checked });
        }

        const errors = this.state.errors
        
        switch (e.target.name) {
            case 'city':
                delete errors.city
                break;
            case 'region':
                delete errors.idRegion
                break;
            default:
             break;
        }
        this.setState({ errors: errors, [e.target.name]: e.target.value })

    };

    handleClose = () => {
        this.props.handleOnClose(CRUDUPDATE)
    };

    handleSubmit = (id) => (e) => {

        const status = this.state.status ? 1 : 0;

        let idRegion = ''
        const region = this.state.dataRegions.filter(itemRegion => itemRegion.region === this.state.region)
        
        if (region.length > 0) {
            idRegion = region[0].id
        }

        const dataAdd = {
			
            city: this.state.city,
            idRegion: idRegion.toString(),
            status: status.toString()
        }

        if (!id) {
            http.post('/api/cities', dataAdd).then((result) => {
                this.handleClose()
                this.props.backAfterInsert(CRUDADD, result.data.message)
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
        } else {

            http.put(`/api/cities/${id}`, dataAdd).then((result) => {
                if (result) {
                    this.handleClose()
                    this.props.backAfterInsert(CRUDUPDATE, result.data.message)
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
        }
    }

    getDataRegions = () => {
        this._isMounted = true;
        http.get('/api/regions')
            .then((result) => {
			
                if (this._isMounted) {
                    this.setState({ dataRegions: result.data.data })
                    
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    }

    getData = (id) => {

        if (id !== '') {
            http.get(`/api/cities/${id}`).then((result) => {

                const dataUpdate = result.data.city
                let status = false
                if (dataUpdate.status === 1) {
                    status = true
                }
                this.setState({
                    city: dataUpdate.city,
                    region: dataUpdate.Region.region,
                    status: status
                })

            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
        }
    }

    render() {
        const { id, title, classes } = this.props;
        const { status, city, region, dataRegions, errors, open } = this.state

        return (
            <Dialog
                fullWidth
                maxWidth="sm"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>{id ? CRUDUPDATE : CRUDADD} {title}</DialogTitle>
                <DialogContent>

                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="city"
                                name="city"
                                value={city}
                                label="Ciudad"
                                error={errors.city ? true : false }
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.city}
                            </Typography>

                            <br />

                            <FormControlLabel
                                control={
                                    <Switch
                                        required
                                        id="status"
                                        checked={status}
                                        onChange={this.handleChange}
                                        value="status"
                                    />
                                }
                                label="Estado"
                            />
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <FormControl error={errors.idRegion ? true : false} fullWidth required className={classes.formControl}>
                                <InputLabel required error={errors.idRegion ? true : false} htmlFor="region">{REGION}</InputLabel>
                                <Select
                                    error={errors.idRegion ? true : false}
                                    value={region}
                                    onChange={this.handleChange}
                                    name="region"
                                    id="region"
                                    inputProps={{
                                        id: 'region',
                                    }}
                                    className={classes.selectEmpty}
                                >
                                    <MenuItem value="">
                                        <em>Ninguno</em>
                                    </MenuItem>
                                    {dataRegions.map(itemRegion => (
                                        <MenuItem key={itemRegion.id} value={itemRegion.region}>{itemRegion.region}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <Typography color="error" variant="caption">
                                {errors.idRegion}
                            </Typography>
                        </Grid>
                    </Grid>

                </DialogContent>
                {errors.message ? (<Typography align="center" color="error" variant="caption">
                    {errors.message}
                </Typography>) : null}
                <DialogActions>
                    <FormHelperText>{CAMPOSOBLIGATORIOS}</FormHelperText>
                    <Button onClick={this.handleSubmit(id)} color="secondary">
                        Agregar
                    </Button>
                </DialogActions>

            </Dialog>

        );
    }
}

DialogAdd.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    backAfterInsert: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    handleOnClose: PropTypes.func.isRequired
}

export default withStyles(styles)(DialogAdd);
