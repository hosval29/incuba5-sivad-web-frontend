//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { connect } from 'react-redux';

//Material UI
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Tabs, Tab } from '@material-ui/core';

//Utils
import { setToolbar } from '../../../store/actions/authActions'
import { SISTEMA, REGIONS, CITIES, PARAMETER, PARAMETERS } from '../../../utils/constants';

//Components
import Regions from './components/Regions/Regions'
import Cities from './components/Cities/Cities'
import Parameters from './components/Parameters/Parameters'

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    appBar: {
        backgroundColor: theme.palette.background.paper,
        color: '#e0e0e0',
        position: 'fixed'
    }
});

function TabContainer(props) {

    switch (props.children) {
        case REGIONS:
            return <Regions />
            break;
        case CITIES:
            return <Cities /> 
            break;
        case PARAMETERS:
            return <Parameters />
        default:
            break;
    }
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired
};

class Sistema extends Component {
    state = {
        value: 0
    };
    handleChange = (event, value) => {
        this.setState({ value });
    };

    componentWillMount = () => {
        const toolbar = {
            title: SISTEMA,
            icon: ''
        }
        this.props.setToolbar(toolbar)
    }
    render() {
        const { classes, auth, match: { url } } = this.props;
        const { value } = this.state;

        const { user } = auth;
        const modules = user.profiles.modules;
        const menuSistema = modules.find(itemMenu => itemMenu.module === SISTEMA);
        const itemsMenuSistema = menuSistema.items;


        return (
            <div className={classes.root}>
                <AppBar className={classes.appBar} position="static">
                    <Tabs
                        variant="scrollable"
                        scrollButtons="auto"
                        textColor="secondary"
                        value={value}
                        onChange={this.handleChange}
                    >
                        {itemsMenuSistema.map((itemMenuSistema, index) => (
                            <Tab
                                key={itemMenuSistema.id}
                                value={index}
                                label={itemMenuSistema.itemModule}
                            />
                        ))}
                    </Tabs>
                </AppBar>
                {itemsMenuSistema.map(
                    (itemMenuSistema, index) =>
                        value === index && (
                            <TabContainer key={itemMenuSistema.id}>
                                {itemMenuSistema.itemModule}
                            </TabContainer>
                        )
                )}
            </div>
        );
    }
}

Sistema.propTypes = {
    classes: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    setToolbar: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default compose(
    withStyles(styles),
    connect(mapStateToProps, { setToolbar }),
    withRouter
)(Sistema);
