import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames'
import esLocale from "date-fns/locale/es";
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Grid, FormControl, InputLabel, Select, MenuItem, Paper, FormHelperText, FormLabel, RadioGroup, FormControlLabel, Radio, TextField } from '@material-ui/core';
import { OWNER, DRIVER, CAMPOSOBLIGATORIOS, TYPEALERT, STATEVEHICLE, TYPEINSPECTION , GESTION_VEHICULOS} from '../../../../../utils/constants';
import {
  MuiPickersUtilsProvider,
  DatePicker
} from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';

import { withRouter } from 'react-router'
import { compose } from 'recompose';
import { connect } from 'react-redux'

import { setToolbar } from '../../../../../store/actions/authActions'

const styles = theme => ({
  root: {
    width: '100%',
  },
  content: {
    padding: theme.spacing.unit * 2,
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column'
  },
  backButton: {
    marginRight: theme.spacing.unit,
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },

  //Style SelectOwnerAndDriver
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width: '80%'
  },
  formHelperText: {
    marginBottom: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit * 3
  },
  buttons: {
    marginTop: theme.spacing.unit * 2
  },
  formControl: {
    display: 'flex',
    flexDirection: 'row'
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  rootGrid: {
    flexGrow: 1,
  }
});

function getSteps() {
  return ['Selección Propietario || Conductor',
    'Crear Alertas Vehículo',
    'Crear Vehículo'];
}

class CreateVehicle extends React.Component {
  state = {
    activeStep: 0,
    dataOwners: [],
    idOwner: '',
    owner: '',

    dataDrivers: [],
    idDriver: '',
    driver: '',

    dateExpiration: new Date(),
    value: 'female',

    plate: '',
    model: '',
    color: '',
    combustible: "",
    capacidadPasajeros: "",
    capacidadKLS: "",
    cilindraje: "",
    noMotor: "",
    noChais: "",
    noLicencia: "",
    vin: "",
    dataStatusVehicles: [],
    statusVehicle: '',
    idStatusVehicle: "",
    dataTypesInspectionVehicula: [],
    inspectionVehicular: '',
    idTypeInspectionVehicular: '',
    errors: {}

  };

  componentWillMount = () => {
    const toolbar = {
      title: GESTION_VEHICULOS,
      icon: ''
    }
    this.props.setToolbar(toolbar)
  }

  handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
    });
  };

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep,
      dataOwners,
      idOwner,
      owner,

      dataDrivers,
      idDriver,
      driver,

      dateExpiration,
      value,

      plate,
      model,
      color,
      combustible,
      capacidadPasajeros,
      capacidadKLS,
      cilindraje,
      noMotor,
      noChais,
      noLicencia,
      vin,
      dataStatusVehicles,
      statusVehicle,
      idStatusVehicle,
      dataTypesInspectionVehicula,
      inspectionVehicular,
      idTypeInspectionVehicular,
      errors } = this.state;

    const SelectOwnerAndDriver = () => {
      return (

        <Grid container spacing={24}>
          <Grid item xs={12} sm={12}>
            <FormControl
              error={errors.owner ? true : false}
              fullWidth
              required

            >
              <InputLabel
                required
                error={errors.owner ? true : false}
                htmlFor="owner"
              >
                {OWNER}
              </InputLabel>
              <Select
                autoFocus
                error={errors.owner ? true : false}
                value={owner}
                onChange={this.handleChange}
                name="owner"
                id="owner"
                inputProps={{
                  id: 'owner'
                }}
                className={classes.selectEmpty}
              >
                <MenuItem value="">
                  <em>Ninguno</em>
                </MenuItem>
                {dataOwners.map(item => (
                  <MenuItem key={item.id} value={item.owner}>
                    {item.owner}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <Typography color="error" variant="caption">
              {errors.owner}
            </Typography>
          </Grid>

          <Grid item xs={12} sm={12}>
            <FormControl
              error={errors.driver ? true : false}
              fullWidth
              required

            >
              <InputLabel
                required
                error={errors.driver ? true : false}
                htmlFor="driver"
              >
                {DRIVER}
              </InputLabel>
              <Select
                error={errors.driver ? true : false}
                value={driver}
                onChange={this.handleChange}
                name="driver"
                id="driver"
                inputProps={{
                  id: 'driver'
                }}
                className={classes.selectEmpty}
              >
                <MenuItem value="">
                  <em>Ninguno</em>
                </MenuItem>
                {dataDrivers.map(item => (
                  <MenuItem key={item.id} value={item.driver}>
                    {item.driver}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <Typography color="error" variant="caption">
              {errors.driver}
            </Typography>
          </Grid>
        </Grid>
      )
    }

    const CreateAlertsVehicle = () => {
      return (
        <Grid container spacing={24}>

          <Grid item xs={12}>

            <FormControl
              error={errors.owner ? true : false}
              fullWidth
              required
            >
              <InputLabel
                required
                error={errors.owner ? true : false}
                htmlFor="owner"
              >
                {TYPEALERT}
              </InputLabel>
              <Select
                autoFocus
                error={errors.owner ? true : false}
                value={owner}
                onChange={this.handleChange}
                name="owner"
                id="owner"
                inputProps={{
                  id: 'owner'
                }}
                className={classes.selectEmpty}
              >
                <MenuItem value="">
                  <em>Ninguno</em>
                </MenuItem>
                {dataOwners.map(item => (
                  <MenuItem key={item.id} value={item.owner}>
                    {item.owner}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <Typography color="error" variant="caption">
              {errors.owner}
            </Typography>

          </Grid>
          <Grid item xs={12}>

            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
              <Grid container className={classes.grid} justify="space-around">
                <DatePicker
                  id="dateExpiration"
                  margin="normal"
                  fullWidth
                  label="Fecha Expiration"
                  format="yyyy/mm/dd"
                  required
                  invalidDateMessage={errors.dateExpiration}
                  invalidLabel={errors.dateExpiration}
                  value={dateExpiration}
                  error={errors.dateExpiration ? true : false}
                  onChange={this.handleDateExpeditionChange}
                />
              </Grid>
            </MuiPickersUtilsProvider>

          </Grid>

          <Grid item xs={12}>

            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">Visible Para:</FormLabel>
              <RadioGroup
                aria-label="Visible Para:"
                name="destino"
                className={classes.group}
                value={this.state.value}
                onChange={this.handleChange}
              >
                <FormControlLabel value="detinoWeb" control={<Radio />} label="Aplicación Movil" />
                <FormControlLabel value="destinoApp" control={<Radio />} label="Plataforma Web" />
                <FormControlLabel value="destinoAll" control={<Radio />} label="Todos" />
              </RadioGroup>
            </FormControl>

          </Grid>

        </Grid>
      )
    }

    const CreateVehicle = () => {

      return (
        <Grid container spacing={24}>
          <Grid item xs={12} sm={3} md={3}>
            <TextField
              margin="dense"
              id="plate"
              name="plate"
              value={plate}
              label="Placa"
              error={errors.plate ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.plate}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <TextField
              margin="dense"
              id="model"
              name="model"
              value={model}
              label="Modelo"
              error={errors.model ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.model}
            </Typography>
          </Grid>

          <Grid item xs={12} sm={3} md={3}>
            <TextField
              margin="dense"
              id="color"
              name="color"
              value={color}
              label="Color"
              error={errors.color ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.color}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <TextField

              margin="dense"
              id="combustible"
              name="combustible"
              value={combustible}
              label="Combustible"
              error={errors.combustible ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.combustible}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <TextField

              margin="dense"
              id="capacidadPasajeros"
              name="capacidadPasajeros"
              value={capacidadPasajeros}
              label="Capacidad Pasajeros"
              error={errors.capacidadPasajeros ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.capacidadPasajeros}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <TextField
              margin="dense"
              id="capacidadKLS"
              name="capacidadKLS"
              value={capacidadKLS}
              label="Capacidad KLS"
              error={errors.capacidadKLS ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.capacidadKLS}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <TextField
              margin="dense"
              id="cilindraje"
              name="cilindraje"
              value={cilindraje}
              label="Cilindraje"
              error={errors.cilindraje ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.cilindraje}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <TextField
              margin="dense"
              id="noMotor"
              name="noMotor"
              value={noMotor}
              label="No. Motor"
              error={errors.noMotor ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.noMotor}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <TextField

              margin="dense"
              id="noChais"
              name="noChais"
              value={noChais}
              label="No. Chais"
              error={errors.noChais ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.noChais}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <TextField
              margin="dense"
              id="noLicencia"
              name="noLicencia"
              value={noLicencia}
              label="No. Licencia"
              error={errors.noLicencia ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.noLicencia}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <TextField

              margin="dense"
              id="vin"
              name="vin"
              value={vin}
              label="VIN"
              error={errors.vin ? true : false}
              fullWidth
              onChange={this.handleChange}
              required
            />
            <Typography color="error" variant="caption">
              {errors.vin}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <FormControl
              error={errors.statusVehicle ? true : false}
              fullWidth
              required
            >
              <InputLabel
                required
                error={errors.statusVehicle ? true : false}
                htmlFor="owner"
              >
                {STATEVEHICLE}
              </InputLabel>
              <Select
                autoFocus
                error={errors.statusVehicle ? true : false}
                value={owner}
                onChange={this.handleChange}
                name="statusVehicle"
                id="statusVehicle"
                inputProps={{
                  id: 'statusVehicle'
                }}
                className={classes.selectEmpty}
              >
                <MenuItem value="">
                  <em>Ninguno</em>
                </MenuItem>
                {dataStatusVehicles.map(item => (
                  <MenuItem key={item.id} value={item.state}>
                    {item.state}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <Typography color="error" variant="caption">
              {errors.statusVehicle}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}>
            <FormControl
              error={errors.inspectionVehicular ? true : false}
              fullWidth
              required
            >
              <InputLabel
                required
                error={errors.inspectionVehicular ? true : false}
                htmlFor="owner"
              >
                {TYPEINSPECTION}
              </InputLabel>
              <Select
                autoFocus
                error={errors.inspectionVehicular ? true : false}
                value={inspectionVehicular}
                onChange={this.handleChange}
                name="inspectionVehicular"
                id="inspectionVehicular"
                inputProps={{
                  id: 'inspectionVehicular'
                }}
                className={classes.selectEmpty}
              >
                <MenuItem value="">
                  <em>Ninguno</em>
                </MenuItem>
                {dataTypesInspectionVehicula.map(item => (
                  <MenuItem key={item.id} value={item.type}>
                    {item.type}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <Typography color="error" variant="caption">
              {errors.owner}
            </Typography>
          </Grid>
        </Grid>
      )
    }

    function getStepContent(stepIndex) {
      switch (stepIndex) {
        case 0:
          return <SelectOwnerAndDriver />
        case 1:
          return <CreateAlertsVehicle />;
        case 2:
          return <CreateVehicle />
        default:
          return 'Unknown stepIndex';
      }
    }

    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map(label => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {this.state.activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>All steps completed</Typography>
              <Button onClick={this.handleReset}>Reset</Button>
            </div>
          ) : (
              <div className={classes.content}>
                <Paper className={classNames(classes.instructions, classes.paper)}>{getStepContent(activeStep, classes)}</Paper>
                <div className={classes.buttons}>
                  <Button
                    disabled={activeStep === 0}
                    onClick={this.handleBack}
                    className={classes.backButton}
                  >
                    Atras
                </Button>
                  <Button variant="contained" color="primary" onClick={this.handleNext}>
                    {activeStep === steps.length - 1 ? 'Finalizar' : 'Siguiente'}
                  </Button>
                </div>
              </div>
            )}
        </div>
      </div>
    );
  }
}

CreateVehicle.propTypes = {
  classes: PropTypes.object,
  setToolbar: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  toolbar: state.toolbar
})

export default compose(
  withStyles(styles),
  connect(mapStateToProps, { setToolbar }),
  withRouter
)(CreateVehicle);
