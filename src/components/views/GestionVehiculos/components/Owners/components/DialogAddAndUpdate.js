//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material UI
import {
    Dialog,
    FormHelperText,
    TextField,
    FormControlLabel,
    Switch,
    Button,
    Typography,
    IconButton,
    withStyles,
    Grid,
    FormControl,
    MenuItem,
    Select,
    InputLabel
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';

//Utils
import http from '../../../../../../store/actions/http';
import {
    CRUDUPDATE,
    CRUDADD,
    CAMPOSOBLIGATORIOS,
    BUTTONADD,
    BUTTONSAVE
} from '../../../../../../utils/constants';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    },
    iconTitleDialog: {
        marginRight: 10
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
        justifyContent: 'space-between'
    }
}))(MuiDialogActions);

class DialogAddAndUpdate extends Component {
    state = {
        status: true,
        name: '',
        lastName: '',
        typeIdentfication: '',
        idTypeIdentification: '',
        dataTypeIdentification: [],
        identification: '',
        homeAddress: '',
        homePhone: '',
        cellPhoneNumber: '',
        email: '',
        errors: {},
        open: this.props.open
    };

    componentWillMount = () => {

        if (this.props.id) {
            this.getData(this.props.id);
        }

        this.getDataTypesIdentification();
    };

    componentWillUnmount = () => {

        this.setState({
            name: '',
            lastName: '',
            typeIdentfication: '',
            idTypeIdentification: '',
            dataTypeIdentification: [],
            identification: '',
            homeAddress: '',
            homePhone: '',
            cellPhoneNumber: '',
            email: '',
            errors: {}
        });
    };

    getDataTypesIdentification = () => {
        http
            //.get('/api/typesidentification/register/enabled')
            .get('/api/typesidentification')
            .then(result => {
                if (result) {
                    this.setState({ dataTypeIdentification: result.data.data })
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    };

    handleChange = e => {
        if (e.target.id === 'status') {
            this.setState({ status: e.target.checked });
        }

        const errors = this.state.errors;

        switch (e.target.name) {
            case 'name':
                delete errors.name;
                break;
            case 'lastName':
                delete errors.lastName;
                break;
            case 'typeIdentfication':
                delete errors.idTypeIdentification;
                break;
            case 'identification':
                delete errors.identification;
                break;
            case 'homeAddress':
                delete errors.homeAddress;
                break;
            case 'homePhone':
                delete errors.homePhone;
                break;
            case 'cellPhoneNumber':
                delete errors.cellPhoneNumber;
                break;
            case 'email':
                delete errors.email;
                break;
            default:
                break;
        }
        this.setState({ errors: errors, [e.target.name]: e.target.value });
    };

    handleClose = () => {
        this.props.handleOnClose(CRUDUPDATE);
    };

    handleSubmit = id => e => {
        const status = this.state.status ? 1 : 0;

        let idTypeIdentification = '';
        const typeIdentification = this.state.dataTypeIdentification.filter(
            itemTypeIdentification =>
                itemTypeIdentification.type === this.state.typeIdentfication
        );

        if (typeIdentification.length > 0) {
            idTypeIdentification = typeIdentification[0].id;
        }

        const dataAdd = {
            idTypeIdentification: idTypeIdentification.toString(),
            identification: this.state.identification.toString(),
            name: this.state.name.toString(),
            lastName: this.state.lastName.toString(),
            homeAddress: this.state.homeAddress,
            homePhone: this.state.homePhone.toString(),
            cellPhoneNumber: this.state.cellPhoneNumber.toString(),
            email: this.state.email,
            status: status.toString()
        };

        if (!id) {
            http
                .post('/api/owners', dataAdd)
                .then(result => {
                    this.handleClose();
                    this.props.backAfterInsert(CRUDADD, result.data.message);
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        } else {
            http
                .put(`/api/owners/${id}`, dataAdd)
                .then(result => {
                    if (result) {
                        this.handleClose();
                        this.props.backAfterInsert(CRUDUPDATE, result.data.message);
                    }
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    getData = id => {
        if (id !== '') {
            http
                .get(`/api/owners/${id}`)
                .then(result => {

                    const dataUpdate = result.data.data;

                    let status = false;
                    if (dataUpdate.status === 1) {
                        status = true;
                    }

                    this.setState({
                        name: dataUpdate.name,
                        lastName: dataUpdate.lastName,
                        idTypeIdentification: dataUpdate.idTypeIdentification,
                        identification: dataUpdate.identification,
                        homeAddress: dataUpdate.homeAddress,
                        homePhone: dataUpdate.homePhone,
                        cellPhoneNumber: dataUpdate.cellPhoneNumber,
                        email: dataUpdate.email,
                        status: status,
                        typeIdentfication: dataUpdate.TypesIdentification.type
                    });
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    render() {
        const { classes, open, title, id } = this.props;
        const {
            status,
            name,
            lastName,
            typeIdentfication,
            dataTypeIdentification,
            // idTypeIdentification,
            identification,
            homeAddress,
            homePhone,
            cellPhoneNumber,
            email,
            errors,
        } = this.state;

        return (
            <Dialog
                fullWidth
                maxWidth="sm"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />{' '}
                    {id ? CRUDUPDATE : CRUDADD} {title}
                </DialogTitle>

                <DialogContent>
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={6}>
                            <FormControl
                                error={errors.idTypeIdentification ? true : false}
                                fullWidth
                                required
                                className={classes.formControl}
                            >
                                <InputLabel
                                    required
                                    error={errors.idTypeIdentification ? true : false}
                                    htmlFor="idTypeIdentification"
                                >
                                    Tipo de Identificación
                                </InputLabel>
                                <Select
                                    error={errors.idTypeIdentification ? true : false}
                                    value={typeIdentfication}
                                    onChange={this.handleChange}
                                    name="typeIdentfication"
                                    id="typeIdentfication"
                                    inputProps={{
                                        id: 'typeIdentfication'
                                    }}
                                    className={classes.selectEmpty}
                                >
                                    <MenuItem value="">
                                        <em>Ninguno</em>
                                    </MenuItem>
                                    {dataTypeIdentification.map(itemIdentification => {
                                        return itemIdentification.id !== 2 ?
                                            (
                                                <MenuItem
                                                    key={itemIdentification.id}
                                                    value={itemIdentification.type}
                                                >
                                                    {itemIdentification.type}
                                                </MenuItem>
                                            ): null
                                        }
                                    )}
                                </Select>
                            </FormControl>
                            <Typography color="error" variant="caption">
                                {errors.idTypeIdentification}
                            </Typography>

                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="identification"
                                name="identification"
                                value={identification}
                                label="Identificación"
                                error={errors.identification ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.identification}
                            </Typography>

                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                name="name"
                                value={name}
                                label="Nombre"
                                error={errors.name ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.name}
                            </Typography>

                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="lastName"
                                name="lastName"
                                value={lastName}
                                label="Apellido"
                                error={errors.lastName ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.lastName}
                            </Typography>

                        </Grid>

                        <Grid item xs={12} sm={6}>

                            <TextField
                                autoFocus
                                margin="dense"
                                id="homeAddress"
                                name="homeAddress"
                                value={homeAddress}
                                label="Dirección Casa"
                                error={errors.homeAddress ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.homeAddress}
                            </Typography>

                        </Grid>

                        <Grid item xs={12} sm={6}>

                            <TextField
                                autoFocus
                                margin="dense"
                                id="homePhone"
                                name="homePhone"
                                value={homePhone}
                                label="Teléfono Casa"
                                error={errors.homePhone ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.homePhone}
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={6}>

                            <TextField
                                autoFocus
                                margin="dense"
                                id="cellPhoneNumber"
                                name="cellPhoneNumber"
                                value={cellPhoneNumber}
                                label="Teléfono Celular"
                                error={errors.cellPhoneNumber ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.cellPhoneNumber}
                            </Typography>

                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="email"
                                name="email"
                                value={email}
                                label="Correo"
                                error={errors.email ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.email}
                            </Typography>

                        </Grid>

                        <Grid item xs={12} sm={6}>

                            <FormControlLabel
                                control={
                                    <Switch
                                        required
                                        id="status"
                                        checked={status}
                                        onChange={this.handleChange}
                                        value="status"
                                    />
                                }
                                label="Estado"
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                {errors.message ? (
                    <Typography align="center" color="error" variant="caption">
                        {errors.message}
                    </Typography>
                ) : null}
                <DialogActions>
                    <FormHelperText>{CAMPOSOBLIGATORIOS}</FormHelperText>
                    <Button onClick={this.handleSubmit(id)} color="secondary">
                        {id ? BUTTONSAVE : BUTTONADD}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogAddAndUpdate.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    handleOnClose: PropTypes.func.isRequired,
    backAfterInsert: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogAddAndUpdate);
