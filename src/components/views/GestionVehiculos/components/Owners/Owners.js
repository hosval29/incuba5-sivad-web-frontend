import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';
import { withRouter } from 'react-router'
import { compose } from 'recompose';
import { connect } from 'react-redux'


//Styles
import { withStyles } from '@material-ui/core/styles';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

//Components
import CustomToolbar from '../../../components/CustomToolbar'
import DialogAdd from './components/DialogAddAndUpdate'
import SnackbarInfo from '../../../../../utils/snackbars/components/Snackbar/SnackbarInfo';
import CustomButtonGestion from '../../../components/CustomButtonGestion'

//Utils
import http from '../../../../../store/actions/http';
import {
    STATUSINACTIVO,
    STATUSACTIVO,
    CRUDDELETE,
    CRUDUPDATE,
    CRUDADD,
    GESTIONBUTTON,
    OWNER,
    GESTION_VEHICULOS,
    LISTOWNERS
} from '../../../../../utils/constants'
import DialogDelete from './components/DialogDelete';
// import { REGION, REGIONS } from '../../../../../utils/constants'
import { setToolbar } from '../../../../../store/actions/authActions'

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 3
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    }
});

class Owners extends Component {

    state = {
        errors: [],
        data: [],
        openDialogAdd: false,
        openDialogDelete: false,
        _id: '',
        snackbarOpen: false,
        snackbarMessage: '',
        snackbarVariant: '',
    }

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            },
            overrides: {
                MuiToolbar: {
                    root: {
                        backgroundColor: '#f5f5f5',
                        marginTop: 10,
                        marginBottom: 10,
                        boxShadow:
                            '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)'
                    }
                },
                MUIDataTableHeadCell: {
                    fixedHeader: {
                        color: '#FFFFFF',
                        backgroundColor: '#00587c'
                    }
                }
            }
        });

    componentWillMount() {
        const toolbar = {
            title: GESTION_VEHICULOS,
            icon: ''
        }
        this.props.setToolbar(toolbar)
        this.getData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getData = () => {
        this._isMounted = true;
        http
            .get('/api/owners')
            .then(result => {
                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    };

    handleClickAdd = e => {
        this.setState({ openDialogAdd: !this.state.openDialogAdd });

    };

    handleClickButtonGestion = (gestion, idUser) => {

        switch (gestion) {
            case GESTIONBUTTON[1]:
                this.setState({ openDialogDelete: !this.state.openDialogDelete, status: 0 })
                break;
            case GESTIONBUTTON[2]:
                this.setState({ openDialogDelete: !this.state.openDialogDelete, status: 1 })
                break;
            case GESTIONBUTTON[0]:
                this.setState({ openDialogAdd: !this.state.openDialogAdd })
                break;
            default:
                break
        }

        this.setState({ _id: idUser });
    }

    handleClose = (action) => {
        switch (action) {
            case CRUDUPDATE:
                this.setState({ openDialogAdd: !this.state.openDialogAdd })
                break;
            case CRUDDELETE:
                this.setState({ openDialogDelete: !this.state.openDialogDelete, status: '' })
                break;
            default:
                break
        }

    }

    handleCloseDialogDelete = () => {
        this.setState({openDialogDelete: !this.state.openDialogDelete, status: ''})
    }

    backAfterInsert = (action, message) => {
        switch (action) {
            case CRUDADD:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'success'
                });
                break;
            case CRUDUPDATE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;
            case CRUDDELETE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'warning'
                });
                break;
            default:
                break;
        }

        this.setState({
            data: [],
            openDialogAdd: false,
            openDialogDelete: false,
            _id: '',
        })

        this.getData();
    }

    handleCloseSnackbar = e => {
        this.setState({ snackbarOpen: !this.state.snackbarOpen });
    };

    render() {
        const { classes } = this.props;
        const { data, openDialogAdd, _id, openDialogDelete, status } = this.state;

        const columns = [
            { name: 'id', label: 'Id' },
            { name: "name", label: "Nombre" },
            { name: "lastName", label: "Apellido" },
            {
                name: "TypesIdentification", label: "Tipo Identificación", options: {
                    customBodyRender: (value) => value.type
                }
            },
            { name: "identification", label: "Identificación" },
            { name: "homeAddress", label: "Dirección Casa" },
            { name: "homePhone", label: "Teléfono Casa" },
            { name: "cellPhoneNumber", label: "Teléfono Celular" },
            { name: "email", label: "Correo" },

            {
                name: 'status',
                label: 'Estado',
                options: {
                    isRowSelectable: null,
                    customBodyRender: (value, tableMeta, updateValue) =>
                        (value === 1 ? <Typography color="secondary" variant="caption">
                            {STATUSACTIVO}
                        </Typography> : <Typography color="error" variant="caption">
                                {STATUSINACTIVO}
                            </Typography>)
                }
            },
            {
                name: 'id',
                label: 'Gestión',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <CustomButtonGestion id={value} handleClickButtonGestion={this.handleClickButtonGestion} />
                        );
                    }
                }
            },
        ];

        const options = {
            filterType: "dropdown",
            responsive: "scroll",
            selectableRows: false,
            isRowSelectable: false,
            rowsSelected: false,
            textLabels: {
                body: {
                    noMatch: "Lo sentimos, no se encontraron registros coincidentes",
                    toolTip: "Ordenar",
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                },
                filter: {
                    all: 'Todos',
                    title: 'Filtrar',
                    reset: 'Limpiar Filtro'
                },
                viewColumns: {
                    title: "Columas Visibles"
                },
            },
            customToolbar: () => {
                return (
                    <CustomToolbar title={OWNER} handleClickAdd={this.handleClickAdd} />
                );
            }
        };

        return (
            <div className={classes.root}>
                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable
                        title={LISTOWNERS}
                        data={data}
                        columns={columns}
                        options={options} />
                </MuiThemeProvider>
                {openDialogAdd ?
                    (<DialogAdd
                        open={openDialogAdd}
                        backAfterInsert={this.backAfterInsert}
                        id={_id}
                        title={OWNER}
                        handleOnClose={this.handleClose}
                    />
                    ) : null}
                {openDialogDelete ?
                    (<DialogDelete
                        open={openDialogDelete}
                        backAfterInsert={this.backAfterInsert}
                        id={_id}
                        status={status}
                        title={OWNER}
                        handleClose={this.handleCloseDialogDelete}
                    />) : null}

                {this.state.snackbarOpen ?
                    (<SnackbarInfo
                        open={this.state.snackbarOpen}
                        onClose={this.handleCloseSnackbar}
                        variant={this.state.snackbarVariant}
                        message={this.state.snackbarMessage}
                    />) : null}
            </div>
        );
    }
}

Owners.propTypes = {
    classes: PropTypes.object.isRequired,
    setToolbar: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
});

export default compose(
    withStyles(styles),
    connect(mapStateToProps, { setToolbar }),
    withRouter
)(Owners);
