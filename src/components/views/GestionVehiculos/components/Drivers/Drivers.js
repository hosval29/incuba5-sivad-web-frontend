//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';
import { withRouter } from 'react-router';
import { compose } from 'recompose';
import { connect } from 'react-redux';

//Material UI
import {
    withStyles,
    createMuiTheme,
    MuiThemeProvider,
    Typography,
    IconButton
} from '@material-ui/core';

//Components
import CustomToolbar from '../../../components/CustomToolbar';
import DialogAddAndUpdate from './components/DialogAddAndUpdate';
import SnackbarInfo from '../../../../../utils/snackbars/components/Snackbar/SnackbarInfo';
import CustomButtonGestion from '../../../components/CustomButtonGestion';
import DialogDetailDataDriver from './components/DialogDetailDataDriver'
import DialogDelete from './components/DialogDelete';
import DialogConvertUser from './components/DialogConvertUser'
import DialogDriverDisabled from './components/DialogDriverDisabled'

//Icons
import { FileEyeOutline, Account } from 'mdi-material-ui';

//Utils
import http from '../../../../../store/actions/http';
import {
    STATUSINACTIVO,
    STATUSACTIVO,
    CRUDDELETE,
    CRUDUPDATE,
    CRUDADD,
    GESTIONBUTTON,
    DRIVER,
    GESTION_VEHICULOS,
    LISTDRIVERS,
    DETAILDATADRIVER,
    CRUDCONVERTUSER,
    CONVERTDRIVERUSER
} from '../../../../../utils/constants';
import { setToolbar } from '../../../../../store/actions/authActions';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 3
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    }
});

class Drivers extends Component {
    state = {
        data: [],
        openDialogDetailData: false,
        openDialogAddAndUpdate: false,
        openDialogDriverDisabled: false,
        openDialogDelete: false,
        openDialogConvertUser: false,
        openDialogDriverDisabled: false,
        _id: '',
        snackbarOpen: false,
        snackbarMessage: '',
        snackbarVariant: '',
        dataUserConvert: {},
        messageDialogDriverDisabled: ''
    };

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            },
            overrides: {
                MuiToolbar: {
                    root: {
                        backgroundColor: '#f5f5f5',
                        marginTop: 10,
                        marginBottom: 10,
                        boxShadow:
                            '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)'
                    }
                },
                MUIDataTableHeadCell: {
                    fixedHeader: {
                        color: '#FFFFFF',
                        backgroundColor: '#00587c'
                    }
                }
            }
        });

    componentWillMount() {
        const toolbar = {
            title: GESTION_VEHICULOS,
            icon: ''
        };
        this.props.setToolbar(toolbar);
        this.getData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getData = () => {
        this._isMounted = true;
        http
            .get('/api/drivers')
            .then(result => {
                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    };

    getDataDriverConvertUser = (id) => {
        console.log("OUTPUT: Drivers -> getDataDriverConvertUser -> id", id)
        http.get(`/api/drivers/convertuser/${id}`)
            .then((result) => {
                console.log("OUTPUT: Drivers -> getDataDriverConvertUser -> result", result)

                if (result.data.error) {
                    this.setState({ openDialogDriverDisabled: !this.state.openDialogDriverDisabled, messageDialogDriverDisabled: result.data.message })
                    return
                } else {
                    const data = result.data.data
                    console.log("OUTPUT: Drivers -> getDataDriverConvertUser -> data", data)
                    const name = data.name
                    const lastName = data.lastName
                    const email = data.email
                    const homePhone = data.homePhone
                    let idUser = data.idUser !== null ? data.idUser : ''
                    let password = data.User !== null ? data.User.password : ''

                    let dataUserConvert = {
                        name: name,
                        lastName: lastName,
                        email: email,
                        homePhone: homePhone,
                        idUser: idUser,
                        pass: password,
                        passConfirm: password,
                        idDriver: id
                    }

                    this.setState({
                        dataUserConvert: dataUserConvert,
                        openDialogConvertUser: !this.state.openDialogConvertUser
                    })
                }

            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    }

    handleClickAdd = e => {
        this.setState({
            openDialogAddAndUpdate: !this.state.openDialogAddAndUpdate
        });
    };

    handleClickButtonGestion = (gestion, id) => {
        switch (gestion) {
            case GESTIONBUTTON[1]:
                this.setState({
                    openDialogDelete: !this.state.openDialogDelete,
                    status: 0
                });
                break;
            case GESTIONBUTTON[2]:
                this.setState({
                    openDialogDelete: !this.state.openDialogDelete,
                    status: 1
                });
                break;
            case GESTIONBUTTON[0]:
                this.setState({
                    openDialogAddAndUpdate: !this.state.openDialogAddAndUpdate
                });
                break;
            default:
                break;
        }

        this.setState({ _id: id });
    };

    handleClose = action => {
        switch (action) {
            case CRUDUPDATE:
                this.setState({
                    openDialogAddAndUpdate: !this.state.openDialogAddAndUpdate,
                    _id: ''
                });
                break;
            case CRUDDELETE:
                this.setState({ openDialogDelete: !this.state.openDialogDelete });
                break;
            case CRUDCONVERTUSER:
                this.setState({ openDialogConvertUser: !this.state.openDialogConvertUser });
                break;
            default:
                break;
        }
    };

    handleCloseDialogDetailData = () => {
        this.setState({ openDialogDetailData: !this.state.openDialogDetailData })
    }

    handleCloDialogDriverDisabled = () => {
        this.setState({ openDialogDriverDisabled: !this.state.openDialogDriverDisabled, messageDialogDriverDisabled: '' })
    }

    backAfterInsert = (action, message) => {
        switch (action) {
            case CRUDADD:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'success'
                });
                break;
            case CRUDUPDATE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;
            case CRUDDELETE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'warning'
                });
                break;
        }

        this.setState({
            data: [],
            openDialogAddAndUpdate: false,
            openDialogDelete: false,
            _id: ''
        });

        this.getData();
    };

    handleCloseSnackbar = e => {
        this.setState({ snackbarOpen: !this.state.snackbarOpen });
    };

    render() {
        const { classes } = this.props;
        const { data,
            openDialogAddAndUpdate,
            _id,
            dataUserConvert,
            openDialogDetailData,
            openDialogDelete,
            openDialogConvertUser,
            openDialogDriverDisabled,
            messageDialogDriverDisabled } = this.state;

        const columns = [
            { name: 'id', label: 'Id' },
            {
                name: 'TypesIdentification',
                label: 'Tipo Identificación',
                options: {
                    customBodyRender: value => value.type
                }
            },
            { name: 'identification', label: 'Identificación' },
            {
                name: 'id',
                label: 'Detalle Conductor',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <IconButton
                                onClick={() =>
                                    this.setState({ openDialogDetailData: !openDialogDetailData, _id: value })
                                }
                            >
                                <FileEyeOutline />
                            </IconButton>
                        );
                    }
                }
            },
            {
                name: 'status',
                label: 'Estado',
                options: {
                    isRowSelectable: null,
                    customBodyRender: (value, tableMeta, updateValue) =>
                        value === 1 ? (
                            <Typography color="secondary" variant="caption">
                                {STATUSACTIVO}
                            </Typography>
                        ) : (
                                <Typography color="error" variant="caption">
                                    {STATUSINACTIVO}
                                </Typography>
                            )
                }
            },
            {
                name: 'id',
                label: 'Convertir a Usuario',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <IconButton
                                onClick={() =>
                                    this.getDataDriverConvertUser(value)
                                }
                            >
                                <Account />
                            </IconButton>
                        );
                    }
                }
            },
            {
                name: 'id',
                label: 'Gestión',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <CustomButtonGestion
                                id={value}
                                handleClickButtonGestion={this.handleClickButtonGestion}
                            />
                        );
                    }
                }
            }
        ];

        const options = {
            filterType: "dropdown",
            responsive: "scroll",
            selectableRows: false,
            isRowSelectable: false,
            rowsSelected: false,
            textLabels: {
                body: {
                    noMatch: "Lo sentimos, no se encontraron registros coincidentes",
                    toolTip: "Ordenar",
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                },
                filter: {
                    all: 'Todos',
                    title: 'Filtrar',
                    reset: 'Limpiar Filtro'
                },
                viewColumns: {
                    title: "Columas Visibles"
                },
            },
            customToolbar: () => {
                return (
                    <CustomToolbar title={DRIVER} handleClickAdd={this.handleClickAdd} />
                );
            }
        };

        return (
            <div className={classes.root}>
                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable
                        title={LISTDRIVERS}
                        data={data}
                        columns={columns}
                        options={options}
                    />
                </MuiThemeProvider>
                {openDialogAddAndUpdate ? (
                    <DialogAddAndUpdate
                        open={openDialogAddAndUpdate}
                        title={DRIVER}
                        backAfterInsert={this.backAfterInsert}
                        id={_id}
                        handleClose={this.handleClose}
                    />
                ) : null}
                {openDialogDelete ? (
                    <DialogDelete
                        open={openDialogDelete}
                        backAfterInsert={this.backAfterInsert}
                        id={this.state._id}
                        status={this.state.status}
                        title={DRIVER}
                        handleClose={this.handleClose}
                    />
                ) : null}
                {openDialogDetailData ?
                    (<DialogDetailDataDriver
                        title={DETAILDATADRIVER}
                        open={openDialogDetailData}
                        handleClose={this.handleCloseDialogDetailData}
                        id={_id}
                    />) : null}
                {openDialogConvertUser ? (
                    <DialogConvertUser
                        title={CONVERTDRIVERUSER}
                        open={openDialogConvertUser}
                        handleClose={this.handleClose}
                        backAfterInsert={this.backAfterInsert}
                        data={dataUserConvert}
                    />
                ) : null}
                {this.state.snackbarOpen ? (
                    <SnackbarInfo
                        open={this.state.snackbarOpen}
                        onClose={this.handleCloseSnackbar}
                        variant={this.state.snackbarVariant}
                        message={this.state.snackbarMessage}
                    />
                ) : null}
                {openDialogDriverDisabled ? (
                    <DialogDriverDisabled
                        open={openDialogDriverDisabled}
                        title="Conductor Inhabilitado"
                        handleClose={this.handleCloDialogDriverDisabled}
                        message={messageDialogDriverDisabled}
                    />
                ) : null}
            </div>
        );
    }
}

Drivers.propTypes = {
    classes: PropTypes.object.isRequired,
    setToolbar: PropTypes.func.isRequired
};

const mapStateToProps = state => ({});

export default compose(
    withStyles(styles),
    connect(
        mapStateToProps,
        { setToolbar }
    ),
    withRouter
)(Drivers);
