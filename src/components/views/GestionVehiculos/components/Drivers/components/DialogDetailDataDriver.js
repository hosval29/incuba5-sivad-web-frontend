import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { PDFExport } from '@progress/kendo-react-pdf';

//Material UI
import {
    Typography,
    IconButton,
    Slide,
    Dialog,
    AppBar,
    Toolbar,
    Grid,
    Paper,
    Table,
    TableRow,
    TableBody,
    TableCell,
    List,
    ListSubheader,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    Tooltip,
    withStyles
} from '@material-ui/core';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import SaveAltIcon from '@material-ui/icons/SaveAlt';

//Constants
import {
    ID,
    DATE,
    USERGESTOR,
    PLATEVEHICLE,
    DETAILINSPECTION
} from '../../../../../../utils/constants.js';

//Utils
import http from '../../../../../../utils/http';

const styles = theme => ({

    flex: {
        flex: 1
    },
    body: {
        padding: '1rem'
    },
    paper: {
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    subtitle: {
        fontWeight: '500'
    },
    tableCell: {
        backgroundColor: '#68debd'
    },
    tableCellTd: {
        backgroundColor: '#eeeeee',
        width: '50%'
    },
    tableCellTh: {
        width: '50%'
    },
    listItems: {
        padding: '0px',
        width: '100%'
    },
    listItem: {
        display: 'flex',
        borderBottom: '1px solid #9e9e9e',
        alignItems: 'center'
    },
    titleItem: {
        position: 'relative'
    },
    listItemText: {
        padding: 0
    },
    listItemTextListSubItem: {
        fontSize: '0.75rem',
        color: '#9e9e9e'
    },
});

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class DialogDetailDataDriver extends Component {
    pdfExportComponent;

    state = {
        data: {},
    };

    componentWillMount = () => {
        this.getData(this.props.id);
    };

    getData = id => {

        this._isMounted = true;
        http
            .get(`/api/drivers/${id}`)
            .then(result => {
                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    };

    handleClose = () => {
        this.props.handleClose();
    };

    exportPDFWithComponent = () => {
        this.pdfExportComponent.save();
    };

    render() {
        const { classes, open, title } = this.props;
        const { data } = this.state;

        return (
            <Dialog
                fullScreen
                open={open}
                onClose={this.handleClose}
                TransitionComponent={Transition}
            >
                <AppBar position="relative" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            onClick={this.handleClose}
                            aria-label="Close"
                        >
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" className={classes.flex}>
                            {title}
                        </Typography>
                        <div className={classes.row}>
                            <Tooltip title="Exportar" placement="top">
                                <IconButton
                                    aria-haspopup="true"
                                    onClick={this.exportPDFWithComponent}
                                    color="inherit"
                                >
                                    <SaveAltIcon />
                                </IconButton>
                            </Tooltip>
                        </div>
                    </Toolbar>
                </AppBar>
                <PDFExport
                    paperSize="A4"
                    scale={0.5}
                    fileName={`CONDUCTOR-IDENTIFICACIÓN(${data.identification}).pdf`}
                    ref={component => (this.pdfExportComponent = component)}
                >
                    {JSON.stringify(data) != '{}' ? (
                        <div className={classes.body}>
                            <Grid container spacing={24} >
                                <Grid item xs={12} sm={6} md={6} >
                                    <Paper className={classes.paper}>
                                        <Table className={classes.table}>
                                            <TableBody>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        {ID}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.id}
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        {DATE}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.createdAt}
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12} sm={6} md={6}>
                                    <Paper className={classes.paper}>
                                        <Table className={classes.table}>
                                            <TableBody>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        Conductor
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.name + " " + data.lastName}
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        Identificación
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.TypesIdentification.type + ":" + data.identification}
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </Paper>
                                </Grid>

                                {data.dataDriver.map((itemDataDriver, index) => {
                                    return Object.keys(itemDataDriver).map(item =>
                                        <Grid key={item} item xs={12} sm={12} md={12}>
                                            <Paper className={classes.paper}>
                                                <Typography
                                                    variant="subtitle1"
                                                    color="secondary"
                                                    noWrap
                                                    gutterBottom
                                                    className={classes.subtitle}
                                                >
                                                    {item}
                                                </Typography>
                                                <Grid container spacing={24}>
                                                    {itemDataDriver[item].map((subItem, index) => {

                                                        for (let [key, value] of Object.entries(subItem)) {
                                                            return <Grid item xs={12} sm={4} md={4} key={index}>
                                                                <Table className={classes.table}>
                                                                    <TableBody>
                                                                        <TableRow>
                                                                            <TableCell className={classes.tableCellTd}>
                                                                                {value.inputTitle}
                                                                            </TableCell>
                                                                            <TableCell className={classes.tableCellTh} component="th" scope="row">
                                                                                {value.value}
                                                                            </TableCell>
                                                                        </TableRow>
                                                                    </TableBody>
                                                                </Table>
                                                            </Grid>
                                                        }
                                                    })}
                                                </Grid>
                                            </Paper>
                                        </Grid>
                                    )
                                })}
                            </Grid>
                        </div>
                    ) : null}
                </PDFExport>
            </Dialog>
        );
    }
}

DialogDetailDataDriver.propTypes = {
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired
};

export default withStyles(styles)(DialogDetailDataDriver);
