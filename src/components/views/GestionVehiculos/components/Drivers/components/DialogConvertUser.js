//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames'

//Material UI
import {
    withStyles,
    Dialog,
    FormControl,
    InputLabel,
    FormHelperText,
    TextField,
    FormControlLabel,
    Switch,
    Button,
    Typography,
    IconButton,
    Grid,
    Select,
    Input,
    Chip,
    MenuItem,
    Divider,
    Paper
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Icons 
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';

//Utils
import {
    CAMPOSOBLIGATORIOS,
    PROFILE,
    CRUDADD,
    NAME,
    APELLIDO,
    PHONE,
    EMAIL,
    PASS,
    PASSCONFIRM,
    ROLES,
    CRUDUPDATE,
    BUTTONADD,
    ROLE,
    PROFILES,
    PASSOLD,
    PASSNEW,
    CRUDCONVERTUSER,
    MESSAGECONVERTUSER,
    MESSAGEEXISTSCONVERTUSER,
    BUTTONOK
} from '../../../../../../utils/constants'
import http from '../../../../../../utils/http';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    containerSectionForm: {
        padding: theme.spacing.unit,
        marginTop: theme.spacing.unit + 2,
        marginBottom: theme.spacing.unit * 2
    },
    divider: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2
    },
    textField: {
        color: '#9e9e9e',
        fontSize: '0.75rem'
    },
    iconTitleDialog: {
        marginRight: 10
    },
    subtitle: {
        fontWeight: '500'
    },
    rootSuccess: {
        padding: theme.spacing.unit * 2,
        backgroundColor: theme.palette.secondary.main
    },
    rootDanger: {
        padding: theme.spacing.unit * 2,
        backgroundColor: '#ffa000'
    },
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250
        }
    }
};

function getStyles(role, that) {
    return {
        fontWeight:
            that.state.dataRoles.indexOf(role) === -1
                ? that.props.theme.typography.fontWeightRegular
                : that.props.theme.typography.fontWeightMedium
    };
}

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
        justifyContent: 'space-between'
    }
}))(MuiDialogActions);


class DialogConvertUser extends Component {

    state = {
        name: '',
        lastName: '',
        email: '',
        homePhone: '',
        pass: '',
        passConfirm: '',
        status: true,
        idUser: '',

        dataProfiles: [],
        idProfile: '',
        profile: 'Conductor',

        dataRoles: [],
        roles: [],
        role: ['App'],

        errors: {},
    };

    componentWillMount = () => {
        this.getDataProfiles();
        this.getDataRoles();
        this.setValueState();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getDataProfiles = () => {
        this._isMounted = true;
        http
            .get('/api/profiles/profilesuser')
            .then(result => {
                if (this._isMounted) {
                    this.setState({ dataProfiles: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    getDataRoles = () => {
        this._isMounted = true;
        http
            .get('/api/roles/rolesuser')
            .then(result => {
                if (this._isMounted) {
                    this.setState({ dataRoles: result.data.data });
                    const roles = this.state.dataRoles.map(item => item.role);
                    this.setState({ roles: roles });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    setValueState = () => {

        let name = this.props.data.name !== null ? this.props.data.name : ''
        let lastName = this.props.data.lastName !== null ? this.props.data.lastName : ''
        let email = this.props.data.email !== null ? this.props.data.email : ''
        let homePhone = this.props.data.homePhone !== null ? this.props.data.homePhone : ''
        let idUser = this.props.data.idUser !== null ? this.props.data.idUser : ''
        let password = this.props.data.pass !== null ? this.props.data.pass : ''
        let passConfirm = this.props.data.passConfirm !== null ? this.props.data.passConfirm : ''

        this.setState({
            name: name,
            lastName: lastName,
            email: email,
            homePhone: homePhone,
            idUser: idUser,
            pass: password,
            passConfirm: passConfirm
        })
    }

    handleClose = () => {
        this.props.handleClose(CRUDCONVERTUSER)
    }

    handleChange = e => {

        if (e.target.id === 'status') {
            this.setState({ [e.target.id]: e.target.checked });
        }

        const errors = this.state.errors

        switch (e.target.name) {
            case 'name':
                delete errors.name
                break;
            case 'email':
                delete errors.email
                break;

            case 'homePhone':
                delete errors.homePhone
                break;

            case 'pass':
                delete errors.password
                break;

            case 'passConfirm':
                delete errors.passwordConfirm
                break;

            case 'profile':
                delete errors.idProfile
                break;

            case 'role':
                delete errors.roles
                break;
            default:
                break;
        }
        this.setState({ errors: errors, [e.target.name]: e.target.value })
    };

    handleSubmit = e => {
        let idProfile = '';
        const status = this.state.status ? 1 : 0;
        const profile = this.state.dataProfiles.filter(
            item => item.profile === this.state.profile
        );

        if (profile.length > 0) {
            idProfile = profile[0].id;
        }

        const rolesAdd = [];
        let itemIdRole = {};
        this.state.role.map(role => {
            const arrayItem = Object.values(this.state.dataRoles).filter(
                item => item.role === role
            );
            let idRoleAddArray = arrayItem.map(item => item.id);
            if (idRoleAddArray[0]) {
                itemIdRole = { idRole: idRoleAddArray[0], status: 1 };
                rolesAdd.push(itemIdRole);
            }
        });
        const dataAdd = {
            name: this.state.name.toString(),
            password: this.state.pass.toString(),
            lastName: this.state.lastName.toString(),
            email: this.state.email.toString(),
            phone: this.state.homePhone.toString(),
            idProfile: idProfile.toString(),
            passwordConfirm: this.state.passConfirm.toString(),
            roles: rolesAdd,
            status: status.toString(),
            idUser: '',
            idDriver: this.props.data.idDriver
        };

        console.log("OUTPUT: DialogConvertUser -> dataAdd", dataAdd)

        http
            .post('/api/users', dataAdd)
            .then(result => {
                this.handleClose();
                this.props.backAfterInsert(CRUDADD, result.data.message);
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    render() {
        const { classes, title, open, data } = this.props

        const { errors,
            name,
            lastName,
            email,
            homePhone,
            pass,
            passConfirm,
            dataProfiles,
            profile,
            roles,
            role,
            status,
            idUser
        } = this.state

        return (
            <Dialog
                fullWidth
                maxWidth="sm"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />{' '}
                    {title}
                </DialogTitle>
                <DialogContent>
                    <div>
                        <Paper className={idUser ? classes.rootDanger : classes.rootSuccess}>
                            <Typography color="textPrimary" component="p">
                                {idUser ? MESSAGEEXISTSCONVERTUSER : MESSAGECONVERTUSER}
                            </Typography>
                        </Paper>
                    </div>
                    <div className={classes.containerSectionForm}>
                        <Typography
                            variant="subtitle1"
                            color="secondary"
                            noWrap
                            className={classes.subtitle}
                        >
                            Información General
                        </Typography>

                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoFocus
                                    margin="dense"
                                    id="name"
                                    name="name"
                                    value={name}
                                    label={NAME}
                                    error={errors.name ? true : false}
                                    fullWidth
                                    disabled
                                    onChange={this.handleChange}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.name}
                                </Typography>

                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <TextField
                                    disabled
                                    margin="dense"
                                    id="lastName"
                                    name="lastName"
                                    value={lastName}
                                    label={APELLIDO}
                                    error={errors.lastName ? true : false}
                                    fullWidth
                                    onChange={this.handleChange}
                                />
                                <Typography color="error" variant="caption">
                                    {errors.lastName}

                                </Typography>

                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <TextField
                                    disabled
                                    margin="dense"
                                    id="email"
                                    name="email"
                                    value={email}
                                    label={EMAIL}
                                    error={errors.email ? true : false}
                                    fullWidth
                                    onChange={this.handleChange}
                                    required
                                    helperText="Recuerda que 'Correo' es tu usuario y es unico en la plataforma"
                                />
                                <Typography color="error" variant="caption">
                                    {errors.email}

                                </Typography>

                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <TextField
                                    disabled
                                    margin="dense"
                                    id="homePhone"
                                    name="homePhone"
                                    value={homePhone}
                                    label={PHONE}
                                    error={errors.homePhone ? true : false}
                                    fullWidth
                                    onChange={this.handleChange}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.homePhone}
                                </Typography>
                            </Grid>
                        </Grid>
                    </div>

                    <Divider className={classes.divider} />

                    <div className={classes.containerSectionForm}>
                        <Typography
                            variant="subtitle1"
                            color="secondary"
                            noWrap
                            className={classes.subtitle}
                        >
                            Acceso y Privilegio
                        </Typography>

                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>
                                <FormControl
                                    error={errors.idProfile ? true : false}
                                    fullWidth
                                    required
                                    className={classes.formControl}
                                >
                                    <InputLabel
                                        required
                                        error={errors.idProfile ? true : false}
                                        htmlFor="profile"
                                    >
                                        {PROFILE}
                                    </InputLabel>
                                    <Select
                                        disabled
                                        error={errors.idProfile ? true : false}
                                        value={profile}
                                        onChange={this.handleChange}
                                        name="profile"
                                        id="profile"
                                        inputProps={{
                                            id: 'profile'
                                        }}
                                        className={classes.selectEmpty}
                                    >
                                        <MenuItem value="">
                                            <em>Ninguno</em>
                                        </MenuItem>
                                        {dataProfiles.map(profile => (
                                            <MenuItem key={profile.id} value={profile.profile}>
                                                {profile.profile}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                                <Typography color="error" variant="caption">
                                    {errors.idProfile}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <FormControl disabled={idUser ? true : false} fullWidth className={classes.formControl}>
                                    <InputLabel
                                        required
                                        error={errors.roles ? true : false}
                                        htmlFor="role"
                                    >
                                        {ROLES}
                                    </InputLabel>
                                    <Select
                                        disabled
                                        multiple
                                        id="role"
                                        name="role"
                                        required
                                        error={errors.roles ? true : false}
                                        value={role}
                                        onChange={this.handleChange}
                                        input={<Input id="role" name="role" />}
                                        renderValue={selected => (
                                            <div className={classes.chips}>
                                                {selected.map(value => (
                                                    <Chip
                                                        key={value}
                                                        label={value}
                                                        className={classes.chip}
                                                    />
                                                ))}
                                            </div>
                                        )}
                                        MenuProps={MenuProps}
                                    >
                                        {roles.map(name => (
                                            <MenuItem
                                                key={name}
                                                value={name}
                                                style={getStyles(name, this)}
                                            >
                                                {name}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                                <Typography color="error" variant="caption">
                                    {errors.roles}
                                </Typography>
                            </Grid>
                        </Grid>
                    </div>

                    <Divider className={classes.divider} />

                    <div className={classes.containerSectionForm}>

                        <Typography
                            variant="subtitle1"
                            color="secondary"
                            noWrap
                            className={classes.subtitle}
                        >
                            Agregar Contraseña
                    </Typography>
                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>
                                <TextField

                                    margin="dense"
                                    id="pass"
                                    name="pass"
                                    value={pass}
                                    label={PASS}
                                    disabled={idUser ? true : false}
                                    error={errors.password ? true : false}
                                    fullWidth
                                    onChange={this.handleChange}
                                    required
                                    type="password"
                                />
                                <Typography color="error" variant="caption">
                                    {errors.password}
                                </Typography>


                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <TextField
                                    margin="dense"
                                    id="passConfirm"
                                    name="passConfirm"
                                    value={passConfirm}
                                    label={PASSCONFIRM}
                                    disabled={idUser ? true : false}
                                    error={errors.passwordConfirm ? true : false}
                                    fullWidth
                                    onChange={this.handleChange}
                                    required
                                    type="password"
                                />
                                <Typography color="error" variant="caption">
                                    {errors.passwordConfirm}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <FormControlLabel
                                    disabled
                                    control={
                                        <Switch
                                            disabled
                                            required
                                            id="status"
                                            checked={this.state.status}
                                            onChange={this.handleChange}
                                            value="status"
                                        />
                                    }
                                    label="Estado"
                                />
                            </Grid>
                        </Grid>
                    </div>


                </DialogContent>
                <DialogActions>
                    <FormHelperText>{CAMPOSOBLIGATORIOS}</FormHelperText>
                    <Button onClick={idUser ? this.handleClose : this.handleSubmit} color="secondary">
                        {idUser ? BUTTONOK : BUTTONADD}
                    </Button>
                </DialogActions>
            </Dialog >
        )
    }
}

DialogConvertUser.propTypes = {
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    backAfterInsert: PropTypes.func.isRequired
}

export default withStyles(styles, { withTheme: true })(DialogConvertUser);

