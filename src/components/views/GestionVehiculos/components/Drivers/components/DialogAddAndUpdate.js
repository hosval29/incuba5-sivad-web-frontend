//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import esLocale from "date-fns/locale/es";
import classNames from 'classnames';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';

//Material UI
import {
    Dialog,
    FormHelperText,
    TextField,
    FormControlLabel,
    Switch,
    Button,
    Typography,
    IconButton,
    withStyles,
    Grid,
    FormControl,
    MenuItem,
    Select,
    InputLabel,
    Divider
} from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Material UI || Icons
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';

//Utils
import http from '../../../../../../store/actions/http';
import {
    CRUDUPDATE,
    CRUDADD,
    CAMPOSOBLIGATORIOS,
    DATABASIC,
    DATAOPERATIVE,
    DATALABORAL,
    INPUTSFORMDRIVER,
    BUTTONACEPT,
    BUTTONSAVE,
    BUTTONADD,

} from '../../../../../../utils/constants';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    },
    subtitle: {
        fontWeight: '500'
    },
    containerSectionForm: {

        padding: theme.spacing.unit
    },
    containerSectionFormColor: {
        backgroundColor: 'aliceblue',
        marginTop: '1.5rem',
        marginBottom: '1.5rem'
    },
    iconTitleDialog: {
        marginRight: 10
    },
    divider: {
        height: 2,
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2
    },
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
        justifyContent: 'space-between'
    }
}))(MuiDialogActions);

class DialogAddAndUpdate extends Component {

    state = {

        typeIdentfication: '',
        idTypeIdentification: '',
        dataTypeIdentification: [],
        identification: '',
        name: '',
        lastName: '',
        homeAddress: '',
        homePhone: '',
        cellPhoneNumber: '',
        email: '',
        placeBirth: '',
        dateBirth: new Date(),
        acedemicTraining: '',
        typeRH: '',
        status: true,

        numLicencia: '',
        categoryLicCon: '',
        dateExpirationLicCon: new Date(),

        typeContract: '',
        dateStartContract: new Date(),
        dateEndContract: new Date(),
        eps: '',
        fondoPensiones: '',
        fondoCesantias: '',
        arl: '',

        errors: {},
    };

    componentWillMount = () => {

        if (this.props.id) {
            this.getData(this.props.id);
        }

        this.getDataTypesIdentification()
    };

    componentWillUnmount = () => {

        this.setState({
            typeIdentfication: '',
            idTypeIdentification: '',
            dataTypeIdentification: [],
            identification: '',
            name: '',
            lastName: '',
            homeAddress: '',
            homePhone: '',
            cellPhoneNumber: '',
            email: '',
            placeBirth: '',
            dateBirth: new Date(),
            acedemicTraining: '',
            typeRH: '',
            status: true,

            numLicencia: '',
            categoryLicCon: '',
            dateExpirationLicCon: new Date(),

            typeContract: '',
            dateStartContract: new Date(),
            dateEndContract: new Date(),
            eps: '',
            fondoPensiones: '',
            fondoCesantias: '',
            arl: '',

            errors: {},

        })
    }

    getDataTypesIdentification = () => {
        http
        // .get('/api/typesidentification/register/enabled')
        .get('/api/typesidentification')
        .then((result) => {
            if (result) {
                this.setState({ dataTypeIdentification: result.data.data })
            }
        })
        .catch(err => {
            switch(err.response.status) {
                case 400:
                    this.setState({ errors: err.response.data })
                    break;
                case 404:
                    this.setState({ errors: err.response.data })
                    break;
                case 500:
                    this.setState({ errors: err.response.data })
                    break;
                default:
                    this.setState({ errors: {} })
                    break;
            }
        });
    }

    handleChangeDataBasic = e => {
        if (e.target.id == 'status') {
            this.setState({ status: e.target.checked });
        }

        const errors = this.state.errors;

        switch (e.target.name) {
            case 'typeIdentfication':
                delete errors.idTypeIdentification;
                break;
            case 'identification':
                delete errors.identification;
                break;
            case 'name':
                delete errors.name;
                break;
            case 'lastName':
                delete errors.lastName;
                break;
            case 'identification':
                delete errors.lastName;
                break;
            case 'homeAddress':
                delete errors.homeAddress;
                break;
            case 'homePhone':
                delete errors.homePhone;
                break;
            case 'cellPhoneNumber':
                delete errors.cellPhoneNumber;
                break;
            case 'email':
                delete errors.email;
                break;
            case 'email':
                delete errors.email;
                break;
            case 'placeBirth':
                delete errors.placeBirth;
                break;
            case 'dateBirth':
                delete errors.dateBirth;
                break;
            case 'acedemicTraining':
                delete errors.acedemicTraining;
                break;
            case 'typeRH':
                delete errors.typeRH;
                break;
            default:
                break;
        }

        this.setState({ errors: errors, [e.target.name]: e.target.value });
    }

    handleChangeDataOperational = e => {

        const errors = this.state.errors;

        switch (e.target.name) {
            case 'numLicencia':
                delete errors.numLicencia;
                break;
            case 'categoryLicCon':
                delete errors.categoryLicCon;
                break;
            case 'dateExpirationLicCon':
                delete errors.dateExpirationLicCon;
                break;
            default:
                break;
        }

        this.setState({ errors: errors, [e.target.name]: e.target.value });
    };

    handleChangeDataLabor = e => {

        const errors = this.state.errors;

        switch (e.target.name) {
            case 'typeContract':
                delete errors.typeContract;
                break;
            case 'dateStartContract':
                delete errors.dateStartContract;
                break;
            case 'dateEndContract':
                delete errors.dateEndContract;
                break;
            case 'eps':
                delete errors.eps;
                break;
            case 'fondoPensiones':
                delete errors.fondoPensiones;
                break;
            case 'fondoCesantias':
                delete errors.fondoCesantias;
                break;
            case 'arl':
                delete errors.arl;
                break;
            default:
                break;
        }

        this.setState({ errors: errors, [e.target.name]: e.target.value });
    }

    handleChangeDateBirth = date => {
        this.setState({ dateBirth: date });
    };

    handleChangeDateExpirationLicCon = date => {
        this.setState({ dateExpirationLicCon: date });
    };

    handleChangeDateStartContract = date => {
    console.log("OUTPUT: DialogAddAndUpdate -> date", date)
        this.setState({ dateStartContract: date });
    };

    handleChangeDateEndContract = date => {
    console.log("OUTPUT: DialogAddAndUpdate -> date", date)
        this.setState({ dateEndContract: date });
    };

    handleClose = () => {
        this.setState({
            typeIdentfication: '',
            idTypeIdentification: '',
            dataTypeIdentification: [],
            identification: '',
            name: '',
            lastName: '',
            homeAddress: '',
            homePhone: '',
            cellPhoneNumber: '',
            email: '',
            placeBirth: '',
            dateBirth: new Date(),
            acedemicTraining: '',
            typeRH: '',
            status: true,

            numLicencia: '',
            categoryLicCon: '',
            dateExpirationLicCon: new Date(),

            typeContract: '',
            dateStartContract: new Date(),
            dateEndContract: new Date(),
            eps: '',
            fondoPensiones: '',
            fondoCesantias: '',
            arl: '',

            errors: {}
        })
        this.props.handleClose(CRUDUPDATE);
    };

    handleSubmit = id => e => {
        const status = this.state.status ? 1 : 0;

        let idTypeIdentification = ''
        const typeIdentification = this.state.dataTypeIdentification.filter(itemTypeIdentification => itemTypeIdentification.type === this.state.typeIdentfication)

        if (typeIdentification.length > 0) {
            idTypeIdentification = typeIdentification[0].id
        }

        const dataDriver = []

        const dataBasicDriver = [
            { homeAddress: { value: this.state.homeAddress, inputTitle: 'Direccion Casa' } },
            { homePhone: { value: this.state.homePhone.toString().replace(/ /g, "").trim(), inputTitle: 'Teléfono Casa' } },
            { cellPhoneNumber: { value: this.state.cellPhoneNumber.toString().replace(/ /g, "").trim(), inputTitle: 'Número de Celular' } },
            { email: { value: this.state.email, inputTitle: 'Correo' } },
            { placeBirth: { value: this.state.placeBirth, inputTitle: 'Lugar de Nacimiento' } },
            { dateBirth: { value: this.state.dateBirth, inputTitle: 'Fecha de Nacimiento' } },
            { acedemicTraining: { value: this.state.acedemicTraining, inputTitle: 'Formación Academica' } },
            { typeRH: { value: this.state.typeRH, inputTitle: 'Tipo RH' } }
        ]

        const dataOperationalDriver = [
            { numLicencia: { value: this.state.numLicencia.replace(/ /g, "").trim(), inputTitle: 'Número Licencia de Conducción' } },
            { categoryLicCon: { value: this.state.categoryLicCon, inputTitle: 'Categoria Licencia de Conducción' } },
            { dateExpirationLicCond: { value: this.state.dateExpirationLicCon, inputTitle: 'Fecha Expiración Licencia de Conducción' } }
        ]

        const dataLaborDriver = [
            { typeContract: { value: this.state.typeContract, inputTitle: 'Tipo de Contrato' } },
            { dateInitContract: { value: this.state.dateStartContract, inputTitle: 'Fecha Inicio de Contrato' } },
            { dateEndContract: { value: this.state.dateEndContract, inputTitle: 'Fecha Fin de Contrato' } },
            { eps: { value: this.state.eps, inputTitle: 'EPS' } },
            { fondoPensiones: { value: this.state.fondoPensiones, inputTitle: 'Fondo de Pensiones' } },
            { fondoCesantias: { value: this.state.fondoCesantias, inputTitle: 'Fondo de Cesantias' } },
            { arl: { value: this.state.arl, inputTitle: 'ARL' } }
        ]

        dataDriver.push({ "Datos Básicos": dataBasicDriver })
        dataDriver.push({ "Datos Operacional": dataOperationalDriver })
        dataDriver.push({ "Datos Laboral": dataLaborDriver })

        const dataAdd = {
            idTypeIdentification: idTypeIdentification.toString(),
            identification: this.state.identification.toString().replace(/ /g, "").trim(),
            name: this.state.name,
            lastName: this.state.lastName,
            email: this.state.email.toString().replace(/ /g, "").trim(),
            homePhone: this.state.homePhone.toString().replace(/ /g, "").trim(),
            dataDriver: dataDriver,
            status: status.toString(),
        };
        
        console.log("OUTPUT: DialogAddAndUpdate -> dataAdd", dataAdd)

        if (!id) {
            http
                .post('/api/drivers', dataAdd)
                .then(result => {
                    this.handleClose();
                    this.props.backAfterInsert(CRUDADD, result.data.message);
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        } else {
            http
                .put(`/api/drivers/${id}`, dataAdd)
                .then(result => {
                    if (result) {
                        this.handleClose();
                        this.props.backAfterInsert(CRUDUPDATE, result.data.message);
                    }
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    getData = id => {
        if (id !== '') {
            http
                .get(`/api/drivers/${id}`)
                .then(result => {
                    const dataUpdate = result.data.data;

                    let status = false;
                    if (dataUpdate.status === 1) {
                        status = true;
                    }

                    let dataDriver = dataUpdate.dataDriver

                    let dataBasicDriver = []
                    let dataOperationalDriver = []
                    let dataLaborDriver = []

                    dataDriver.map(itemDataDriverInputShow => {
                        let items = Object.keys(itemDataDriverInputShow)
                        items.forEach(function (item) {
                            if (item === "Datos Básicos") {
                                dataBasicDriver = itemDataDriverInputShow[item]
                                console.log("OUTPUT: DialogAddAndUpdate -> dataBasicDriver", dataBasicDriver)
                            }

                            if (item === "Datos Operacional") {
                                dataOperationalDriver = itemDataDriverInputShow[item]
                            }

                            if (item === "Datos Laboral") {
                                dataLaborDriver = itemDataDriverInputShow[item]
                            }
                        })
                    })

                    this.setState({
                        idTypeIdentification: dataUpdate.idTypeIdentification,
                        identification: dataUpdate.identification,
                        name: dataUpdate.name,
                        lastName: dataUpdate.lastName,
                        status: status,
                        typeIdentfication: dataUpdate.TypesIdentification.type
                    })

                    dataBasicDriver.map(itemDataBasicDriver => {
                        if (itemDataBasicDriver.homeAddress) {
                            this.setState({ homeAddress: itemDataBasicDriver.homeAddress.value })
                        }
                        if (itemDataBasicDriver.homePhone) {
                            this.setState({ homePhone: itemDataBasicDriver.homePhone.value })
                        }

                        if (itemDataBasicDriver.cellPhoneNumber) {
                            this.setState({ cellPhoneNumber: itemDataBasicDriver.cellPhoneNumber.value })
                        }

                        if (itemDataBasicDriver.email) {
                            this.setState({ email: itemDataBasicDriver.email.value })
                        }

                        if (itemDataBasicDriver.placeBirth) {
                            this.setState({ placeBirth: itemDataBasicDriver.placeBirth.value })
                        }

                        if (itemDataBasicDriver.dateBirth) {
                            this.setState({ dateBirth: itemDataBasicDriver.dateBirth.value })
                        }

                        if (itemDataBasicDriver.acedemicTraining) {
                            this.setState({ acedemicTraining: itemDataBasicDriver.acedemicTraining.value })
                        }

                        if (itemDataBasicDriver.typeRH) {
                            this.setState({ typeRH: itemDataBasicDriver.typeRH.value })
                        }
                    })

                    dataOperationalDriver.map(itemDataOperationalDriver => {
                        if (itemDataOperationalDriver.numLicencia) {
                            this.setState({ numLicencia: itemDataOperationalDriver.numLicencia.value })
                        }

                        if (itemDataOperationalDriver.categoryLicCon) {
                            this.setState({ categoryLicCon: itemDataOperationalDriver.categoryLicCon.value })
                        }

                        if (itemDataOperationalDriver.dateExpirationLicCond) {
                            this.setState({ dateExpirationLicCon: itemDataOperationalDriver.dateExpirationLicCond.value })
                        }
                    })

                    dataLaborDriver.map(itemDataLaborDriver => {
                        if (itemDataLaborDriver.typeContract) {
                            this.setState({ typeContract: itemDataLaborDriver.typeContract.value })
                        }

                        if (itemDataLaborDriver.dateInitContract) {
                            this.setState({ dateStartContract: itemDataLaborDriver.dateInitContract.value })
                        }

                        if (itemDataLaborDriver.dateEndContract) {
                            this.setState({ dateEndContract: itemDataLaborDriver.dateEndContract.value })
                        }

                        if (itemDataLaborDriver.eps) {
                            this.setState({ eps: itemDataLaborDriver.eps.value })
                        }

                        if (itemDataLaborDriver.fondoPensiones) {
                            this.setState({ fondoPensiones: itemDataLaborDriver.fondoPensiones.value })
                        }

                        if (itemDataLaborDriver.fondoCesantias) {
                            this.setState({ fondoCesantias: itemDataLaborDriver.fondoCesantias.value })
                        }

                        if (itemDataLaborDriver.arl) {
                            this.setState({ arl: itemDataLaborDriver.arl.value })
                        }
                    })

                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    render() {
        const { classes, open, title, id } = this.props;
        const {
            status,
            typeIdentfication,
            dataTypeIdentification,
            identification,
            name,
            lastName,
            homeAddress,
            homePhone,
            cellPhoneNumber,
            email,
            acedemicTraining,
            dateBirth,
            placeBirth,
            typeRH,

            numLicencia,
            categoryLicCon,
            dateExpirationLicCon,

            typeContract,
            dateStartContract,
            dateEndContract,
            eps,
            fondoPensiones,
            fondoCesantias,
            arl,

            errors,
        } = this.state;


        return (
            <Dialog
                fullWidth
                maxWidth="md"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />{' '}
                    {id ? CRUDUPDATE : CRUDADD} {title}
                </DialogTitle>
                <DialogContent>
                    <div className={classes.containerSectionForm}>
                        <Typography
                            variant="subtitle1"
                            color="secondary"
                            noWrap
                            className={classes.subtitle}
                        >
                            {DATABASIC}
                        </Typography>
                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={4}>
                                <FormControl
                                    error={errors.idTypeIdentification ? true : false}
                                    fullWidth
                                    required
                                    className={classes.formControl}
                                >
                                    <InputLabel
                                        required
                                        error={errors.idTypeIdentification ? true : false}
                                        htmlFor="idTypeIdentification"
                                    >
                                        Tipo de Identificación
                                    </InputLabel>
                                    <Select
                                        autoFocus
                                        error={errors.idTypeIdentification ? true : false}
                                        value={typeIdentfication}
                                        onChange={this.handleChangeDataBasic}
                                        name="typeIdentfication"
                                        id="typeIdentfication"
                                        inputProps={{
                                            id: 'typeIdentfication'
                                        }}
                                        className={classes.selectEmpty}
                                    >
                                        <MenuItem value="">
                                            <em>Ninguno</em>
                                        </MenuItem>
                                        {dataTypeIdentification.map(itemIdentification => {
                                            return itemIdentification.id !== 2 ? (
                                            <MenuItem key={itemIdentification.id} value={itemIdentification.type}>
                                                {itemIdentification.type}
                                            </MenuItem>
                                            ): null
                                            }
                                        )}
                                    </Select>
                                </FormControl>
                                <Typography color="error" variant="caption">
                                    {errors.idTypeIdentification}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="identification"
                                    name="identification"
                                    value={identification}
                                    label="Identificación"
                                    error={errors.identification ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataBasic}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.identification}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField

                                    margin="dense"
                                    id="name"
                                    name="name"
                                    value={name}
                                    label="Nombre"
                                    error={errors.name ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataBasic}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.name}
                                </Typography>

                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField

                                    margin="dense"
                                    id="lastName"
                                    name="lastName"
                                    value={lastName}
                                    label="Apellido"
                                    error={errors.lastName ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataBasic}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.lastName}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="homeAddress"
                                    name="homeAddress"
                                    value={homeAddress}
                                    label="Dirección Casa"
                                    error={errors.homeAddress ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataBasic}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.homeAddress}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="homePhone"
                                    name="homePhone"
                                    value={homePhone}
                                    label="Teléfono Casa"
                                    error={errors.homePhone ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataBasic}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.homePhone}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="cellPhoneNumber"
                                    name="cellPhoneNumber"
                                    value={cellPhoneNumber}
                                    label="Teléfono Celular"
                                    error={errors.cellPhoneNumber ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataBasic}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.cellPhoneNumber}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField

                                    margin="dense"
                                    id="email"
                                    name="email"
                                    value={email}
                                    label="Correo"
                                    error={errors.email ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataBasic}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.email}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField

                                    margin="dense"
                                    id="placeBirth"
                                    name="placeBirth"
                                    value={placeBirth}
                                    label="Lugar de Nacimiento"
                                    error={errors.placeBirth ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataBasic}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.placeBirth}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                    <Grid container className={classes.grid} justify="space-around">
                                        <DatePicker
                                            id="dateBirth"
                                            margin="normal"
                                            fullWidth
                                            label="Fecha de Nacimiento"
                                            format="yyyy/MM/dd"
                                            required
                                            invalidDateMessage={errors.dateBirth}
                                            invalidLabel={errors.dateBirth}
                                            value={dateBirth}
                                            error={errors.dateBirth ? true : false}
                                            onChange={this.handleChangeDateBirth}
                                        />
                                    </Grid>
                                </MuiPickersUtilsProvider>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField

                                    margin="dense"
                                    id="acedemicTraining"
                                    name="acedemicTraining"
                                    value={acedemicTraining}
                                    label="Formación Académica"
                                    error={errors.acedemicTraining ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataBasic}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.acedemicTraining}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="typeRH"
                                    name="typeRH"
                                    value={typeRH}
                                    label="Tipo RH"
                                    error={errors.typeRH ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataBasic}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.typeRH}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4} >
                                <FormControlLabel
                                    control={
                                        <Switch
                                            required
                                            id="status"
                                            checked={status}
                                            onChange={this.handleChangeDataBasic}
                                            value="status"
                                        />
                                    }
                                    label="Estado"
                                />
                            </Grid>
                        </Grid>
                    </div>

                    <Divider className={classes.divider} />

                    <div className={classNames(classes.containerSectionForm, classes.containerSectionFormColor)}>
                        <Typography
                            variant="subtitle1"
                            color="secondary"
                            noWrap
                            className={classes.subtitle}
                        >
                            {DATAOPERATIVE}
                        </Typography>

                        <Grid container spacing={24}>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="numLicencia"
                                    name="numLicencia"
                                    value={numLicencia}
                                    label="Número Licencia de Conducción"
                                    error={errors.numLicencia ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataOperational}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.numLicencia}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="categoryLicCon"
                                    name="categoryLicCon"
                                    value={categoryLicCon}
                                    label="Categoria Licencia Conducción"
                                    error={errors.categoryLicCon ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataOperational}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.categoryLicCon}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                    <Grid container className={classes.grid} justify="space-around">
                                        <DatePicker
                                            id="dateExpirationLicCon"
                                            margin="normal"
                                            fullWidth
                                            label="Fecha Expiracion Lic. Con."
                                            format="yyyy/MM/dd"
                                            required
                                            invalidDateMessage={errors.dateExpirationLicCon}
                                            invalidLabel={errors.dateExpirationLicCon}
                                            value={dateExpirationLicCon}
                                            error={errors.dateExpirationLicCon ? true : false}
                                            onChange={this.handleChangeDateExpirationLicCon}
                                        />
                                    </Grid>
                                </MuiPickersUtilsProvider>
                            </Grid>
                        </Grid>
                    </div>

                    <Divider className={classes.divider} />

                    <div className={classes.containerSectionForm}>
                        <Typography
                            variant="subtitle1"
                            color="secondary"
                            noWrap
                            className={classes.subtitle}
                        >
                            {DATALABORAL}
                        </Typography>

                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="typeContract"
                                    name="typeContract"
                                    value={typeContract}
                                    label="Tipo de Contrato"
                                    error={errors.typeContract ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataLabor}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.typeContract}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                    <Grid container className={classes.grid} justify="space-around">
                                        <DatePicker
                                            id="dateStartContract"
                                            margin="normal"
                                            fullWidth
                                            label="Fecha Inicio Contrato"
                                            format="yyyy/MM/dd"
                                            required
                                            invalidDateMessage={errors.dateStartContract}
                                            invalidLabel={errors.dateStartContract}
                                            value={dateStartContract}
                                            error={errors.dateStartContract ? true : false}
                                            onChange={this.handleChangeDateStartContract}
                                        />
                                    </Grid>
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                    <Grid container className={classes.grid} justify="space-around">
                                        <DatePicker
                                            id="dateEndContract"
                                            margin="normal"
                                            fullWidth
                                            label="Fecha Fin Contrato"
                                            format="yyyy/MM/dd"
                                            required
                                            invalidDateMessage={errors.dateEndContract}
                                            invalidLabel={errors.dateEndContract}
                                            value={dateEndContract}
                                            error={errors.dateEndContract ? true : false}
                                            onChange={this.handleChangeDateEndContract}
                                        />
                                    </Grid>
                                </MuiPickersUtilsProvider>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="eps"
                                    name="eps"
                                    value={eps}
                                    label="EPS"
                                    error={errors.eps ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataLabor}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.eps}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="fondoPensiones"
                                    name="fondoPensiones"
                                    value={fondoPensiones}
                                    label="Fondo de Pensiones"
                                    error={errors.fondoPensiones ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataLabor}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.fondoPensiones}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="fondoCesantias"
                                    name="fondoCesantias"
                                    value={fondoCesantias}
                                    label="Fondo Cesantias"
                                    error={errors.fondoCesantias ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataLabor}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.fondoCesantias}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={4}>
                                <TextField
                                    margin="dense"
                                    id="arl"
                                    name="arl"
                                    value={arl}
                                    label="ARL"
                                    error={errors.arl ? true : false}
                                    fullWidth
                                    onChange={this.handleChangeDataLabor}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.arl}
                                </Typography>
                            </Grid>
                        </Grid>
                    </div>
                </DialogContent>
                {errors.message ? (
                    <Typography align="center" color="error" variant="caption">
                        {errors.message}
                    </Typography>
                ) : null}
                <DialogActions>
                    <FormHelperText>{CAMPOSOBLIGATORIOS}</FormHelperText>
                    <Button onClick={this.handleSubmit(id)} color="secondary">
                        {id ? BUTTONSAVE : BUTTONADD}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogAddAndUpdate.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.any.isRequired,
    backAfterInsert: PropTypes.func.isRequired,
    handleClose: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogAddAndUpdate);
