//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';
import { withRouter } from 'react-router';
import { compose } from 'recompose';
import { connect } from 'react-redux';

//Material UI
import {
    withStyles,
    createMuiTheme,
    MuiThemeProvider,
    Typography,
    IconButton
} from '@material-ui/core';

//Icons
import { FileEyeOutline } from 'mdi-material-ui';

//Components
import CustomToolbar from '../../../components/CustomToolbar';
import DialogAddAndUpdate from './components/DialogAddAndUpdate';
import SnackbarInfo from '../../../../../utils/snackbars/components/Snackbar/SnackbarInfo';
import CustomButtonGestionVehicle from '../../../components/CustomButtonGestionVehicle';
import DialogChangeStatus from './components/DialogChangeStatus';
import DialogInspectionEnabled from './components/DialogInspectionEnabled'

//Utils
import {
    //STATUSINACTIVO,
    // STATUSACTIVO,
    CRUDDELETE,
    CRUDUPDATE,
    CRUDADD,
    // GESTIONBUTTON,
    // TYPEIDENTIFICATION,
    VEHICLES,
    VEHICLE,
    GESTION_VEHICULOS,
    GESTIONBUTTONVEHICLE,
    DETAILDATAVEHICLE,
    CLOSEDIALOG,
    INSPECTIONDIABLED,
    INSPECTIONENABLED,
    TITLEINSPECTION,
    CLOSEDIALOGINSPECTIONENABLED
} from '../../../../../utils/constants';
import { setToolbar } from '../../../../../store/actions/authActions';
import http from '../../../../../store/actions/http';
import DialogDetailDataVehicle from './components/DialogDetailDataVehicle';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 3
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    }
});

class Vehicles extends Component {
    state = {
        errors: [],
        data: [],
        open: false,
        openDialogChangeStatus: false,
        openDialogDetailDataVehicle: false,
        openDialogInspectionEnabled: false,
        inspectionEnabled: '',
        _id: '',
        snackbarOpen: false,
        snackbarMessage: '',
        snackbarVariant: ''
    };

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            },
            overrides: {
                MuiToolbar: {
                    root: {
                        backgroundColor: '#f5f5f5',
                        marginTop: 10,
                        marginBottom: 10,
                        boxShadow:
                            '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)'
                    }
                },
                MUIDataTableHeadCell: {
                    fixedHeader: {
                        color: '#FFFFFF',
                        backgroundColor: '#00587c'
                    }
                }
            }
        });

    componentWillMount() {
        const toolbar = {
            title: GESTION_VEHICULOS,
            icon: ''
        };
        this.props.setToolbar(toolbar);
        this.getData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getData = () => {
        this._isMounted = true;
        http
            .get('/api/vehicles')
            .then(result => {
                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    };

    handleClickAdd = e => {
        this.setState({ open: !this.state.open });
    };

    handleClickButtonGestion = (gestion, id) => {
        switch (gestion) {
            case GESTIONBUTTONVEHICLE[3]:
                this.setState({ openDialogInspectionEnabled: !this.state.openDialogInspectionEnabled, inspectionEnabled: 1 })
                break;
            case GESTIONBUTTONVEHICLE[2]:
                this.setState({ openDialogInspectionEnabled: !this.state.openDialogInspectionEnabled, inspectionEnabled: 0 })
                break;
            case GESTIONBUTTONVEHICLE[1]:
                this.setState({
                    openDialogChangeStatus: !this.state.openDialogChangeStatus
                });
                break;
            case GESTIONBUTTONVEHICLE[0]:
                this.setState({ open: !this.state.open });
                break;
            default:
                break;
        }

        this.setState({ _id: id });
    };

    handleOnClose = action => {

        switch (action) {
            case CRUDUPDATE:
                this.setState({ open: !this.state.open });
                break;
            case CRUDDELETE:
                this.setState({
                    openDialogChangeStatus: !this.state.openDialogChangeStatus
                });
                break;
            case CLOSEDIALOG:
                this.setState({
                    openDialogDetailDataVehicle: !this.state.openDialogDetailDataVehicle
                });
                break;
            case CLOSEDIALOGINSPECTIONENABLED:
                this.setState({
                    openDialogInspectionEnabled: !this.state.openDialogInspectionEnabled,
                    inspectionEnabled: ''
                });
                break;
            default:
                break;
        }

        this.setState({
            data: [],
            _id: ''
        });


        this.getData()
    };

    backAfterInsert = (action, message) => {
        switch (action) {
            case CRUDADD:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'success',
                });
                break;
            case CRUDUPDATE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;
            case CRUDDELETE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'warning'
                });
                break;
            case INSPECTIONENABLED:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;
            case INSPECTIONDIABLED:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'warning'
                });
                break;
            default:
                break;
        }

        this.setState({
            data: [],
            open: false,
            openDialogChangeStatus: false,
            openDialogDetailDataVehicle: false,
            openDialogInspectionEnabled: false,
            inspectionEnabled: '',
            _id: ''
        });

        this.getData();
    };

    handleCloseSnackbar = e => {
        this.setState({ snackbarOpen: !this.state.snackbarOpen });
    };

    render() {
        const { classes } = this.props;
        const {
            data,
            open,
            openDialogChangeStatus,
            openDialogDetailDataVehicle,
            openDialogInspectionEnabled,
            inspectionEnabled,
            _id
        } = this.state;

        const columns = [
            { name: 'id', label: 'Id' },
            { name: 'createdAt', label: 'Fecha Creación' },
            { name: 'plate', label: 'Placa' },
            { name: 'mileageInitial', label: 'Kilometraje Inicial' },
            { name: 'mileageCurrent', label: 'Kilometraje Actual' },
            {
                name: 'id',
                label: 'Detalle Vehículo',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <IconButton
                                onClick={() => this.setState({
                                    openDialogDetailDataVehicle: !this.state.openDialogDetailDataVehicle,
                                    _id: value
                                })}
                            >
                                <FileEyeOutline />
                            </IconButton>
                        );
                    }
                }
            },
            {
                name: 'TypesInspectionVehicle',
                label: 'Tipo Inspección',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    customBodyRender: value => value.type
                }
            },
            {
                name: 'VehiclesState',
                label: 'Estado Vehículo',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    customBodyRender: (value, tableMeta, updateValue) => (
                        <Typography style={{ color: '#1976d2' }} variant="subtitle2">
                            {value.state}
                        </Typography>
                    )
                }
            },
            {
                name: 'inspectionEnabled',
                label: 'Modo Inspección',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    customBodyRender: (value, tableMeta, updateValue) =>
                        (value === 1 ? <Typography color="secondary" variant="caption">
                            Habilitada
                        </Typography> : <Typography color="error" variant="caption">
                                Inhabilitada
                            </Typography>)
                }
            },
            {
                name: 'id',
                label: 'Gestión',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <CustomButtonGestionVehicle
                                id={value}
                                handleClickButtonGestion={this.handleClickButtonGestion}
                            />
                        );
                    }
                }
            }
        ];

        const options = {
            filterType: 'dropdown',
            responsive: 'scroll',
            selectableRows: false,
            isRowSelectable: false,
            rowsSelected: false,
            textLabels: {
                body: {
                    noMatch: 'Lo sentimos, no se encontraron registros coincidentes',
                    toolTip: "Ordenar",
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                }
            },
            customToolbar: () => {
                return (
                    <CustomToolbar title={VEHICLE} handleClickAdd={this.handleClickAdd} />
                );
            }
        };

        return (
            <div className={classes.root}>
                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable
                        title={VEHICLES}
                        data={data}
                        columns={columns}
                        options={options}
                    />
                </MuiThemeProvider>
                {open ? (
                    <DialogAddAndUpdate
                        open={open}
                        backAfterInsert={this.backAfterInsert}
                        id={_id}
                        title={VEHICLE}
                        handleOnClose={this.handleOnClose}
                    />
                ) : null}
                {openDialogChangeStatus ? (
                    <DialogChangeStatus
                        open={openDialogChangeStatus}
                        backAfterInsert={this.backAfterInsert}
                        id={_id}
                        title={VEHICLE}
                        handleOnClose={this.handleOnClose}
                    />
                ) : null}
                {this.state.snackbarOpen ? (
                    <SnackbarInfo
                        open={this.state.snackbarOpen}
                        onClose={this.handleCloseSnackbar}
                        variant={this.state.snackbarVariant}
                        message={this.state.snackbarMessage}
                    />
                ) : null}
                {openDialogDetailDataVehicle ? (
                    <DialogDetailDataVehicle
                        open={openDialogDetailDataVehicle}
                        title={DETAILDATAVEHICLE}
                        handleClose={this.handleOnClose}
                        id={_id}
                    />
                ) : null}
                {openDialogInspectionEnabled ? (
                    <DialogInspectionEnabled
                        open={openDialogInspectionEnabled}
                        title={TITLEINSPECTION}
                        handleOnClose={this.handleOnClose}
                        id={_id}
                        inspectionEnabled={inspectionEnabled}
                        backAfterInsert={this.backAfterInsert}
                    />
                ) : null}
            </div>
        );
    }
}

Vehicles.propTypes = {
    classes: PropTypes.object.isRequired,
    setToolbar: PropTypes.func.isRequired
};

const mapStateToProps = state => ({});

export default compose(
    withStyles(styles),
    connect(
        mapStateToProps,
        { setToolbar }
    ),
    withRouter
)(Vehicles);
