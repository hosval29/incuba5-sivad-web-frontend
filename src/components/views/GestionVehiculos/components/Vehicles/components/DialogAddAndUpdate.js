//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import esLocale from "date-fns/locale/es";
import classNames from 'classnames';

//Material UI
import {
    Dialog,
    FormHelperText,
    TextField,
    FormControlLabel,
    // Switch,
    Button,
    Typography,
    IconButton,
    withStyles,
    Grid,
    FormControl,
    MenuItem,
    Select,
    InputLabel,
    Divider,
    Radio,
    RadioGroup,
    FormLabel,
    Checkbox
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import {
    MuiPickersUtilsProvider,
    DatePicker
} from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';

//Components
import CustomTextInput from '../../../../components/CustomTextInput';
import CustomSelectInput from '../../../../components/CustomSelectInput'
import CustomAutoComplete from '../../../../components/CustomAutoComplete'

//Utils
import http from '../../../../../../store/actions/http';
import {
    CRUDUPDATE,
    CRUDADD,
    CAMPOSOBLIGATORIOS,
    OWNER,
    DATAVEHICLE,
    ALERTS,
    // LEYENDOWNERVEHICLE,
    CITYREGISTRATION,
    BUTTONADD,
    BUTTONSAVE,
    LINKAGE
} from '../../../../../../utils/constants';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    },
    subtitle: {
        fontWeight: '500'
    },
    containerSectionForm: {

        padding: theme.spacing.unit
    },
    containerSectionFormColor: {
        backgroundColor: 'aliceblue',
        marginTop: '1.5rem',
        marginBottom: '1.5rem'
    },
    divider: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        width: '80%'
    },
    formHelperText: {
        marginBottom: theme.spacing.unit * 2,
        marginTop: theme.spacing.unit * 3
    },
    buttons: {
        marginTop: theme.spacing.unit * 2
    },
    /* formControl: {
        display: 'flex',
        flexDirection: 'row'
    }, */
    group: {
        margin: `${theme.spacing.unit}px 0`,
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    rootGrid: {
        flexGrow: 1,
    },
    title: {
        color: theme.palette.text.primary
    },
    iconTitleDialog: {
        marginRight: 10
    },
    divider: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        height: 3
    },
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
        justifyContent: 'space-between'
    }
}))(MuiDialogActions);

class DialogAddAndUpdate extends Component {
    state = {
        plate: '',
        model: '',
        color: '',
        mileage: '',
        numberRegistration: '',
        dataCities: [],
        idCity: '',
        city: '',
        //type: '',
        brand: '',
        typeBody: '',
        line: '',
        satelliteCompany: '',
        capacityPassenger: '',
        capacityKLS: '',
        displacement: '',
        numberEngine: '',
        numberChais: '',
        numberLicense: '',
        vin: '',
        dataStatusVehicles: [],
        statusVehicle: '',
        idStatusVehicle: "",
        dataTypesInspection: [],
        typeInspection: '',
        idTypeInspection: '',

        typeDirection: '',
        typeTransmission: '',
        typeTraction: '',
        frontSuspension: '',
        backSuspension: '',
        wheelsMaterial: '',
        numberTires: '',

        numbersCylinders: '',
        fuel: '',
        turbo: '',
        orientation: '',

        typeFrontBrakes: '',
        typeBackBrakes: '',

        dataLinkage: [],
        idLinkage: '',
        linkage: '',

        dataOwners: [],
        idOwner: '',
        owner: '',

        checkBoxRegistration: '',
        dateExpirationRegistration: new Date(),
        visibilityRegistration: '',

        checkBoxSoat: '',
        aseguradoraSoat: '',
        dateExpirationSoat: new Date(),
        visibilitySoat: '',

        checkBoxPolizaTodoRiesgo: '',
        aseguradoraPolizaTodoRiesgo: '',
        numberPolizaPolizaTodoRiesgo: '',
        dateExpirationPolizaTodoRiesgo: new Date(),
        visibilityPolizaTodoRiesgo: '',

        checkBoxRevisionTecnoMecanica: '',
        cdaRevisionTecnoMecanica: '',
        numberRevisionRevisionTecnoMecanica: '',
        dateExpirationRevisionTecnoMecanica: new Date(),
        visibilityRevisionTecnoMecanica: '',

        checkBoxCambioAceite: '',
        dateExpirationCambioAceite: new Date(),
        dateRealizatationCambioAceite: new Date(),
        visibilityCambioAceite: '',

        checkBoxSynchronization: '',
        dateExpirationSynchronization: new Date(),
        dateRealizatationSynchronization: new Date(),
        visibilitySynchronization: '',

        checkBoxAlineacionBalanceo: '',
        dateExpirationAlineacionBalanceo: new Date(),
        dateRealizatationAlineacionBalanceo: new Date(),
        visibilityAlineacionBalanceo: '',

        checkBoxCambioLlantas: '',
        dateExpirationCambioLLantas: new Date(),
        dateRealizatationCambioLLantas: new Date(),
        visibilityCambioLLantas: '',

        errors: {}
    };

    componentWillMount = () => {

        if (this.props.id) {
            this.getDataUpdate(this.props.id);
        }

        this.getDataCities()
        this.getDataStatusVehicles()
        this.getDataLinkage()
        this.getDataOwners()
        this.getDataTypesInspections()
    };

    getDataCities = () => {
        http
            .get('/api/cities')
            .then(result => {
                this.setState({ dataCities: result.data.data });
            })
            .catch(err => {
                switch (err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    }

    getDataLinkage = () => {
        http
            .get('/api/typeslinks')
            .then(result => {
                this.setState({ dataLinkage: result.data.data });
            })
            .catch(err => {
                switch (err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    }

    getDataStatusVehicles = () => {
        http
            .get('/api/vehiclesstates/vehicles')
            .then(result => {
                this.setState({ dataStatusVehicles: result.data.data });
            })
            .catch(err => {
                switch (err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    }

    getDataOwners = () => {
        http
            .get('/api/owners/vehicles')
            .then(result => {
                this.setState({ dataOwners: result.data.data });
            })
            .catch(err => {
                switch (err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    }

    getDataTypesInspections = () => {
        http
            .get('/api/typesvehicularinspection')
            .then(result => {
                if (result) {
                    if (result.data.data) {
                        this.setState({ dataTypesInspection: result.data.data });
                    } else {
                        this.setState({ dataTypesInspection: [], errors: result.data });
                    }
                }
            })
            .catch(err => {
                switch (err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    }

    getDataUpdate = id => {
        console.log("OUTPUT: DialogAddAndUpdate -> id", id)
        if (id !== '') {
            http
                .get(`/api/vehicles/${id}`)
                .then(result => {

                    const dataUpdate = result.data.data

                    const dataVehicle = dataUpdate.dataVehicle
                    let dataBasicVehicle = []
                    let dataDirTraSusVehicle = []
                    let dataEngineVehicle = []
                    let dataBrakesVehicle = []
                    let dataOwnerVehicle = []
                    let dataAlertVehicle = []

                    dataVehicle.map(itemsDataVehicles => {
                        let items = Object.keys(itemsDataVehicles)
                        items.forEach(function (item) {
                            if (item === "Datos Basicos Vehículo") {
                                dataBasicVehicle = itemsDataVehicles[item]
                            }

                            if (item === "Direccion || Transmisión || Suspensión") {
                                dataDirTraSusVehicle = itemsDataVehicles[item]
                            }

                            if (item === "Motor") {
                                dataEngineVehicle = itemsDataVehicles[item]
                            }

                            if (item === "Frenos") {
                                dataBrakesVehicle = itemsDataVehicles[item]
                            }

                            if (item === "Propietario") {
                                dataOwnerVehicle = itemsDataVehicles[item]
                            }

                            if (item === "Alertas") {
                                dataAlertVehicle = itemsDataVehicles[item]
                            }
                        })
                    })

                    //Valido los campos de Data Basicos
                    dataBasicVehicle.map(itemDataBasicVehicle => {
                        if (itemDataBasicVehicle.plate) {
                            this.setState({ plate: itemDataBasicVehicle.plate.value })
                        }

                        if (itemDataBasicVehicle.model) {
                            this.setState({ model: itemDataBasicVehicle.model.value })
                        }

                        if (itemDataBasicVehicle.color) {
                            this.setState({ color: itemDataBasicVehicle.color.value })
                        }

                        if (itemDataBasicVehicle.mileage) {
                            this.setState({ mileage: itemDataBasicVehicle.mileage.value })
                        }

                        if (itemDataBasicVehicle.numberRegistration) {
                            this.setState({ numberRegistration: itemDataBasicVehicle.numberRegistration.value })
                        }

                        if (itemDataBasicVehicle.city) {
                            this.setState({ city: itemDataBasicVehicle.city.value })
                        }

                        /*if (itemDataBasicVehicle.type) {
                            this.setState({ type: itemDataBasicVehicle.type.value })
                        }*/

                        if (itemDataBasicVehicle.brand) {
                            this.setState({ brand: itemDataBasicVehicle.brand.value })
                        }

                        if (itemDataBasicVehicle.typeBody) {
                            this.setState({ typeBody: itemDataBasicVehicle.typeBody.value })
                        }

                        if (itemDataBasicVehicle.line) {
                            this.setState({ line: itemDataBasicVehicle.line.value })
                        }

                        if (itemDataBasicVehicle.satelliteCompany) {
                            this.setState({ satelliteCompany: itemDataBasicVehicle.satelliteCompany.value })
                        }

                        if (itemDataBasicVehicle.capacityPassenger) {
                            this.setState({ capacityPassenger: itemDataBasicVehicle.capacityPassenger.value })
                        }

                        if (itemDataBasicVehicle.capacityKLS) {
                            this.setState({ capacityKLS: itemDataBasicVehicle.capacityKLS.value })
                        }

                        if (itemDataBasicVehicle.displacement) {
                            this.setState({ displacement: itemDataBasicVehicle.displacement.value })
                        }

                        if (itemDataBasicVehicle.numberEngine) {
                            this.setState({ numberEngine: itemDataBasicVehicle.numberEngine.value })
                        }

                        if (itemDataBasicVehicle.numberChais) {
                            this.setState({ numberChais: itemDataBasicVehicle.numberChais.value })
                        }

                        if (itemDataBasicVehicle.numberLicense) {
                            this.setState({ numberLicense: itemDataBasicVehicle.numberLicense.value })
                        }

                        if (itemDataBasicVehicle.vin) {
                            this.setState({ vin: itemDataBasicVehicle.vin.value })
                        }

                        if (itemDataBasicVehicle.statusVehicle) {
                            this.setState({ statusVehicle: itemDataBasicVehicle.statusVehicle.value })
                        }
                    })

                    /**
                     * Seteamos los estados de DirTraSus
                     */
                    dataDirTraSusVehicle.map(item => {

                        if (item.typeDirection) {
                            this.setState({ typeDirection: item.typeDirection.value })
                        }

                        if (item.typeTransmission) {
                            this.setState({ typeTransmission: item.typeTransmission.value })
                        }

                        if (item.typeTraction) {
                            this.setState({ typeTraction: item.typeTraction.value })
                        }

                        if (item.frontSuspension) {
                            this.setState({ frontSuspension: item.frontSuspension.value })
                        }

                        if (item.backSuspension) {
                            this.setState({ backSuspension: item.backSuspension.value })
                        }

                        if (item.wheelsMaterial) {
                            this.setState({ wheelsMaterial: item.wheelsMaterial.value })
                        }

                        if (item.numberTires) {
                            this.setState({ numberTires: item.numberTires.value })
                        }
                    })

                    /**
                    * Seteamos los estados de DirTraSus
                    */
                    dataEngineVehicle.map(item => {

                        if (item.numbersCylinders) {
                            this.setState({ numbersCylinders: item.numbersCylinders.value })
                        }

                        if (item.fuel) {
                            this.setState({ fuel: item.fuel.value })
                        }

                        if (item.turbo) {
                            this.setState({ turbo: item.turbo.value })
                        }

                        if (item.orientation) {
                            this.setState({ orientation: item.orientation.value })
                        }
                    })

                    /**
                     * Seteamos los estados de DirTraSus
                     */
                    dataBrakesVehicle.map(item => {

                        if (item.typeFrontBrakes) {
                            this.setState({ typeFrontBrakes: item.typeFrontBrakes.value })
                        }

                        if (item.typeBackBrakes) {
                            this.setState({ typeBackBrakes: item.typeBackBrakes.value })
                        }
                    })

                    /**
                     * Seteamos los estados de DirTraSus
                     */
                    dataOwnerVehicle.map(item => {
                        if (item.owner) {
                            this.setState({ owner: item.owner.value })
                        }

                        if (item.linkage) {
                            this.setState({ linkage: item.linkage.value })
                        }
                    })

                    //Este segmento de code es para el setState de las Alertas
                    let dataAlertMatricula = {}
                    let dataAlertSoat = {}
                    let dataAlertPolizaTodoRiesgo = {}
                    let dataAlertRevisionTecnoMecanica = {}
                    let dataAlertCambioAceite = {}
                    let dataAlertSincronizacion = {}
                    let dataAlertAlineacionBalanceo = {}
                    let dataAlertCambioLlantas = {}

                    dataAlertVehicle.map(itemsAlerts => {
                        let items = Object.keys(itemsAlerts)
                        items.forEach(function (alert) {
                            if (alert === "matricula") {
                                dataAlertMatricula = itemsAlerts[alert]
                            }

                            if (alert === "soat") dataAlertSoat = itemsAlerts[alert]

                            if (alert === "polizaTodoRiesgo") dataAlertPolizaTodoRiesgo = itemsAlerts[alert]

                            if (alert === "revisionTecnoMecanica") dataAlertRevisionTecnoMecanica = itemsAlerts[alert]

                            if (alert === "cambioAceite") dataAlertCambioAceite = itemsAlerts[alert]

                            if (alert === "sincronizacion") dataAlertSincronizacion = itemsAlerts[alert]

                            if (alert === "alineacionBalanceo") dataAlertAlineacionBalanceo = itemsAlerts[alert]

                            if (alert === "cambioLlantas") dataAlertCambioLlantas = itemsAlerts[alert]

                        })
                    })

                    /**
                     * Seteamos los estados de Alertas Matricula
                     */
                    if (dataAlertMatricula.checkbox) {
                        this.setState({ checkBoxRegistration: dataAlertMatricula.checkbox.value })
                    }

                    if (dataAlertMatricula.dateExpiration) {
                        this.setState({ dateExpirationRegistration: dataAlertMatricula.dateExpiration.value })
                    }

                    if (dataAlertMatricula.visibility) {
                        this.setState({ visibilityRegistration: dataAlertMatricula.visibility.value })
                    }

                    /**
                     * Seteamos los estados de Alertas Soat
                     */
                    if (dataAlertSoat.checkbox) {
                        this.setState({ checkBoxSoat: dataAlertSoat.checkbox.value })
                    }

                    if (dataAlertSoat.aseguradora) {
                        this.setState({ aseguradoraSoat: dataAlertSoat.aseguradora.value })
                    }

                    if (dataAlertSoat.dateExpiration) {
                        this.setState({ dateExpirationSoat: dataAlertSoat.dateExpiration.value })
                    }

                    if (dataAlertSoat.visibility) {
                        this.setState({ visibilitySoat: dataAlertSoat.visibility.value })
                    }

                    /**
                     * Seteamos los estados de Poliza Todo Riesgo
                     */
                    if (dataAlertPolizaTodoRiesgo.checkbox) {
                        this.setState({ checkBoxPolizaTodoRiesgo: dataAlertPolizaTodoRiesgo.checkbox.value })
                    }

                    if (dataAlertPolizaTodoRiesgo.aseguradora) {
                        this.setState({ aseguradoraPolizaTodoRiesgo: dataAlertPolizaTodoRiesgo.aseguradora.value })
                    }

                    if (dataAlertPolizaTodoRiesgo.numberPolizaPolizaTodoRiesgo) {
                        this.setState({ numberPolizaPolizaTodoRiesgo: dataAlertPolizaTodoRiesgo.numberPolizaPolizaTodoRiesgo.value })
                    }

                    if (dataAlertPolizaTodoRiesgo.dateExpiration) {
                        this.setState({ dateExpirationPolizaTodoRiesgo: dataAlertPolizaTodoRiesgo.dateExpiration.value })
                    }

                    if (dataAlertPolizaTodoRiesgo.visibility) {
                        this.setState({ visibilityPolizaTodoRiesgo: dataAlertPolizaTodoRiesgo.visibility.value })
                    }

                    /**
                     * Seteamos los estados de Tecno Mecanica
                     */
                    if (dataAlertRevisionTecnoMecanica.checkbox) {
                        this.setState({ checkBoxRevisionTecnoMecanica: dataAlertRevisionTecnoMecanica.checkbox.value })
                    }

                    if (dataAlertRevisionTecnoMecanica.cda) {
                        this.setState({ cdaRevisionTecnoMecanica: dataAlertRevisionTecnoMecanica.cda.value })
                    }

                    if (dataAlertRevisionTecnoMecanica.numberRevisionRevisionTecnoMecanica) {
                        this.setState({ numberRevisionRevisionTecnoMecanica: dataAlertRevisionTecnoMecanica.numberRevisionRevisionTecnoMecanica.value })
                    }

                    if (dataAlertRevisionTecnoMecanica.dateExpiration) {
                        this.setState({ dateExpirationRevisionTecnoMecanica: dataAlertRevisionTecnoMecanica.dateExpiration.value })
                    }

                    if (dataAlertRevisionTecnoMecanica.visibility) {
                        this.setState({ visibilityRevisionTecnoMecanica: dataAlertRevisionTecnoMecanica.visibility.value })
                    }

                    /**
                     * Seteamos los estados de Cambio Aceite
                     */
                    if (dataAlertCambioAceite.checkbox) {
                        this.setState({ checkBoxCambioAceite: dataAlertCambioAceite.checkbox.value })
                    }

                    if (dataAlertCambioAceite.dateRealizacion) {
                        this.setState({ dateRealizatationCambioAceite: dataAlertCambioAceite.dateRealizacion.value })
                    }

                    if (dataAlertCambioAceite.dateExpiration) {
                        this.setState({ dateExpirationCambioAceite: dataAlertCambioAceite.dateExpiration.value })
                    }

                    if (dataAlertCambioAceite.visibility) {
                        this.setState({ visibilityCambioAceite: dataAlertCambioAceite.visibility.value })
                    }

                    /**
                     * Seteamos los estados de Alertas Sincronizacion
                     */
                    if (dataAlertSincronizacion.checkbox) {
                        this.setState({ checkBoxSynchronization: dataAlertSincronizacion.checkbox.value })
                    }

                    if (dataAlertSincronizacion.dateRealizacion) {
                        this.setState({ dateRealizatationSynchronization: dataAlertSincronizacion.dateRealizacion.value })
                    }

                    if (dataAlertSincronizacion.dateExpiration) {
                        this.setState({ dateExpirationSynchronization: dataAlertSincronizacion.dateExpiration.value })
                    }

                    if (dataAlertSincronizacion.visibility) {
                        this.setState({ visibilitySynchronization: dataAlertSincronizacion.visibility.value })
                    }

                    /**
                     * Seteamos los estados de Alineacion Balanceo
                     */
                    if (dataAlertAlineacionBalanceo.checkbox) {
                        this.setState({ checkBoxAlineacionBalanceo: dataAlertAlineacionBalanceo.checkbox.value })
                    }

                    if (dataAlertAlineacionBalanceo.dateRealizacion) {
                        this.setState({ dateRealizatationAlineacionBalanceo: dataAlertAlineacionBalanceo.dateRealizacion.value })
                    }

                    if (dataAlertAlineacionBalanceo.dateExpiration) {
                        this.setState({ dateExpirationAlineacionBalanceo: dataAlertAlineacionBalanceo.dateExpiration.value })
                    }

                    if (dataAlertAlineacionBalanceo.visibility) {
                        this.setState({ visibilityAlineacionBalanceo: dataAlertAlineacionBalanceo.visibility.value })
                    }

                    /**
                     * Seteamos los estados de Cambio Llantas
                     */
                    if (dataAlertCambioLlantas.checkbox) {
                        this.setState({ checkBoxCambioLlantas: dataAlertCambioLlantas.checkbox.value })
                    }

                    if (dataAlertCambioLlantas.dateRealizacion) {
                        this.setState({ dateRealizatationCambioLLantas: dataAlertCambioLlantas.dateRealizacion.value })
                    }

                    if (dataAlertCambioLlantas.dateExpiration) {
                        this.setState({ dateExpirationCambioLLantas: dataAlertCambioLlantas.dateExpiration.value })
                    }

                    if (dataAlertCambioLlantas.visibility) {
                        this.setState({ visibilityCambioLLantas: dataAlertCambioLlantas.visibility.value })
                    }

                })
                .catch(err => {
                    switch (err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    handleOnChange = event => {

        const errors = this.state.errors;

        switch (event.target.name) {
            case 'plate':
                delete errors.plate;
                break;
            case 'model':
                delete errors.model;
                break;
            case 'color':
                delete errors.color;
                break;
            case 'mileage':
                delete errors.mileage;
                break;
            case 'numberRegistration':
                delete errors.numberRegistration;
                break;
            case 'city':
                delete errors.city;
                break;
            /*case 'type':
                delete errors.type;
                break;*/
            case 'brand':
                delete errors.brand;
                break;
            case 'typeBody':
                delete errors.typeBody;
                break;
            case 'line':
                delete errors.line;
                break;
            case 'satelliteCompany':
                delete errors.satelliteCompany;
                break;
            case 'capacityPassenger':
                delete errors.capacityPassenger;
                break;
            case 'capacityKLS':
                delete errors.capacityKLS;
                break;
            case 'displacement':
                delete errors.displacement;
                break;
            case 'numberEngine':
                delete errors.numberEngine;
                break;
            case 'numberChais':
                delete errors.numberChais;
                break;
            case 'numberLicense':
                delete errors.numberLicense;
                break;
            case 'vin':
                delete errors.vin;
                break;
            case 'statusVehicle':
                delete errors.statusVehicle;
                break;
            case 'typeInspection':
                delete errors.typeInspection;
                break;
            case 'typeDirection':
                delete errors.typeDirection;
                break;
            case 'typeTransmission':
                delete errors.typeTransmission;
                break;
            case 'typeTraction':
                delete errors.typeTraction;
                break;
            case 'frontSuspension':
                delete errors.frontSuspension;
                break;
            case 'backSuspension':
                delete errors.backSuspension;
                break;
            case 'wheelsMaterial':
                delete errors.wheelsMaterial;
                break;
            case 'turbo':
                delete errors.turbo;
                break;
            case 'numberTires':
                delete errors.numberTires;
                break;
            case 'numbersCylinders':
                delete errors.numbersCylinders;
                break;
            case 'fuel':
                delete errors.fuel;
                break;
            case 'orientation':
                delete errors.orientation;
                break;
            case 'typeFrontBrakes':
                delete errors.typeFrontBrakes;
                break;
            case 'typeBackBrakes':
                delete errors.typeBackBrakes;
                break;
            case 'linkage':
                delete errors.linkage;
                break;
            case 'owner':
                delete errors.owner;
                break;
            case 'checkBoxRegistration':
                delete errors.checkboxMatricula;
                break;
            case 'numberRegistration':
                delete errors.numberRegistration;
                break;
            case 'visibilityRegistration':
                delete errors.visibilityMatricula;
                break;
            case 'checkBoxSoat':
                delete errors.checkboxSoat;
                break;
            case 'aseguradoraSoat':
                delete errors.aseguradoraSoat;
                break;
            case 'visibilitySoat':
                delete errors.visibilitySoat;
                break;
            case 'checkBoxPolizaTodoRiesgo':
                delete errors.checkboxPolizaTodoRiesgo;
                break;
            case 'aseguradoraPolizaTodoRiesgo':
                delete errors.aseguradoraPolizaTodoRiesgo;
                break;
            case 'numberPolizaPolizaTodoRiesgo':
                delete errors.numberPolizaPolizaTodoRiesgo;
                break;
            case 'visibilityPolizaTodoRiesgo':
                delete errors.visibilityPolizaTodoRiesgo;
                break;
            case 'checkBoxRevisionTecnoMecanica':
                delete errors.checkboxRevisionTecnoMecanica;
                break;
            case 'cdaRevisionTecnoMecanica':
                delete errors.cdaRevisionTecnoMecanica;
                break;
            case 'numberRevisionRevisionTecnoMecanica':
                delete errors.numberRevisionRevisionTecnoMecanica;
                break;
            case 'visibilityRevisionTecnoMecanica':
                delete errors.visibilityRevisionTecnoMecanica;
                break;
            case 'checkBoxCambioAceite':
                delete errors.checkboxCambioAceite;
                break;
            case 'visibilityCambioAceite':
                delete errors.visibilityCambioAceite;
                break;
            case 'checkBoxSynchronization':
                delete errors.checkboxSincronizacion;
                break;
            case 'visibilitySynchronization':
                delete errors.visibilitySincronizacion;
                break;
            case 'checkBoxAlineacionBalanceo':
                delete errors.checkboxAlineacionBalanceo;
                break;
            case 'visibilityAlineacionBalanceo':
                delete errors.visibilityAlineacionBalanceo;
                break;
            case 'checkBoxCambioLlantas':
                delete errors.checkboxCambioLlantas;
                break;
            case 'visibilityCambioLLantas':
                delete errors.visibilityCambioLlantas;
                break;

            default:
                break;
        }

        if (event.target.type === 'checkbox') {
            this.setState({ [event.target.name]: event.target.checked });
        } else {
            this.setState({ [event.target.name]: event.target.value });
        }

        this.setState({ errors: errors, })
    };

    handleChangeAutoComplete = (name, newValue) => {

        const errors = this.state.errors;

        switch (name) {
            case 'city':
                delete errors.city;
                break;
            default:
                break;
        }

        this.setState({
            errors: errors,
            [name]: newValue,
        });
    };

    handleChangeDateExpirationRegistration = date => {
        this.setState({ dateExpirationRegistration: date });
    }

    handleChangeDateExpirationSoat = date => {
        this.setState({ dateExpirationSoat: date });
    }

    handleChangeDateExpirationPolizaTodoRiesgo = date => {
        this.setState({ dateExpirationPolizaTodoRiesgo: date });
    }

    handleChangeDateExpirationRevisionTecnoMecanica = date => {
        this.setState({ dateExpirationRevisionTecnoMecanica: date });
    }

    handleChangeDateRealitationCambioAceite = date => {
        this.setState({ dateRealizatationCambioAceite: date });
    }

    handleChangeDateExpirationCambioAceite = date => {
        this.setState({ dateExpirationCambioAceite: date });
    }

    handleChangeDateRealitationSynchronization = date => {
        this.setState({ dateRealizatationSynchronization: date });
    }

    handleChangeDateExpirationSynchronization = date => {
        this.setState({ dateExpirationSynchronization: date });
    }

    handleChangeDateRealitationAlineacionBalanceo = date => {
        this.setState({ dateRealizatationAlineacionBalanceo: date });
    }

    handleChangeDateExpirationAlineacionBalanceo = date => {
        this.setState({ dateExpirationAlineacionBalanceo: date });
    }

    handleChangeDateRealitationCambioLlantas = date => {
        this.setState({ dateRealizatationCambioLLantas: date });
    }

    handleChangeDateExpirationCambioLlantas = date => {
        this.setState({ dateExpirationCambioLLantas: date });
    }

    handleClose = () => {
        this.setState({
            plate: '',
            model: '',
            color: '',
            mileage: '',
            numberRegistration: '',
            dataCities: [],
            idCity: '',
            city: '',
            //type: '',
            brand: '',
            typeBody: '',
            line: '',
            satelliteCompany: '',
            capacityPassenger: '',
            capacityKLS: '',
            displacement: '',
            numberEngine: '',
            numberChais: '',
            numberLicense: '',
            vin: '',
            dataStatusVehicles: [],
            statusVehicle: '',
            idStatusVehicle: "",
            dataTypesInspection: [],
            typeInspection: '',
            idTypeInspection: '',

            typeDirection: '',
            typeTransmission: '',
            typeTraction: '',
            frontSuspension: '',
            backSuspension: '',
            wheelsMaterial: '',
            numberTires: '',

            numbersCylinders: '',
            fuel: '',
            turbo: '',
            orientation: '',

            typeFrontBrakes: '',
            typeBackBrakes: '',

            dataLinkage: [],
            idLinkage: '',
            linkage: '',

            dataOwners: [],
            idOwner: '',
            owner: '',

            checkBoxRegistration: '',
            dateExpirationRegistration: new Date(),
            visibilityRegistration: '',

            checkBoxSoat: '',
            aseguradoraSoat: '',
            dateExpirationSoat: new Date(),
            visibilitySoat: '',

            checkBoxPolizaTodoRiesgo: '',
            aseguradoraPolizaTodoRiesgo: '',
            numberPolizaPolizaTodoRiesgo: '',
            dateExpirationPolizaTodoRiesgo: new Date(),
            visibilityPolizaTodoRiesgo: '',

            checkBoxRevisionTecnoMecanica: '',
            cdaRevisionTecnoMecanica: '',
            numberRevisionRevisionTecnoMecanica: '',
            dateExpirationRevisionTecnoMecanica: new Date(),
            visibilityRevisionTecnoMecanica: '',

            checkBoxCambioAceite: '',
            dateExpirationCambioAceite: new Date(),
            dateRealizatationCambioAceite: new Date(),
            visibilityCambioAceite: '',

            checkBoxSynchronization: '',
            dateExpirationSynchronization: new Date(),
            dateRealizatationSynchronization: new Date(),
            visibilitySynchronization: '',

            checkBoxAlineacionBalanceo: '',
            dateExpirationAlineacionBalanceo: new Date(),
            dateRealizatationAlineacionBalanceo: new Date(),
            visibilityAlineacionBalanceo: '',

            checkBoxCambioLlantas: '',
            dateExpirationCambioLLantas: new Date(),
            dateRealizatationCambioLLantas: new Date(),
            visibilityCambioLLantas: '',

            errors: {}
        })
        this.props.handleOnClose(CRUDUPDATE);
    };

    handleSubmit = id => e => {
        const dataVehicle = []

        const dataBasicVehicle = [
            { plate: { value: this.state.plate, inputTitle: 'Placa' } },
            { model: { value: this.state.model, inputTitle: 'Módelo' } },
            { color: { value: this.state.color, inputTitle: 'Color' } },
            { mileage: { value: this.state.mileage.toString().replace(/ /g, "").trim(), inputTitle: 'Kilometraje' } },
            { numberRegistration: { value: this.state.numberRegistration, inputTitle: 'Matrícula' } },
            { city: { value: this.state.city, inputTitle: 'Ciudad Matrícula' } },
            //{ type: { value: this.state.type, inputTitle: 'Tipo' } },
            { brand: { value: this.state.brand, inputTitle: 'Marca' } },
            { typeBody: { value: this.state.typeBody, inputTitle: 'Tipo Carrocería' } },
            { line: { value: this.state.line, inputTitle: 'Linea' } },
            { satelliteCompany: { value: this.state.satelliteCompany, inputTitle: 'Empresa Satélite' } },
            { capacityPassenger: { value: this.state.capacityPassenger, inputTitle: 'Capacidad Pasajero' } },
            { capacityKLS: { value: this.state.capacityKLS, inputTitle: 'Capacidad KLS' } },
            { displacement: { value: this.state.displacement, inputTitle: 'Cilindraje' } },
            { numberEngine: { value: this.state.numberEngine, inputTitle: 'No. Motor' } },
            { numberChais: { value: this.state.numberChais, inputTitle: 'No. Chais' } },
            { numberLicense: { value: this.state.numberLicense, inputTitle: 'No. Licencia' } },
            { vin: { value: this.state.vin, inputTitle: 'VIN' } },
            { statusVehicle: { value: this.state.statusVehicle, inputTitle: 'Estado Vehículo' } },
            { typeInspection: { value: this.state.typeInspection, inputTitle: 'Tipo de Inspección' } },
        ]

        const dataDirTraSus = [
            { typeDirection: { value: this.state.typeDirection, inputTitle: 'Tipo Dirección' } },
            { typeTransmission: { value: this.state.typeTransmission, inputTitle: 'Tipo Transmisión' } },
            { typeTraction: { value: this.state.typeTraction, inputTitle: 'Tipo Tracción' } },
            { frontSuspension: { value: this.state.frontSuspension, inputTitle: 'Suspención Delantera' } },
            { backSuspension: { value: this.state.backSuspension, inputTitle: 'Suspención Trasera' } },
            { wheelsMaterial: { value: this.state.wheelsMaterial, inputTitle: 'Material de Rines' } },
            { numberTires: { value: this.state.numberTires, inputTitle: 'Número de LLantas' } },
        ]

        const dataEngineVehicle = [
            { numbersCylinders: { value: this.state.numbersCylinders, inputTitle: 'Cantidad Cilindros' } },
            { fuel: { value: this.state.fuel, inputTitle: 'Combustible' } },
            { turbo: { value: this.state.turbo, inputTitle: 'Turbo' } },
            { orientation: { value: this.state.orientation, inputTitle: 'Orientación' } },
        ]

        const dataBrakesVehicle = [
            { typeFrontBrakes: { value: this.state.typeFrontBrakes, inputTitle: 'Tipo Frenos Delanteros' } },
            { typeBackBrakes: { value: this.state.typeBackBrakes, inputTitle: 'Tipo Frenos Traseros' } },
        ]

        const dataOwnerVehicle = [
            { linkage: { value: this.state.linkage, inputTitle: 'Vinculación' } },
            { owner: { value: this.state.owner, inputTitle: 'Propietario' } }
        ]

        const dataAlertsVehicle = [
            {
                matricula: {
                    checkbox: { value: this.state.checkBoxRegistration.toString(), inputTitle: 'Matrícula' },
                    numberRegistration: { value: this.state.numberRegistration, inputTitle: 'Número Matrícula' },
                    cityRegistratios: { value: this.state.city, inputTitle: 'Ciudad Matrícula' },
                    dateExpiration: { value: this.state.dateExpirationRegistration, inputTitle: 'Fecha Expiración' },
                    visibility: { value: this.state.visibilityRegistration, inputTitle: 'Visible Para:' }
                }
            },

            {
                soat: {
                    checkbox: { value: this.state.checkBoxSoat.toString(), inputTitle: 'Soat' },
                    aseguradora: { value: this.state.aseguradoraSoat, inputTitle: 'Aseguradora' },
                    dateExpiration: { value: this.state.dateExpirationSoat, inputTitle: 'Fecha Expiración' },
                    visibility: { value: this.state.visibilitySoat, inputTitle: 'Visible Para:' },
                }
            },

            {
                polizaTodoRiesgo: {
                    checkbox: { value: this.state.checkBoxRegistration.toString(), inputTitle: 'Póliza Todo Riesgo' },
                    aseguradora: { value: this.state.aseguradoraPolizaTodoRiesgo, inputTitle: 'Aseguradora' },
                    numberPolizaPolizaTodoRiesgo: { value: this.state.numberPolizaPolizaTodoRiesgo, inputTitle: 'Número Poliza' },
                    dateExpiration: { value: this.state.dateExpirationPolizaTodoRiesgo, inputTitle: 'Fecha Expiración' },
                    visibility: { value: this.state.visibilityPolizaTodoRiesgo, inputTitle: 'Visible Para:' },
                }
            },

            {
                revisionTecnoMecanica: {
                    checkbox: { value: this.state.checkBoxRegistration.toString(), inputTitle: 'Revisión Tecno Mecánica' },
                    cda: { value: this.state.cdaRevisionTecnoMecanica, inputTitle: 'CDA' },
                    numberRevisionRevisionTecnoMecanica: { value: this.state.numberRevisionRevisionTecnoMecanica, inputTitle: 'Número Revisión' },
                    dateExpiration: { value: this.state.dateExpirationRevisionTecnoMecanica, inputTitle: 'Fecha Expiración' },
                    visibility: { value: this.state.visibilityRevisionTecnoMecanica, inputTitle: 'Visible Para:' },
                }
            },

            {
                cambioAceite: {
                    checkbox: { value: this.state.checkBoxCambioAceite.toString(), inputTitle: 'Cambio Aceite' },
                    dateRealizacion: { value: this.state.dateRealizatationCambioAceite, inputTitle: 'Fecha Realización' },
                    dateExpiration: { value: this.state.dateExpirationCambioAceite, inputTitle: 'Fecha Expiración' },
                    visibility: { value: this.state.visibilityCambioAceite, inputTitle: 'Visible Para:' },
                }
            },

            {
                sincronizacion: {
                    checkbox: { value: this.state.checkBoxSynchronization.toString(), inputTitle: 'Sincronización' },
                    dateRealizacion: { value: this.state.dateRealizatationSynchronization, inputTitle: 'Fecha Realización' },
                    dateExpiration: { value: this.state.dateExpirationSynchronization, inputTitle: 'Fecha Expiración' },
                    visibility: { value: this.state.visibilitySynchronization, inputTitle: 'Visible Para:' },
                }
            },

            {
                alineacionBalanceo: {
                    checkbox: { value: this.state.checkBoxAlineacionBalanceo.toString(), inputTitle: 'Alineación || Balanceo' },
                    dateRealizacion: { value: this.state.dateRealizatationAlineacionBalanceo, inputTitle: 'Fecha Realización' },
                    dateExpiration: { value: this.state.dateExpirationAlineacionBalanceo, inputTitle: 'Fecha Expiración' },
                    visibility: { value: this.state.visibilityAlineacionBalanceo, inputTitle: 'Visible Para:' },
                }
            },

            {
                cambioLlantas: {
                    checkbox: { value: this.state.checkBoxCambioLlantas.toString(), inputTitle: 'Cambio de Llantas' },
                    dateRealizacion: { value: this.state.dateRealizatationCambioLLantas, inputTitle: 'Fecha Realización' },
                    dateExpiration: { value: this.state.dateExpirationCambioLLantas, inputTitle: 'Fecha Expiración' },
                    visibility: { value: this.state.visibilityCambioLLantas, inputTitle: 'Visible Para:' },
                }
            }
        ]

        dataVehicle.push({ "Datos Basicos Vehículo": dataBasicVehicle })
        dataVehicle.push({ "Direccion || Transmisión || Suspensión": dataDirTraSus })
        dataVehicle.push({ "Motor": dataEngineVehicle })
        dataVehicle.push({ "Frenos": dataBrakesVehicle })
        dataVehicle.push({ "Propietario": dataOwnerVehicle })
        dataVehicle.push({ "Alertas": dataAlertsVehicle })

        let idVehicleState = ''
        const arrayStateVehicle = this.state.dataStatusVehicles.filter(item => item.state === this.state.statusVehicle)

        if (arrayStateVehicle.length > 0) {
            idVehicleState = arrayStateVehicle[0].id
        }

        let idOwner = ''
        const arrayOwner = this.state.dataOwners.filter(item => item.name === this.state.owner)

        if (arrayOwner.length > 0) {
            idOwner = arrayOwner[0].id
        }

        let idTypeInspection = ''
        const arrayTypesInspections = this.state.dataTypesInspection.filter(item => item.type === this.state.typeInspection)

        if (arrayTypesInspections.length > 0) {
            idTypeInspection = arrayTypesInspections[0].id
        }


        const dataAdd = {
            dataVehicle: dataVehicle,
            idVehicleState: idVehicleState.toString(),
            plate: this.state.plate.replace(/ /g, "").trim(),
            idOwner: idOwner.toString(),
            idTypeInspection: idTypeInspection.toString(),
            mileageInitial: this.state.mileage.replace(/ /g, "").trim(),
            mileageCurrent: this.state.mileage.replace(/ /g, "").trim(),
            inspectionEnabled: "1"
        };

        console.log("OUTPUT: DialogAdd -> dataAdd", dataAdd)

        if (!id) {
            http
                .post('/api/vehicles', dataAdd)
                .then(result => {
                    this.handleClose();
                    this.props.backAfterInsert(CRUDADD, result.data.message);
                })
                .catch(err => {
                    switch (err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        } else {
            http
                .put(`/api/vehicles/${id}`, dataAdd)
                .then(result => {
                    if (result) {
                        this.handleClose();
                        this.props.backAfterInsert(CRUDUPDATE, result.data.message);
                    }
                })
                .catch(err => {
                    switch (err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    render() {

        let dataSelectCities = []
        let dataSelectStatusVehicles = []
        let dataSelectTypesInspection = []
        let dataSelectLinkage = []
        let dataSelectOwner = []
        this.state.dataCities.map(item => {
            let objectCity = {}
            objectCity.label = item.city
            dataSelectCities.push(objectCity)
        })
        dataSelectStatusVehicles = this.state.dataStatusVehicles.map(item => item.state)
        dataSelectOwner = this.state.dataOwners.map(item => item.name)
        dataSelectTypesInspection = this.state.dataTypesInspection.map(item => item.type)
        dataSelectLinkage = this.state.dataLinkage.map(item => item.type)

        const { classes, id, title, open } = this.props;
        const {
            dataCities,
            owner,
            linkage,
            numberRegistration,
            city,
            checkBoxRegistration,
            dateExpirationRegistration,
            visibilityRegistration,

            checkBoxSoat,
            aseguradoraSoat,
            dateExpirationSoat,
            visibilitySoat,

            checkBoxPolizaTodoRiesgo,
            aseguradoraPolizaTodoRiesgo,
            numberPolizaPolizaTodoRiesgo,
            dateExpirationPolizaTodoRiesgo,
            visibilityPolizaTodoRiesgo,

            checkBoxRevisionTecnoMecanica,
            cdaRevisionTecnoMecanica,
            numberRevisionRevisionTecnoMecanica,
            dateExpirationRevisionTecnoMecanica,
            visibilityRevisionTecnoMecanica,

            checkBoxCambioAceite,
            dateExpirationCambioAceite,
            dateRealizatationCambioAceite,
            visibilityCambioAceite,

            checkBoxSynchronization,
            dateExpirationSynchronization,
            dateRealizatationSynchronization,
            visibilitySynchronization,

            checkBoxAlineacionBalanceo,
            dateExpirationAlineacionBalanceo,
            dateRealizatationAlineacionBalanceo,
            visibilityAlineacionBalanceo,

            checkBoxCambioLlantas,
            dateExpirationCambioLLantas,
            dateRealizatationCambioLLantas,
            visibilityCambioLLantas,
            errors } = this.state;

        const dataForm = [
            { name: 'plate', id: 'plate', label: 'Placa', type: 'textinput', focus: true, error: errors.plate, value: this.state.plate },
            { name: 'model', id: 'model', label: 'Módelo', type: 'textinput', focus: false, error: errors.model, value: this.state.model },
            { name: 'color', id: 'color', label: 'Color', type: 'textinput', focus: false, error: errors.color, value: this.state.color },
            { name: 'mileage', id: 'mileage', label: 'Kilometraje', type: 'textinput', focus: false, error: errors.mileage, value: this.state.mileage },
            { name: 'numberRegistration', id: 'numberRegistration', label: 'Matrícula', type: 'textinput', focus: false, error: errors.numberRegistration, value: this.state.numberRegistration },
            { name: 'city', id: 'city', label: 'Ciudad Matrícula', placeHolder: 'Buscar una ciudad *', data: dataSelectCities, value: this.state.city, type: 'autoComplete', focus: false, error: errors.city },
            //{ name: 'type', id: 'type', label: 'Tipo', type: 'textinput', focus: false, error: errors.type, value: this.state.type },
            { name: 'brand', id: 'brand', label: 'Marca', type: 'textinput', focus: false, error: errors.brand, value: this.state.brand },
            { name: 'typeBody', id: 'typeBody', label: 'Tipo Carrocería', type: 'textinput', focus: false, error: errors.typeBody, value: this.state.typeBody },
            { name: 'line', id: 'line', label: 'Linea', type: 'textinput', focus: false, error: errors.line, value: this.state.line },
            { name: 'satelliteCompany', id: 'satelliteCompany', label: 'Empresa Satélite', type: 'textinput', focus: false, error: errors.satelliteCompany, value: this.state.satelliteCompany },
            { name: 'capacityPassenger', id: 'capacityPassenger', label: 'Capacidad Pasajero', type: 'textinput', focus: false, error: errors.capacityPassenger, value: this.state.capacityPassenger },
            { name: 'capacityKLS', id: 'capacityKLS', label: 'Capacidad KLS', type: 'textinput', focus: false, error: errors.capacityKLS, value: this.state.capacityKLS },
            { name: 'displacement', id: 'displacement', label: 'Cilindraje', type: 'textinput', focus: false, error: errors.displacement, value: this.state.displacement },
            { name: 'numberEngine', id: 'numberEngine', label: 'No. Motor', type: 'textinput', focus: false, error: errors.numberEngine, value: this.state.numberEngine },
            { name: 'numberChais', id: 'numberChais', label: 'No. Chais', type: 'textinput', focus: false, error: errors.numberChais, value: this.state.numberChais },
            { name: 'numberLicense', id: 'numberLicense', label: 'No. Licencia', type: 'textinput', focus: false, error: errors.numberLicense, value: this.state.numberLicense },
            { name: 'vin', id: 'vin', label: 'VIN', type: 'textinput', focus: false, error: errors.vin, value: this.state.vin },
            { name: 'statusVehicle', id: 'statusVehicle', label: 'Estado Vehículo', data: dataSelectStatusVehicles, value: this.state.statusVehicle, type: 'select', focus: false, error: errors.statusVehicle },
            { name: 'typeInspection', id: 'typeInspection', label: 'Tipo de Inspección', data: dataSelectTypesInspection, value: this.state.typeInspection, type: 'select', focus: false, error: errors.typeInspection },
        ]

        const dataSectionFormDirTraSus = [
            { name: 'typeDirection', id: 'typeDirection', label: 'Tipo Dirección', type: 'textinput', focus: false, error: errors.typeDirection, value: this.state.typeDirection },
            { name: 'typeTransmission', id: 'typeTransmission', label: 'Tipo Transmisión', type: 'textinput', focus: false, error: errors.typeTransmission, value: this.state.typeTransmission },
            { name: 'typeTraction', id: 'typeTraction', label: 'Tipo Tracción', type: 'textinput', focus: false, error: errors.typeTraction, value: this.state.typeTraction },
            { name: 'frontSuspension', id: 'frontSuspension', label: 'Suspensión Delantera', type: 'textinput', focus: false, error: errors.frontSuspension, value: this.state.frontSuspension },
            { name: 'backSuspension', id: 'backSuspension', label: 'Suspensión Trasera', type: 'textinput', focus: false, error: errors.backSuspension, value: this.state.backSuspension },
            { name: 'wheelsMaterial', id: 'wheelsMaterial', label: 'Material de Rines', type: 'textinput', focus: false, error: errors.wheelsMaterial, value: this.state.wheelsMaterial },
            { name: 'numberTires', id: 'numberTires', label: 'No. Llantas', type: 'textinput', focus: false, error: errors.numberTires, value: this.state.numberTires },
        ]

        const dataSectionFormEngine = [
            { name: 'numbersCylinders', id: 'numbersCylinders', label: 'Cantidad Cilindros', type: 'textinput', focus: false, error: errors.numbersCylinders, value: this.state.numbersCylinders },
            { name: 'fuel', id: 'fuel', label: 'Combustible', type: 'textinput', focus: false, error: errors.fuel, value: this.state.fuel },
            { name: 'turbo', id: 'turbo', label: 'Turbo', type: 'textinput', focus: false, error: errors.turbo, value: this.state.turbo },
            { name: 'orientation', id: 'orientation', label: 'Orientación', type: 'textinput', focus: false, error: errors.orientation, value: this.state.orientation },
        ]

        const dataSectionFormBrakes = [
            { name: 'typeFrontBrakes', id: 'typeFrontBrakes', label: 'Tipo de Frenos Delanteros', type: 'textinput', focus: false, error: errors.typeFrontBrakes, value: this.state.typeFrontBrakes },
            { name: 'typeBackBrakes', id: 'typeBackBrakes', label: 'Tipo de Frenos Traseros', type: 'textinput', focus: false, error: errors.typeBackBrakes, value: this.state.typeBackBrakes },
        ]

        console.log("visibilityRegistration", visibilityRegistration)
        return (
            <Dialog
                fullWidth
                maxWidth="md"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />{' '}
                    {id ? CRUDUPDATE : CRUDADD} {title}
                </DialogTitle>

                <DialogContent>
                    {/* Code Data Vehicle */}
                    <div className={classes.containerSectionForm}>
                        <Typography
                            variant="subtitle1"
                            color="secondary"
                            noWrap
                            className={classes.subtitle}
                        >
                            {DATAVEHICLE}
                        </Typography>
                        <Grid container spacing={24}>
                            {dataForm.map((item, index) => (
                                <Grid key={index} item xs={12} sm={3} md={3} >
                                    {item.type === 'textinput' ?
                                        <CustomTextInput
                                            name={item.name}
                                            label={item.label}
                                            focus={item.focus}
                                            value={item.value}
                                            handleOnChange={this.handleOnChange}
                                            error={item.error} />
                                        : item.type === 'autoComplete' ?
                                            <CustomAutoComplete
                                                name={item.name}
                                                label={item.placeHolder}
                                                data={item.data}
                                                value={item.value}
                                                handleOnChange={this.handleChangeAutoComplete}
                                                error={item.error} />
                                            : <CustomSelectInput
                                                name={item.name}
                                                value={item.value}
                                                label={item.label}
                                                data={item.data}
                                                handleOnChange={this.handleOnChange}
                                                error={item.error}
                                            />
                                    }
                                </Grid>
                            ))}
                        </Grid>
                    </div>

                    {/* Continuacion de Code Data Vehicle */}
                    <div>
                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>
                                <div className={classes.containerSectionForm}>
                                    <Typography
                                        variant="subtitle2"
                                        color="primary"
                                        noWrap
                                        className={classes.subtitle}
                                    >
                                        Dirección || Transmisión || Suspensión
                                    </Typography>
                                    <Grid container spacing={24}>
                                        {dataSectionFormDirTraSus.map((item, index) => (
                                            <Grid key={index} item xs={12} sm={6} md={6} >
                                                <CustomTextInput
                                                    name={item.name}
                                                    label={item.label}
                                                    focus={item.focus}
                                                    value={item.value}
                                                    handleOnChange={this.handleOnChange}
                                                    error={item.error} />
                                            </Grid>
                                        ))}
                                    </Grid>

                                </div>
                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <div className={classes.containerSectionForm}>
                                    <Typography
                                        variant="subtitle2"
                                        color="primary"
                                        noWrap
                                        className={classes.subtitle}
                                    >
                                        Motor
                                    </Typography>
                                    <Grid container spacing={24}>
                                        {dataSectionFormEngine.map((item, index) => (
                                            <Grid key={index} item xs={12} sm={6} md={6} >
                                                <CustomTextInput
                                                    name={item.name}
                                                    label={item.label}
                                                    focus={item.focus}
                                                    value={item.value}
                                                    handleOnChange={this.handleOnChange}
                                                    error={item.error} />
                                            </Grid>
                                        ))}
                                    </Grid>
                                </div>

                                <div>
                                    <Typography
                                        variant="subtitle2"
                                        color="primary"
                                        noWrap
                                        className={classes.subtitle}
                                    >
                                        Frenos
                                    </Typography>
                                    <Grid container spacing={24}>
                                        {dataSectionFormBrakes.map((item, index) => (
                                            <Grid key={index} item xs={12} sm={6} md={6} >
                                                <CustomTextInput
                                                    name={item.name}
                                                    label={item.label}
                                                    focus={item.focus}
                                                    value={item.value}
                                                    handleOnChange={this.handleOnChange}
                                                    error={item.error} />
                                            </Grid>
                                        ))}
                                    </Grid>
                                </div>
                            </Grid>
                        </Grid>
                    </div>

                    <Divider className={classes.divider} />

                    {/* Code Data Owner */}
                    <div className={classes.containerSectionForm}>
                        <Typography
                            variant="subtitle1"
                            color="secondary"
                            noWrap
                            gutterBottom
                            className={classes.subtitle}
                        >
                            Selección de Vinculación || Propietario
                        </Typography>

                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>
                                <FormControl
                                    error={errors.linkage ? true : false}
                                    fullWidth
                                    required
                                >
                                    <InputLabel
                                        required
                                        error={errors.linkage ? true : false}
                                        htmlFor="linkage"
                                    >
                                        {LINKAGE}
                                    </InputLabel>
                                    <Select
                                        error={errors.linkage ? true : false}
                                        value={linkage}
                                        onChange={this.handleOnChange}
                                        name="linkage"
                                        id="linkage"
                                        inputProps={{
                                            id: 'linkage'
                                        }}
                                        className={classes.selectEmpty}
                                    >
                                        <MenuItem value="">
                                            <em>Ninguno</em>
                                        </MenuItem>
                                        {dataSelectLinkage.map((item, index) => (
                                            <MenuItem key={index} value={item}>
                                                {item}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                                <Typography color="error" variant="caption">
                                    {errors.linkage}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl
                                    error={errors.owner ? true : false}
                                    fullWidth
                                    required
                                >
                                    <InputLabel
                                        required
                                        error={errors.owner ? true : false}
                                        htmlFor="owner"
                                    >
                                        {OWNER}
                                    </InputLabel>
                                    <Select
                                        error={errors.owner ? true : false}
                                        value={owner}
                                        onChange={this.handleOnChange}
                                        name="owner"
                                        id="owner"
                                        inputProps={{
                                            id: 'owner'
                                        }}
                                        className={classes.selectEmpty}
                                    >
                                        <MenuItem value="">
                                            <em>Ninguno</em>
                                        </MenuItem>
                                        {dataSelectOwner.map((item, index) => (
                                            <MenuItem key={index} value={item}>
                                                {item}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                                <Typography color="error" variant="caption">
                                    {errors.owner}
                                </Typography>
                            </Grid>
                        </Grid>
                    </div>

                    <Divider className={classes.divider} />

                    {/* Code Data Alerts */}
                    <div className={classes.containerSectionForm}>
                        <Typography
                            variant="subtitle1"
                            color="secondary"
                            noWrap
                            className={classes.subtitle}
                        >
                            {ALERTS}
                        </Typography>

                        <div className={classes.containerSectionForm}>
                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={4}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                name="checkBoxRegistration"
                                                checked={checkBoxRegistration}
                                                value="checkBoxRegistration"
                                                onChange={this.handleOnChange}
                                            />
                                        }
                                        label="Matricula"
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.checkboxMatricula}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <TextField
                                        disabled
                                        margin="dense"
                                        id="numberRegistration"
                                        name="numberRegistration"
                                        value={numberRegistration}
                                        label="Número Matricula"
                                        error={errors.numberRegistration ? true : false}
                                        fullWidth
                                        onChange={this.handleOnChange}
                                        required
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.numberRegistration}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12} sm={3} md={3}>
                                    <FormControl
                                        error={errors.city ? true : false}
                                        fullWidth
                                        required
                                    >
                                        <InputLabel
                                            required
                                            error={errors.city ? true : false}
                                            htmlFor="city"
                                        >
                                            {CITYREGISTRATION}
                                        </InputLabel>
                                        <Select
                                            disabled
                                            error={errors.city ? true : false}
                                            value={city}
                                            onChange={this.handleOnChange}
                                            name="city"
                                            id="city"
                                            inputProps={{
                                                id: 'city'
                                            }}
                                            className={classes.selectEmpty}
                                        >
                                            <MenuItem value="">
                                                <em>Ninguno</em>
                                            </MenuItem>
                                            {dataCities.map(item => (
                                                <MenuItem key={item.id} value={item.city}>
                                                    {item.city}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                    <Typography color="error" variant="caption">
                                        {errors.city}
                                    </Typography>
                                </Grid>
                            </Grid>

                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={6}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Expiración"
                                                required
                                                format="yyyy/MM/dd"
                                                invalidDateMessage={errors.dateExpirationRegistration}
                                                invalidLabel={errors.dateExpirationRegistration}
                                                value={dateExpirationRegistration}
                                                error={errors.dateExpirationRegistration ? true : false}
                                                onChange={this.handleChangeDateExpirationRegistration}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl component="fieldset" className={classes.formControl}>
                                        <FormLabel component="legend">Visible Para:</FormLabel>
                                        <RadioGroup
                                            aria-label="Visible Para:"
                                            name="visibilityRegistration"
                                            className={classes.group}
                                            value={visibilityRegistration}
                                            onChange={this.handleOnChange}
                                        >
                                            <FormControlLabel value="destinoWeb" control={<Radio />} label="Plataforma Web" />
                                            <FormControlLabel value="destinoApp" control={<Radio />} label="Aplicación Movil" />
                                            <FormControlLabel value="destinoAll" control={<Radio />} label="Todos" />
                                        </RadioGroup>
                                    </FormControl>
                                    <Typography color="error" variant="caption">
                                        {errors.visibilityMatricula}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </div>

                        <div className={classNames(classes.containerSectionForm, classes.containerSectionFormColor)}>
                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={4}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                name="checkBoxSoat"
                                                checked={checkBoxSoat}
                                                value="checkBoxSoat"
                                                onChange={this.handleOnChange}
                                            />
                                        }
                                        label="Soat"
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.checkboxSoat}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <TextField
                                        margin="dense"
                                        id="aseguradoraSoat"
                                        name="aseguradoraSoat"
                                        value={aseguradoraSoat}
                                        label="Aseguradora"
                                        error={errors.aseguradoraSoat ? true : false}
                                        fullWidth
                                        onChange={this.handleOnChange}
                                        required
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.aseguradoraSoat}
                                    </Typography>
                                </Grid>
                            </Grid>

                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={6}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Expiración"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateExpirationSoat}
                                                invalidLabel={errors.dateExpirationSoat}
                                                value={dateExpirationSoat}
                                                error={errors.dateExpirationSoat ? true : false}
                                                onChange={this.handleChangeDateExpirationSoat}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl component="fieldset" className={classes.formControl}>
                                        <FormLabel component="legend">Visible Para:</FormLabel>
                                        <RadioGroup
                                            aria-label="Visible Para:"
                                            name="visibilitySoat"
                                            className={classes.group}
                                            value={visibilitySoat}
                                            onChange={this.handleOnChange}
                                        >
                                            <FormControlLabel value="destinoWeb" control={<Radio />} label="Plataforma Web" />
                                            <FormControlLabel value="destinoApp" control={<Radio />} label="Aplicación Movil" />
                                            <FormControlLabel value="destinoAll" control={<Radio />} label="Todos" />
                                        </RadioGroup>
                                    </FormControl>
                                    <Typography color="error" variant="caption">
                                        {errors.visibilitySoat}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </div>

                        <div className={classes.containerSectionForm}>
                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={4}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                name="checkBoxPolizaTodoRiesgo"
                                                checked={checkBoxPolizaTodoRiesgo}
                                                value="checkBoxPolizaTodoRiesgo"
                                                onChange={this.handleOnChange}
                                            />
                                        }
                                        label="Póliza Todo Riesgo"
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.checkboxPolizaTodoRiesgo}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <TextField
                                        margin="dense"
                                        id="aseguradoraPolizaTodoRiesgo"
                                        name="aseguradoraPolizaTodoRiesgo"
                                        value={aseguradoraPolizaTodoRiesgo}
                                        label="Aseguradora"
                                        error={errors.aseguradoraPolizaTodoRiesgo ? true : false}
                                        fullWidth
                                        onChange={this.handleOnChange}
                                        required
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.aseguradoraPolizaTodoRiesgo}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <TextField
                                        margin="dense"
                                        id="numberPolizaPolizaTodoRiesgo"
                                        name="numberPolizaPolizaTodoRiesgo"
                                        value={numberPolizaPolizaTodoRiesgo}
                                        label="Número de Póliza"
                                        error={errors.numberPolizaPolizaTodoRiesgo ? true : false}
                                        fullWidth
                                        onChange={this.handleOnChange}
                                        required
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.numberPolizaPolizaTodoRiesgo}
                                    </Typography>

                                </Grid>
                            </Grid>

                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={6}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Expiración"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateExpirationPolizaTodoRiesgo}
                                                invalidLabel={errors.dateExpirationPolizaTodoRiesgo}
                                                value={dateExpirationPolizaTodoRiesgo}
                                                error={errors.dateExpirationPolizaTodoRiesgo ? true : false}
                                                onChange={this.handleChangeDateExpirationPolizaTodoRiesgo}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl component="fieldset" className={classes.formControl}>
                                        <FormLabel component="legend">Visible Para:</FormLabel>
                                        <RadioGroup
                                            aria-label="Visible Para:"
                                            name="visibilityPolizaTodoRiesgo"
                                            className={classes.group}
                                            value={visibilityPolizaTodoRiesgo}
                                            onChange={this.handleOnChange}
                                        >
                                            <FormControlLabel value="destinoWeb" control={<Radio />} label="Plataforma Web" />
                                            <FormControlLabel value="destinoApp" control={<Radio />} label="Aplicación Movil" />
                                            <FormControlLabel value="destinoAll" control={<Radio />} label="Todos" />
                                        </RadioGroup>
                                    </FormControl>
                                    <Typography color="error" variant="caption">
                                        {errors.visibilityPolizaTodoRiesgo}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </div>

                        <div className={classNames(classes.containerSectionForm, classes.containerSectionFormColor)}>
                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={4}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                name="checkBoxRevisionTecnoMecanica"
                                                checked={checkBoxRevisionTecnoMecanica}
                                                value="checkBoxRevisionTecnoMecanica"
                                                onChange={this.handleOnChange}
                                            />
                                        }
                                        label="Revisión Tecno Mecánica"
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.checkboxRevisionTecnoMecanica}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <TextField
                                        margin="dense"
                                        id="cdaRevisionTecnoMecanica"
                                        name="cdaRevisionTecnoMecanica"
                                        value={cdaRevisionTecnoMecanica}
                                        label="CDA"
                                        error={errors.cdaRevisionTecnoMecanica ? true : false}
                                        fullWidth
                                        onChange={this.handleOnChange}
                                        required
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.cdaRevisionTecnoMecanica}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <TextField
                                        margin="dense"
                                        id="numberRevisionRevisionTecnoMecanica"
                                        name="numberRevisionRevisionTecnoMecanica"
                                        value={numberRevisionRevisionTecnoMecanica}
                                        label="Número de Revisión"
                                        error={errors.numberRevisionRevisionTecnoMecanica ? true : false}
                                        fullWidth
                                        onChange={this.handleOnChange}
                                        required
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.numberRevisionRevisionTecnoMecanica}
                                    </Typography>
                                </Grid>
                            </Grid>

                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={6}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Expiración"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateExpirationRevisionTecnoMecanica}
                                                invalidLabel={errors.dateExpirationRevisionTecnoMecanica}
                                                value={dateExpirationRevisionTecnoMecanica}
                                                error={errors.dateExpirationRevisionTecnoMecanica ? true : false}
                                                onChange={this.handleChangeDateExpirationRevisionTecnoMecanica}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl component="fieldset" className={classes.formControl}>
                                        <FormLabel component="legend">Visible Para:</FormLabel>
                                        <RadioGroup
                                            aria-label="Visible Para:"
                                            name="visibilityRevisionTecnoMecanica"
                                            className={classes.group}
                                            value={visibilityRevisionTecnoMecanica}
                                            onChange={this.handleOnChange}
                                        >
                                            <FormControlLabel value="destinoWeb" control={<Radio />} label="Plataforma Web" />
                                            <FormControlLabel value="destinoApp" control={<Radio />} label="Aplicación Movil" />
                                            <FormControlLabel value="destinoAll" control={<Radio />} label="Todos" />
                                        </RadioGroup>
                                    </FormControl>
                                    <Typography color="error" variant="caption">
                                        {errors.visibilityRevisionTecnoMecanica}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </div>

                        <div className={classes.containerSectionForm}>
                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={4}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                name="checkBoxCambioAceite"
                                                checked={checkBoxCambioAceite}
                                                value="checkBoxCambioAceite"
                                                onChange={this.handleOnChange}
                                            />
                                        }
                                        label="Cambio de Aceite"
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.checkboxCambioAceite}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Realización"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateRealizatationCambioAceite}
                                                invalidLabel={errors.dateRealizatationCambioAceite}
                                                value={dateRealizatationCambioAceite}
                                                error={errors.dateRealizatationCambioAceite ? true : false}
                                                onChange={this.handleChangeDateRealitationCambioAceite}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Expiración"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateExpirationCambioAceite}
                                                invalidLabel={errors.dateExpirationCambioAceite}
                                                value={dateExpirationCambioAceite}
                                                error={errors.dateExpirationCambioAceite ? true : false}
                                                onChange={this.handleChangeDateExpirationCambioAceite}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl component="fieldset" className={classes.formControl}>
                                        <FormLabel component="legend">Visible Para:</FormLabel>
                                        <RadioGroup
                                            aria-label="Visible Para:"
                                            name="visibilityCambioAceite"
                                            className={classes.group}
                                            value={visibilityCambioAceite}
                                            onChange={this.handleOnChange}
                                        >
                                            <FormControlLabel value="destinoWeb" control={<Radio />} label="Plataforma Web" />
                                            <FormControlLabel value="destinoApp" control={<Radio />} label="Aplicación Movil" />
                                            <FormControlLabel value="destinoAll" control={<Radio />} label="Todos" />
                                        </RadioGroup>
                                    </FormControl>
                                    <Typography color="error" variant="caption">
                                        {errors.visibilityCambioAceite}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </div>

                        <div className={classNames(classes.containerSectionForm, classes.containerSectionFormColor)}>
                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={4}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                name="checkBoxSynchronization"
                                                checked={checkBoxSynchronization}
                                                value="checkBoxSynchronization"
                                                onChange={this.handleOnChange}
                                            />
                                        }
                                        label="Sincronización"
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.checkboxSincronizacion}
                                    </Typography>
                                </Grid>


                                <Grid item xs={12} sm={4}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Realización"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateRealizatationSynchronization}
                                                invalidLabel={errors.dateRealizatationSynchronization}
                                                value={dateRealizatationSynchronization}
                                                error={errors.dateRealizatationSynchronization ? true : false}
                                                onChange={this.handleChangeDateRealitationSynchronization}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>
                                <Grid item xs={12} sm={4}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                id="dateExpiration"
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Expiración"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateExpirationSynchronization}
                                                invalidLabel={errors.dateExpirationSynchronization}
                                                value={dateExpirationSynchronization}
                                                error={errors.dateExpirationSynchronization ? true : false}
                                                onChange={this.handleChangeDateExpirationSynchronization}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl component="fieldset" className={classes.formControl}>
                                        <FormLabel component="legend">Visible Para:</FormLabel>
                                        <RadioGroup
                                            aria-label="Visible Para:"
                                            name="visibilitySynchronization"
                                            className={classes.group}
                                            value={visibilitySynchronization}
                                            onChange={this.handleOnChange}
                                        >
                                            <FormControlLabel value="destinoWeb" control={<Radio />} label="Plataforma Web" />
                                            <FormControlLabel value="destinoApp" control={<Radio />} label="Aplicación Movil" />
                                            <FormControlLabel value="destinoAll" control={<Radio />} label="Todos" />
                                        </RadioGroup>
                                    </FormControl>
                                    <Typography color="error" variant="caption">
                                        {errors.visibilitySincronizacion}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </div>

                        <div className={classes.containerSectionForm}>
                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={4}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                name="checkBoxAlineacionBalanceo"
                                                checked={checkBoxAlineacionBalanceo}
                                                value="checkBoxAlineacionBalanceo"
                                                onChange={this.handleOnChange}
                                            />
                                        }
                                        label="Alineación || Balanceo"
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.checkboxAlineacionBalanceo}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} sm={4}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Realización"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateRealizatationAlineacionBalanceo}
                                                invalidLabel={errors.dateRealizatationAlineacionBalanceo}
                                                value={dateRealizatationAlineacionBalanceo}
                                                error={errors.dateRealizatationAlineacionBalanceo ? true : false}
                                                onChange={this.handleChangeDateRealitationAlineacionBalanceo}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>
                                <Grid item xs={12} sm={4}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Expiración"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateExpirationAlineacionBalanceo}
                                                invalidLabel={errors.dateExpirationAlineacionBalanceo}
                                                value={dateExpirationAlineacionBalanceo}
                                                error={errors.dateExpirationAlineacionBalanceo ? true : false}
                                                onChange={this.handleChangeDateExpirationAlineacionBalanceo}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl component="fieldset" className={classes.formControl}>
                                        <FormLabel component="legend">Visible Para:</FormLabel>
                                        <RadioGroup
                                            aria-label="Visible Para:"
                                            name="visibilityAlineacionBalanceo"
                                            className={classes.group}
                                            value={visibilityAlineacionBalanceo}
                                            onChange={this.handleOnChange}
                                        >
                                            <FormControlLabel value="destinoWeb" control={<Radio />} label="Plataforma Web" />
                                            <FormControlLabel value="destinoApp" control={<Radio />} label="Aplicación Movil" />
                                            <FormControlLabel value="destinoAll" control={<Radio />} label="Todos" />
                                        </RadioGroup>
                                    </FormControl>
                                    <Typography color="error" variant="caption">
                                        {errors.visibilityAlineacionBalanceo}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </div>

                        <div className={classNames(classes.containerSectionForm, classes.containerSectionFormColor)}>
                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={4}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                name="checkBoxCambioLlantas"
                                                checked={checkBoxCambioLlantas}
                                                value="checkBoxCambioLlantas"
                                                onChange={this.handleOnChange}
                                            />
                                        }
                                        label="Cambio de LLantas"
                                    />
                                    <Typography color="error" variant="caption">
                                        {errors.checkboxCambioLlantas}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Realización"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateRealizatationCambioLLantas}
                                                invalidLabel={errors.dateRealizatationCambioLLantas}
                                                value={dateRealizatationCambioLLantas}
                                                error={errors.dateRealizatationCambioLLantas ? true : false}
                                                onChange={this.handleChangeDateRealitationCambioLlantas}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                        <Grid container className={classes.grid} justify="space-around">
                                            <DatePicker
                                                margin="normal"
                                                fullWidth
                                                label="Fecha Expiración"
                                                format="yyyy/MM/dd"
                                                required
                                                invalidDateMessage={errors.dateExpirationCambioLLantas}
                                                invalidLabel={errors.dateExpirationCambioLLantas}
                                                value={dateExpirationCambioLLantas}
                                                error={errors.dateExpirationCambioLLantas ? true : false}
                                                onChange={this.handleChangeDateExpirationCambioLlantas}
                                            />
                                        </Grid>
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl component="fieldset" className={classes.formControl}>
                                        <FormLabel component="legend">Visible Para:</FormLabel>
                                        <RadioGroup
                                            aria-label="Visible Para:"
                                            name="visibilityCambioLLantas"
                                            className={classes.group}
                                            value={visibilityCambioLLantas}
                                            onChange={this.handleOnChange}
                                        >
                                            <FormControlLabel value="destinoWeb" control={<Radio />} label="Plataforma Web" />
                                            <FormControlLabel value="destinoApp" control={<Radio />} label="Aplicación Movil" />
                                            <FormControlLabel value="destinoAll" control={<Radio />} label="Todos" />
                                        </RadioGroup>
                                    </FormControl>
                                    <Typography color="error" variant="caption">
                                        {errors.visibilityCambioLlantas}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                </DialogContent>

                {errors.message ? (
                    <Typography align="center" color="error" variant="caption">
                        {errors.message}
                    </Typography>
                ) : null}

                <DialogActions>
                    <FormHelperText>{CAMPOSOBLIGATORIOS}</FormHelperText>
                    <Button onClick={this.handleSubmit(id)} color="secondary">
                        {id ? BUTTONSAVE : BUTTONADD}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogAddAndUpdate.propTypes = {
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    backAfterInsert: PropTypes.func.isRequired,
    handleOnClose: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogAddAndUpdate);
