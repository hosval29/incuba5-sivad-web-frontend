import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { PDFExport } from '@progress/kendo-react-pdf';

//Material UI
import {
    Typography,
    IconButton,
    Slide,
    Dialog,
    AppBar,
    Toolbar,
    Grid,
    Paper,
    Table,
    TableRow,
    TableBody,
    TableCell,
    List,
    // ListSubheader,
    ListItem,
    // ListItemSecondaryAction,
    ListItemText,
    Tooltip,
    withStyles,
    Avatar
} from '@material-ui/core';
// import ToggleButton from '@material-ui/lab/ToggleButton';
// import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import SaveAltIcon from '@material-ui/icons/SaveAlt';

//Constants
import {
    ID,
    DATE,
    // USERGESTOR,
    // PLATEVEHICLE,
    // DETAILINSPECTION,
    CLOSEDIALOG
} from '../../../../../../utils/constants.js';

//Utils
import http from '../../../../../../utils/http';

//Media
import Logo from '../../../../../../assets/images/logo.png'

const styles = theme => ({
    flex: {
        flex: 1
    },
    body: {
        padding: '1rem'
    },
    paper: {
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    subtitle: {
        fontWeight: '500'
    },
    tableCell: {
        backgroundColor: '#68debd'
    },
    tableCellTd: {
        backgroundColor: '#eeeeee',
        width: '50%'
    },
    tableCellTh: {
        width: '50%'
    },
    listItems: {
        padding: '0px',
        width: '100%'
    },
    listItem: {
        display: 'flex',
        borderBottom: '1px solid #9e9e9e',
        alignItems: 'center'
    },
    titleItem: {
        position: 'relative'
    },
    listItemText: {
        padding: 0
    },
    listItemTextListSubItem: {
        fontSize: '0.75rem',
        color: '#9e9e9e'
    },
    numberManifest: {
        display: 'flex',
        flexDirection: 'column',
        alignSelf: 'center',
        padding: 10
    },
    gridLogo: {
        display: 'flex',
        alignSelf: 'center',
    }
});

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class DialogDetailDataVehicle extends Component {
    pdfExportComponent;

    state = {
        errors: [],
        data: {}
    };

    componentWillMount = () => {
        this.getData(this.props.id);
    };

    getData = id => {
        this._isMounted = true;
        http
            .get(`/api/vehicles/${id}`)
            .then(result => {
                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                }
            })
            .catch(err => {
                switch (err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    };

    handleClose = () => {
        this.props.handleClose(CLOSEDIALOG);
    };

    exportPDFWithComponent = () => {
        this.pdfExportComponent.save();
    };

    render() {
        const { classes, open, title } = this.props;
        const { data } = this.state;

        const messages = [
            {
                id: 1,
                primary: 'Incuba5 SAS',
                secondary: "Nit: 111-111-111-0",
                logo: Logo,
            }
        ];

        return (
            <Dialog
                fullScreen
                open={open}
                onClose={this.handleClose}
                TransitionComponent={Transition}
            >
                <AppBar position="relative" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            onClick={this.handleClose}
                            aria-label="Close"
                        >
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" className={classes.flex}>
                            {title}
                        </Typography>
                        <div className={classes.row}>
                            <Tooltip title="Exportar" placement="top">
                                <IconButton
                                    aria-haspopup="true"
                                    onClick={this.exportPDFWithComponent}
                                    color="inherit"
                                >
                                    <SaveAltIcon />
                                </IconButton>
                            </Tooltip>
                        </div>
                    </Toolbar>
                </AppBar>
                <PDFExport
                    paperSize="A4"
                    scale={0.5}
                    fileName={`INFO-VEHICULO-PLACA-(${data.plate}).pdf`}
                    ref={component => (this.pdfExportComponent = component)}
                >
                    {JSON.stringify(data) !== '{}' ? (
                        <div className={classes.body}>
                            <Grid container spacing={24}>
                                <Grid item style={{ backgroundColor: '#eee' }} xs={12} sm={12} md={12}>
                                    <Paper>
                                        <Grid container>
                                            <Grid item xs={12} sm={6} md={6} className={classes.gridLogo}>

                                                <List className={classes.list}>
                                                    {messages.map(({ id, primary, secondary, logo }) => (
                                                        <Fragment key={id}>
                                                            <ListItem button>
                                                                <Avatar alt="Profile Picture" src={logo} />
                                                                <ListItemText primary={primary} secondary={secondary} />
                                                            </ListItem>
                                                        </Fragment>
                                                    ))}
                                                </List>

                                            </Grid>

                                            <Grid item xs={12} sm={6} md={6} className={classes.numberManifest}>

                                                <Typography
                                                    property
                                                    variant="h5"
                                                    color="inherit"
                                                    gutterBottom
                                                    className={classes.flex}>
                                                    Información Detalle del Vehículo
                                                </Typography>

                                                <div style={{ display: 'flex' }}>
                                                    <Typography
                                                        variant="h6"
                                                        color="inherit"
                                                        noWrap
                                                        gutterBottom
                                                        style={{ marginRight: 10 }}
                                                        className={classes.subtitle}
                                                    >
                                                        Kilometraje Inicial:
                                                    </Typography>

                                                    <Typography
                                                        variant="h6"
                                                        color="error"
                                                        noWrap
                                                        gutterBottom
                                                        className={classes.subtitle}
                                                    >
                                                        {data.mileageInitial ? data.mileageInitial : 'Vacio'}
                                                    </Typography>
                                                </div>

                                                <div style={{ display: 'flex' }}>
                                                    <Typography
                                                        variant="h6"
                                                        color="inherit"
                                                        noWrap
                                                        gutterBottom
                                                        style={{ marginRight: 10 }}
                                                        className={classes.subtitle}
                                                    >
                                                        Kilometraje Actual:
                                                    </Typography>

                                                    <Typography
                                                        variant="h6"
                                                        color="error"
                                                        noWrap
                                                        gutterBottom
                                                        className={classes.subtitle}
                                                    >
                                                        {data.mileageCurrent ? data.mileageCurrent : 'Vacio'}
                                                    </Typography>
                                                </div>

                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12} sm={6} md={6}>
                                    <Paper className={classes.paper}>
                                        <Table className={classes.table}>
                                            <TableBody>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        {ID}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.id}
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        {DATE}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.createdAt}
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        Placa
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.plate}
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12} sm={6} md={6}>
                                    <Paper className={classes.paper}>
                                        <Table className={classes.table}>
                                            <TableBody>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        Tipo Inspección
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.TypesInspectionVehicle.type}
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        Propietario
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.Owner.name}
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        Estado Vehículo
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.VehiclesState.state}
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </Paper>
                                </Grid>

                                {data.dataVehicle.map((itemDataVehicle, index) => {
                                    return Object.keys(itemDataVehicle).map(item => (
                                        <Grid key={item} item xs={12} sm={12} md={12}>
                                            <Paper className={classes.paper}>
                                                <Typography
                                                    variant="subtitle1"
                                                    color="secondary"
                                                    noWrap
                                                    gutterBottom
                                                    className={classes.subtitle}
                                                >
                                                    {item}
                                                </Typography>
                                                <Grid container spacing={24}>
                                                    {itemDataVehicle[item].map((subItem, index) => {
                                                        if (item === 'Alertas') {

                                                            return Object.keys(subItem).map(itemSubItem => {

                                                                for (let [key, value] of Object.entries(
                                                                    subItem[itemSubItem]
                                                                )) {
                                                                    return (
                                                                        <Grid
                                                                            item
                                                                            xs={12}
                                                                            sm={6}
                                                                            md={6}
                                                                            key={index}
                                                                        >
                                                                            <Paper className={classes.paper}>
                                                                                <Typography
                                                                                    gutterBottom
                                                                                    variant="subtitle2"
                                                                                    color="primary"
                                                                                    noWrap
                                                                                    className={classes.subtitle}
                                                                                >
                                                                                    {'Alerta' + " " + value.inputTitle}
                                                                                </Typography>
                                                                                <Table className={classes.table}>
                                                                                    <TableBody>
                                                                                        {Object.values(subItem).map(
                                                                                            itemAlert =>
                                                                                                Object.entries(itemAlert).map(
                                                                                                    ([key, value]) => (
                                                                                                        <TableRow key={key}>
                                                                                                            <TableCell className={classes.tableCellTd}>
                                                                                                                {value.inputTitle}
                                                                                                            </TableCell>
                                                                                                            <TableCell
                                                                                                                className={
                                                                                                                    classes.tableCellTh
                                                                                                                }
                                                                                                                component="th"
                                                                                                                scope="row"
                                                                                                            >
                                                                                                                {value.value === 'true'
                                                                                                                    ? 'Programada'
                                                                                                                    : value.value === 'destinoAll'
                                                                                                                        ? 'Todos'
                                                                                                                        : value.value === 'destinoWeb'
                                                                                                                            ? 'Web' : value.value === 'destinoApp'
                                                                                                                                ? 'App' : value.value}
                                                                                                            </TableCell>
                                                                                                        </TableRow>
                                                                                                    )
                                                                                                )
                                                                                        )}
                                                                                    </TableBody>
                                                                                </Table>
                                                                            </Paper>
                                                                        </Grid>
                                                                    );
                                                                }
                                                            });
                                                        } else {
                                                            for (let [key, value] of Object.entries(
                                                                subItem
                                                            )) {
                                                                return (
                                                                    <Grid item xs={12} sm={4} md={4} key={index}>
                                                                        <Table className={classes.table}>
                                                                            <TableBody>
                                                                                <TableRow>
                                                                                    <TableCell
                                                                                        className={classes.tableCellTd}
                                                                                    >
                                                                                        {value.inputTitle}
                                                                                    </TableCell>
                                                                                    <TableCell
                                                                                        className={classes.tableCellTh}
                                                                                        component="th"
                                                                                        scope="row"
                                                                                    >
                                                                                        {value.value}
                                                                                    </TableCell>
                                                                                </TableRow>
                                                                            </TableBody>
                                                                        </Table>
                                                                    </Grid>
                                                                );
                                                            }
                                                        }
                                                    })}
                                                </Grid>
                                            </Paper>
                                        </Grid>
                                    ));
                                })}
                            </Grid>
                        </div>
                    ) : null}
                </PDFExport>
            </Dialog>
        );
    }
}

DialogDetailDataVehicle.propTypes = {
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired
};

export default withStyles(styles)(DialogDetailDataVehicle);
