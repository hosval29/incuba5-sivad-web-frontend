//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material-IU
import {
    Dialog,
    Button,
    IconButton,
    Typography,
    withStyles
} from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';

//Utils
import {
    BUTTONACEPT,
    // CRUDADD,
    // CRUDUPDATE,
    // CRUDDELETE,
    // CRUDACTIVAR,
    // MESSAGEDELETE,
    // MESSAGEACTIVAR,
    INSPECTIONDIABLED,
    MESSAGEDISABLEDINSPECTION,
    MESSAGEENABLEDINSPECTION,
    INSPECTIONENABLED,
    CLOSEDIALOGINSPECTIONENABLED
} from '../../../../../../utils/constants';
import http from '../../../../../../store/actions/http';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    },
    iconTitleDialog: {
        marginRight: 10
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit
    }
}))(MuiDialogActions);

class DialogInspectionEnabled extends Component {

    state = {
        error: ''
    }

    handleSubmit = (id, inspectionEnabled) => (e) => {
    console.log("OUTPUT: DialogInspectionEnabled -> handleSubmit -> inspectionEnabled", inspectionEnabled)

        e.preventDefault()
        http.put(`/api/vehicles/${id}/${inspectionEnabled}`).then((result) => {

            if (result.data.error) {
                this.setState({ error: result.data.error })
                return;
            }

            this.handleOnClose()
            if (inspectionEnabled === 1) {
                this.props.backAfterInsert(INSPECTIONENABLED, result.data.message)
            } else {
                this.props.backAfterInsert(INSPECTIONDIABLED, result.data.message)
            }

        })
        .catch(err => {
            switch(err.response.status) {
                case 400:
                    this.setState({ error: 'Error al conectarse con el servidor.' })
                    break;
                case 404:
                    this.setState({ error: 'Error al conectarse con el servidor.' })
                    break;
                case 500:
                    this.setState({ error: 'Error al conectarse con el servidor.' })
                    break;
                default:
                    this.setState({ error: 'Sin conexión al servidor' })
                    break;
            }
        });
    };

    handleOnClose = () => {
        this.props.handleOnClose(CLOSEDIALOGINSPECTIONENABLED)
    }

    render() {
        const { classes, open, id, inspectionEnabled, title } = this.props;
        const { error } = this.state
        return (
            <Dialog
                id="dialogEdit"
                fullWidth
                maxWidth="xs"
                open={open}
                onClose={this.handleOnClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleOnClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />
                    {inspectionEnabled === 0 ? INSPECTIONDIABLED : INSPECTIONENABLED} {title}
                </DialogTitle>
                <DialogContent>
                    <Typography component="div">
                        {inspectionEnabled === 0 ? MESSAGEDISABLEDINSPECTION : MESSAGEENABLEDINSPECTION}
                    </Typography>
                    <Typography component="div">¡Aceptar para continuar!.</Typography>

                    {error ? (
                        <Typography align="center" color="error" gutterBottom variant="caption">
                            {error}
                        </Typography>
                    ) : null}

                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleSubmit(id, inspectionEnabled)} color="secondary">
                        {BUTTONACEPT}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogInspectionEnabled.propTypes = {
    classes: PropTypes.object.isRequired,
    id: PropTypes.number.isRequired,
    open: PropTypes.bool.isRequired,
    inspectionEnabled: PropTypes.number.isRequired,
    backAfterInsert: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    handleOnClose: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogInspectionEnabled);
