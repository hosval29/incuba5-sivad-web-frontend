//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material-IU
import {
    Dialog,
    Button,
    IconButton,
    Typography,
    withStyles,
    FormControl,
    InputLabel,
    Select,
    MenuItem
} from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Material-IU Icon
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';

//Utils
import {
    BUTTONACEPT,
    // CRUDADD,
    CRUDUPDATE,
    CRUDDELETE,
    // CRUDACTIVAR,
    // CHANGESTATUSVEHICLE,
    // MESSAGEDELETE,
    // MESSAGEACTIVAR,
    CHANGESTATUS,
    MESSAGECHANGESTATUS
} from '../../../../../../utils/constants';
import http from '../../../../../../store/actions/http';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        marginButtom: theme.spacing.unit + 2
    },
    iconTitleDialog: {
        marginRight: 10
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit
    }
}))(MuiDialogActions);

class DialogChangeStatus extends Component {

    state = {
        dataVehicle: '',

        dataVehicleStates: [],
        vehicleState: '',

        errors: {}
    }

    componentWillMount = () => {
        this.getData(this.props.id)
        this.getDataVehicleStates()
    }

    getDataVehicleStates = () => {
        http
            .get('/api/vehiclesstates/vehicles')
            .then(result => {
                this.setState({ dataVehicleStates: result.data.data });
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    }

    getData = (id) => {
        http
            .get(`/api/vehicles/${id}`)
            .then(result => {
                this.setState({ dataVehicle: result.data.data });
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    }

    handleOnChange = (e) => {
        const name = e.target.name
        const value = e.target.value

        const errors = this.state.errors;

        switch (name) {
            case 'vehicleState':
                delete errors.idVehicleState;
                this.setState({ errors: errors, vehicleState: value })
                break;
            default:
                break;
        }
    }

    handleSubmit = (id, status) => (e) => {

        e.preventDefault()

        let idVehicleState = ''
        const vehicleState = this.state.dataVehicleStates.filter(item => item.state === this.state.vehicleState)

        if (vehicleState.length > 0) {
            idVehicleState = vehicleState[0].id
        }

        const dataDelete = {
            idVehicleState: idVehicleState.toString()
        }

        http.delete(`/api/vehicles/${id}`, { data: dataDelete })
            .then((result) => {
                if (result) {
                    this.handleOnClose()
                    this.props.backAfterInsert(CRUDUPDATE, result.data.message)
                }

            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    };

    handleOnClose = () => {
        this.props.handleOnClose(CRUDDELETE)
    }

    render() {
        const { classes, id, status, title, open } = this.props;
        const { dataVehicleStates, vehicleState, errors } = this.state
        return (
            <Dialog
                id="dialogEdit"
                fullWidth
                maxWidth="xs"
                open={open}
                onClose={this.handleOnClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleOnClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />
                    {CHANGESTATUS + " " + title}
                </DialogTitle>
                <DialogContent>
                    <Typography gutterBottom component="div">
                        {MESSAGECHANGESTATUS + " " + title + "?"}
                    </Typography>

                    <FormControl
                        className={classes.formControl}
                        error={errors.vehicleState ? true : false}
                        fullWidth
                        required
                    >
                        <InputLabel
                            required
                            error={errors.idVehicleState ? true : false}
                            htmlFor='vehicleState'
                        >
                            Estado Vehículo
                        </InputLabel>
                        <Select
                            error={errors.idVehicleState ? true : false}
                            value={vehicleState}
                            onChange={this.handleOnChange}
                            name='vehicleState'
                            id='vehicleState'
                            inputProps={{
                                id: 'vehicleState'
                            }}
                            className={classes.selectEmpty}
                        >
                            <MenuItem value="">
                                <em>Ninguno</em>
                            </MenuItem>
                            {dataVehicleStates.map((item, index) => (
                                <MenuItem key={index} value={item.state}>
                                    {item.state}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <Typography color="error" variant="caption">
                        {errors.idVehicleState}
                    </Typography>

                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleSubmit(id, status)} color="secondary">
                        {BUTTONACEPT}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogChangeStatus.propTypes = {
    classes: PropTypes.object.isRequired,
    id: PropTypes.number.isRequired,
    open: PropTypes.bool.isRequired,
    backAfterInsert: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    handleOnClose: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogChangeStatus);
