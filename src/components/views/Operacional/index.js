//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

//Material UI
import { withStyles, AppBar, Tabs, Tab } from '@material-ui/core';

//Components
import CategoryLicCon from './components/CategoryLicCon/CategoryLicCon'
import TypesAlerts from './components/TypesAlerts/TypesAlerts'
import TypesConstratos from './components/TypesContratos/TypesContratos'
import TypesInspectios from './components/TypesInspections/TypesInspections'
import StatesVehicles from './components/StatesVehicles/StatesVehicles'
import TypesIdentifications from './components/TypesIdentification/TypesIdenfitications'

//Utils
import { setToolbar } from '../../../store/actions/authActions'
import { OPERACIONAL, TYPESALERTS, TYPESINSPECTIONS, CATEGORYSLICCONDUCT, TYPESCONTRATO, TYPESIDENTIFICATIONS, TYPEIDENTIFICATION } from '../../../utils/constants'

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    appBar: {
        backgroundColor: theme.palette.background.paper,
        color: '#e0e0e0',
        position: 'fixed'
    }
});

function TabContainer(props) {

    switch (props.children) {
        case TYPESALERTS:
            return <TypesAlerts />;
            break;
        case TYPESINSPECTIONS:
            return <TypesInspectios />;
            break;
        case CATEGORYSLICCONDUCT:
            return <CategoryLicCon />;
            break;
        case TYPESCONTRATO:
            return <TypesConstratos />;
            break;
        case TYPESIDENTIFICATIONS:
            return <TypesIdentifications />;
            break;
        default:
            //Aqui va states vehicles
            return <StatesVehicles />;
            break;
    }
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired
};

class Operacional extends Component {

    state = {
        value: 0
    };
    handleChange = (event, value) => {
        this.setState({ value });
    };

    componentWillMount = () => {
        const toolbar = {
            title: OPERACIONAL,
            icon: ''
        }
        this.props.setToolbar(toolbar)
    }

    render() {
        const { classes, auth } = this.props;
        const { value } = this.state
        const { user } = auth;
        const modules = user.profiles.modules;
        const menuOperacional = modules.find(itemMenu => itemMenu.module === OPERACIONAL);
        console.log("OUTPUT: Operacional -> render -> menuOperacional", menuOperacional)
        const itemsOperacional = menuOperacional.items;
        return (
            <div className={classes.root}>
                <AppBar className={classes.appBar} position="static">
                    <Tabs
                        variant="scrollable"
                        scrollButtons="auto"
                        textColor="secondary"
                        value={value}
                        onChange={this.handleChange}
                    >
                        {itemsOperacional.map((itemOperacional, index) => (
                            <Tab
                                key={itemOperacional.id}
                                value={index}
                                label={itemOperacional.itemModule}
                            />
                        ))}
                    </Tabs>
                </AppBar>
                {itemsOperacional.map(
                    (itemOperacional, index) =>
                        value === index && (
                            <TabContainer key={index}>
                                {itemOperacional.itemModule}
                            </TabContainer>
                        )
                )}
            </div>
        )
    }
}

Operacional.propTypes = {
    classes: PropTypes.object.isRequired,
    setToolbar: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default compose(
    withStyles(styles),
    connect(mapStateToProps, { setToolbar }),
    withRouter
)(Operacional);
