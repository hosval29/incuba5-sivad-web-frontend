//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import 'date-fns';
import esLocale from "date-fns/locale/es";


//Material UI
import {
    Dialog,
    FormHelperText,
    TextField,
    FormControlLabel,
    Switch,
    Button,
    Typography,
    IconButton,
    withStyles,
    Grid
} from '@material-ui/core';
import {
    MuiPickersUtilsProvider,
    TimePicker,
    DatePicker
} from 'material-ui-pickers';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import DateFnsUtils from '@date-io/date-fns';

//Material UI || Icons
import CloseIcon from '@material-ui/icons/Close';

//Utils
import http from '../../../../../../store/actions/http';
import {
    CRUDUPDATE,
    CRUDADD,
    CAMPOSOBLIGATORIOS
} from '../../../../../../utils/constants';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    },
    grid: {
        width: '60%'
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography color="secondary" variant="h6">
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
        justifyContent: 'space-between'
    }
}))(MuiDialogActions);

export class DialogAdd extends Component {
    state = {
        open: this.props.open,
        code: '',
        aseguradora: '',
        status: true,
        errors: {}
    };

    componentWillMount = () => {
        if (this.props.id) {
            this.getData(this.props.id);
        }
    };

    handleChange = e => {
        if (e.target.id === 'status') {
            this.setState({ status: e.target.checked });
        } else {
            this.setState({ [e.target.id]: e.target.value });
        }
    };

    /*handleDateExpeditionChange = date => {
        this.setState({ dateExpedition: date });
    };

    handleDateExpirationChange = date => {
        this.setState({ dateExpiration: date });
    };*/

    handleClose = () => {
        this.props.handleOnClose(CRUDUPDATE);
    };

    handleSubmit = id => e => {
        const status = this.state.status ? 1 : 0;
        const dataAdd = {
			
            code: this.state.code.toString(),
            aseguradora: this.state.aseguradora,
            status: status.toString()
        };

        console.log("OUTPUT: DialogAdd -> dataAdd", dataAdd)

        if (!id) {
            http
                .post('/api/expirationsalerts', dataAdd)
                .then(result => {
                    this.handleClose();
                    this.props.backAfterInsert(CRUDADD, result.data.message);
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        } else {
            http
                .put(`/api/expirationsalerts/${id}`, dataAdd)
                .then(result => {
                    if (result) {
                        this.handleClose();
                        this.props.backAfterInsert(CRUDUPDATE, result.data.message);
                    }
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    getData = id => {
        if (id !== '') {
            http
                .get(`/api/expirationsalerts/${id}`)
                .then(result => {
                    const dataUpdate = result.data.data;
                    let status = false;
                    if (dataUpdate.status === 1) {
                        status = true;
                    }
                    this.setState({
                        code: dataUpdate.code.toString(),
                        aseguradora: dataUpdate.aseguradora,
                        status: status
                    });
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    render() {
        const { id, title, classes } = this.props;
        const {
            open,
            code,
            aseguradora,
            dateExpedition,
            dateExpiration,
            status,
            errors
        } = this.state;

        return (
            <Dialog
                fullWidth
                maxWidth="sm"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    {id ? CRUDUPDATE : CRUDADD} {title}
                </DialogTitle>
                <DialogContent>
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                
                                margin="dense"
                                id="code"
                                name="code"
                                value={code}
                                label="Código"
                                error={
                                    errors.code ? true : false || errors.message ? true : false
                                }
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.code}
                                {errors.message}
                            </Typography>
                            <br />

                            {/*<MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                <Grid container className={classes.grid} justify="space-around">
                                    <DatePicker
                                        id="dateExpedition"
                                        margin="normal"
                                        label="Fecha Expedición"
                                        format="yyyy/mm/dd"
                                        required
                                        invalidDateMessage={errors.dateExpedition}
                                        invalidLabel={errors.dateExpedition}
                                        value={dateExpedition}
                                        error={errors.dateExpedition ? true : false}
                                        onChange={this.handleDateExpeditionChange}
                                    />
                                </Grid>
                            </MuiPickersUtilsProvider>

                            <br />*/}

                            <FormControlLabel
                                control={
                                    <Switch
                                        required
                                        id="status"
                                        checked={status}
                                        onChange={this.handleChange}
                                        value="status"
                                    />
                                }
                                label="Estado"
                            />
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                
                                margin="dense"
                                id="aseguradora"
                                name="aseguradora"
                                value={aseguradora}
                                label="Aseguradora"
                                error={
                                    errors.aseguradora ? true : false || errors.message ? true : false
                                }
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.aseguradora}
                                {errors.message}
                            </Typography>

                            <br />

                            {/*<MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                <Grid container className={classes.grid} justify="space-around">
                                    <DatePicker
                                        id="dateExpiration"
                                        margin="normal"
                                        label="Fecha Expiración"
                                        format="yyyy/mm/dd"
                                        required
                                        error={errors.dateExpiration ? true : false}
                                        value={dateExpiration}
                                        onChange={this.handleDateExpirationChange}
                                    />
                                </Grid>
                            </MuiPickersUtilsProvider>*/}
                        </Grid>
                    </Grid>
                </DialogContent>
                {errors.message ? (
                    <Typography align="center" color="error" variant="caption">
                        {errors.message}
                    </Typography>
                ) : null}
                <DialogActions>
                    <FormHelperText>{CAMPOSOBLIGATORIOS}</FormHelperText>
                    <Button onClick={this.handleSubmit(id)} color="secondary">
                        Agregar
          </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogAdd.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    backAfterInsert: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    handleOnClose: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogAdd);
