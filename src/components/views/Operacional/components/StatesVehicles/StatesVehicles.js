//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

//Material UI
import { createMuiTheme, MuiThemeProvider, withStyles, Typography, Divider } from '@material-ui/core';

//Components
import CustomToolbar from '../../../components/CustomToolbar'
import DialogAdd from './components/DialogAddAndUpdate'
import SnackbarInfo from '../../../../../utils/snackbars/components/Snackbar/SnackbarInfo';
import CustomButtonGestion from '../../../components/CustomButtonGestion'
import DialogDelete from './components/DialogDelete';

//Utils
import http from '../../../../../store/actions/http';
import {
  STATUSINACTIVO,
  STATUSACTIVO,
  CRUDDELETE,
  CRUDUPDATE,
  CRUDADD,
  ROLE,
  ROLES,
  GESTIONBUTTON,
  STATESVEHICLES,
  STATEVEHICLE,
} from '../../../../../utils/constants'

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 8
  },
  divider: {
    marginBottom: theme.spacing.unit * 2
  }
});

class StatesVehicles extends Component {

  state = {
    data: [],
    open: false,
    openDialogEdit: false,
    _id: '',
    snackbarOpen: false,
    snackbarMessage: '',
    snackbarVariant: '',
  }

  getMuiTheme = () =>
    createMuiTheme({
      palette: {
        primary: {
          main: '#2096c6',
          dark: '#00587c'
        },
        secondary: {
          light: '#68debd',
          main: '#2eac8d',
          dark: '#005337'
        },
        error: {
          dark: '#b00020',
          main: '#b00020'
        },
        background: {
          default: '#F0F0F0',
          paper: '#FFFFFF'
        },
        text: {
          primary: '#000000',
          secondary: '#404e67',
          disabled: '#212121',
          hint: '#404e67'
        }
      },
      typography: {
        useNextVariants: true,
        fontFamily: ['"Oswald"', 'sans-serif'].join(',')
      },
      overrides: {
        MuiToolbar: {
          root: {
            backgroundColor: '#f5f5f5',
            marginTop: 10,
            marginBottom: 10,
            boxShadow:
              '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)'
          }
        },
        MUIDataTableHeadCell: {
          fixedHeader: {
            color: '#FFFFFF',
            backgroundColor: '#00587c'
          }
        }
      }
    });

  componentWillMount() {
    this.getData();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getData = () => {
    this._isMounted = true;
    http
      .get('/api/vehiclesstates')
      .then(result => {

        if (this._isMounted) {
          this.setState({ data: result.data.data });
        }
      })
      .catch(err => {
        switch (err.response.status) {
          case 400:
            this.setState({ errors: err.response.data })
            break;
          case 404:
            this.setState({ errors: err.response.data })
            break;
          case 500:
            this.setState({ errors: err.response.data })
            break;
          default:
            this.setState({ errors: [] })
            break;
        }
      });
  };

  handleClickAdd = e => {
    this.setState({ open: !this.state.open });

  };

  handleClickButtonGestion = (gestion, idUser) => {

    switch (gestion) {
      case GESTIONBUTTON[1]:
        this.setState({ openDialogEdit: !this.state.openDialogEdit, status: 0 })
        break;
      case GESTIONBUTTON[2]:
        this.setState({ openDialogEdit: !this.state.openDialogEdit, status: 1 })
        break;
      case GESTIONBUTTON[0]:

        this.setState({ open: !this.state.open })
        break;
      default:
        break
    }

    this.setState({ _id: idUser });
  }

  handleOnClose = (action) => {
    switch (action) {
      case CRUDUPDATE:
        this.setState({ open: !this.state.open })
        break;
      case CRUDDELETE:
        this.setState({ openDialogEdit: !this.state.openDialogEdit })
        break;
      default:
        break
    }

  }

  backAfterInsert = (action, message) => {
    switch (action) {
      case CRUDADD:
        this.setState({
          snackbarOpen: !this.state.snackbarOpen,
          snackbarMessage: message,
          snackbarVariant: 'success'
        });
        break;
      case CRUDUPDATE:
        this.setState({
          snackbarOpen: !this.state.snackbarOpen,
          snackbarMessage: message,
          snackbarVariant: 'info'
        });
        break;
      case CRUDDELETE:
        this.setState({
          snackbarOpen: !this.state.snackbarOpen,
          snackbarMessage: message,
          snackbarVariant: 'warning'
        });
        break;
    }

    this.setState({
      data: [],
      open: false,
      openDialogEdit: false,
      _id: '',
    })

    this.getData();
  }

  handleCloseSnackbar = e => {
    this.setState({ snackbarOpen: !this.state.snackbarOpen });
  };

  render() {
    const { classes } = this.props;
    const { data } = this.state;

    const columns = [
      { name: 'id', label: 'Id' },
      { name: 'state', label: 'Estado Vehículo' },
      {
        name: 'status',
        label: 'Estado',
        options: {
          isRowSelectable: null,
          customBodyRender: (value, tableMeta, updateValue) =>
            (value === 1 ? <Typography color="secondary" variant="caption">
              {STATUSACTIVO}
            </Typography> : <Typography color="error" variant="caption">
                {STATUSINACTIVO}
              </Typography>)
        }
      },
      {
        name: 'id',
        label: 'Gestión',
        options: {
          filter: true,
          sort: false,
          empty: true,
          isRowSelectable: null,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <CustomButtonGestion id={value} handleClickButtonGestion={this.handleClickButtonGestion} />
            );
          }
        }
      },
    ];

    const options = {
      filterType: "dropdown",
      responsive: "scroll",
      selectableRows: false,
      isRowSelectable: false,
      rowsSelected: false,
      textLabels: {
        body: {
          noMatch: "Lo sentimos, no se encontraron registros coincidentes",
          toolTip: "Ordenar",
        },
        toolbar: {
          search: 'Buscar...',
          downloadCsv: 'Descargar CSV',
          print: 'Imprimir',
          viewColumns: 'Columnas Visibles',
          filterTable: 'Filtro'
        },
        pagination: {
          next: 'Página Siguiente',
          previous: 'Página Anterior',
          rowsPerPage: 'Filas por paginas:',
          displayRows: 'de'
        }
      },
      customToolbar: () => {
        return (
          <CustomToolbar title={STATEVEHICLE} handleClickAdd={this.handleClickAdd} />
        );
      }
    };

    return (
      <div className={classes.root}>
        <Typography
          component="h1"
          variant="h4"
          color="textSecondary"
          noWrap
          className="title"
        >
          {STATESVEHICLES}
        </Typography>

        <Divider className={classes.divider} />

        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable data={data} columns={columns} options={options} />
        </MuiThemeProvider>
        {this.state.open ?
          (<DialogAdd
            open={this.state.open}
            backAfterInsert={this.backAfterInsert}
            id={this.state._id}
            title={STATEVEHICLE}
            handleOnClose={this.handleOnClose}
          />
          ) : null}
        {this.state.openDialogEdit ?
          (<DialogDelete
            open={this.state.openDialogEdit}
            backAfterInsert={this.backAfterInsert}
            id={this.state._id}
            status={this.state.status}
            title={STATEVEHICLE}
            handleOnClose={this.handleOnClose}
          />) : null}
        {this.state.snackbarOpen ?
          (<SnackbarInfo
            open={this.state.snackbarOpen}
            onClose={this.handleCloseSnackbar}
            variant={this.state.snackbarVariant}
            message={this.state.snackbarMessage}
          />) : null}
      </div>
    );
  }
}

StatesVehicles.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(StatesVehicles);
