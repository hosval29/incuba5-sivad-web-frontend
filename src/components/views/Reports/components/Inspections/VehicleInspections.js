//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import MUIDataTable from 'mui-datatables';
import { compose } from 'recompose';

//Material UI
import {
    createMuiTheme,
    MuiThemeProvider,
    withStyles
} from '@material-ui/core/styles';
import { IconButton, Typography } from '@material-ui/core';

//Icons
import { FileEyeOutline } from 'mdi-material-ui';

//Components
import DialogDetailInspection from './components/DialogDetailInspection';
import SnackbarInfo from '../../../../../utils/snackbars/components/Snackbar/SnackbarInfo';

//Utils
import {
    REPORTES,
    LISTINSPECCIONES,
    CRUDADD,
    CRUDUPDATE,
    NUMBERMANIFEST,
    GESTIONBUTTONINSPECTION,
    CLOSEDIALOGDETAILINSPECTION,
    CLOSEDIALOGNUMBERMANIFEST,
    CLOSEDIALOGNOVELTIESINSPECTION,
    NOVELTIES
} from '../../../../../utils/constants';
import { setToolbar } from '../../../../../store/actions/authActions';
import DialogAddNumberManifest from './components/DialogAddNumberManifest';
import http from '../../../../../utils/http';
import CustomButtonGestionInspection from '../../../components/CustomButtonGestionInspection';
import DialogNoveltiesInspection from './components/DialogNoveltiesInspection';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 4
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    }
});

class VehicleInspections extends Component {
    state = {
        data: [],
        open: false,
        openDialogAddNumberManifest: false,
        openDialogNoveltiesInspection: false,
        _id: '',
        snackbarOpen: false,
        snackbarMessage: '',
        snackbarVariant: ''
    };

    componentWillMount() {
        const toolbar = {
            title: REPORTES,
            icon: ''
        };
        this.props.setToolbar(toolbar);
        this.getData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            },
            overrides: {
                MuiToolbar: {
                    root: {
                        backgroundColor: '#f5f5f5',
                        marginTop: 10,
                        marginBottom: 10,
                        boxShadow:
                            '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)'
                    }
                },
                MUIDataTableHeadCell: {
                    fixedHeader: {
                        color: '#FFFFFF',
                        backgroundColor: '#00587c'
                    }
                }
            }
        });

    getData = () => {
        this._isMounted = true;
        http
            .get('/api/vehiclesinspections')
            .then(result => {
                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    handleOnClose = action => {
        switch (action) {
            case CLOSEDIALOGDETAILINSPECTION:
                this.setState({ open: !this.state.open });
                break;

            case CLOSEDIALOGNUMBERMANIFEST:
                this.setState({
                    openDialogAddNumberManifest: !this.state.openDialogAddNumberManifest
                });
                break;

            case CLOSEDIALOGNOVELTIESINSPECTION:
                this.setState({
                    openDialogNoveltiesInspection: !this.state.openDialogNoveltiesInspection
                });
                break;

            default:
                break;
        }

        this.setState({ _id: '' });
    };

    backAfterInsert = (action, message) => {
        switch (action) {
            case CRUDADD:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'success'
                });
                break;
            case CRUDUPDATE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;
            default:
                break;
        }

        this.setState({
            data: [],
            open: false,
            openDialogChangeStatus: false,
            openDialogDetailDataVehicle: false,
            openDialogAddNumberManifest: false,
            openDialogNoveltiesInspection: false,
            _id: ''
        });

        this.getData();
    };

    handleClickButtonGestion = (gestion, id) => {
        switch (gestion) {
            case GESTIONBUTTONINSPECTION[0]:
                this.setState({
                    openDialogAddNumberManifest: !this.state.openDialogAddNumberManifest
                });
                break;
            default:
                break;
        }

        this.setState({ _id: id });
    };

    handleCloseSnackbar = e => {
        this.setState({ snackbarOpen: !this.state.snackbarOpen });
    };

    render() {
        const { classes } = this.props;
        const {
            data,
            open,
            _id,
            openDialogAddNumberManifest,
            openDialogNoveltiesInspection,
            snackbarOpen,
            snackbarMessage,
            snackbarVariant
        } = this.state;

        const columns = [
            { name: 'id', label: 'Id' },
            { name: 'dateRegister', label: 'Fecha' },
            {
                name: 'User',
                label: 'Usuario Gestor',
                options: {
                    customBodyRender: value => value.name
                }
            },
            {
                name: 'Vehicle',
                label: 'Vehículo',
                options: {
                    customBodyRender: value => value.plate
                }
            },
            {
                name: 'id',
                label: 'Inspección',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <IconButton
                                onClick={() => this.setState({ open: !open, _id: value })}
                            >
                                <FileEyeOutline />
                            </IconButton>
                        );
                    }
                }
            },
            {
                name: 'numberManifest',
                label: 'Número Manifiesto',
                options: {
                    customBodyRender: (value, tableMeta, updateValue) =>
                        value === null ? (
                            <Typography color="error" variant="subtitle2">
                                Vacio
                            </Typography>
                        ) : (
                                <Typography color="secondary" variant="subtitle2">
                                    {value}
                                </Typography>
                            )
                }
            },
            {
                name: 'id',
                label: 'Novedades',
                options: {
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <IconButton
                                onClick={() =>
                                    this.setState({
                                        openDialogNoveltiesInspection: !openDialogNoveltiesInspection,
                                        _id: value
                                    })
                                }
                            >
                                <FileEyeOutline />
                            </IconButton>
                        );
                    }
                }
            },
            {
                name: 'id',
                label: 'Gestión',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <CustomButtonGestionInspection
                                id={value}
                                handleClickButtonGestion={this.handleClickButtonGestion}
                            />
                        );
                    }
                }
            }
        ];

        const options = {
            filterType: 'dropdown',
            responsive: 'scroll',
            selectableRows: false,
            isRowSelectable: false,
            rowsSelected: false,
            textLabels: {
                body: {
                    noMatch: 'Lo sentimos, no se encontraron registros coincidentes',
                    toolTip: 'Ordenar'
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                }
            }
        };

        return (
            <div className={classes.root}>
                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable
                        title={LISTINSPECCIONES}
                        data={data}
                        columns={columns}
                        options={options}
                    />
                </MuiThemeProvider>
                {open ? (
                    <DialogDetailInspection
                        open={open}
                        handleClose={this.handleOnClose}
                        _id={_id}
                    />
                ) : null}
                {openDialogAddNumberManifest ? (
                    <DialogAddNumberManifest
                        open={openDialogAddNumberManifest}
                        title={NUMBERMANIFEST}
                        handleOnClose={this.handleOnClose}
                        id={_id}
                        backAfterInsert={this.backAfterInsert}
                    />
                ) : null}
                {snackbarOpen ? (
                    <SnackbarInfo
                        open={snackbarOpen}
                        onClose={this.handleCloseSnackbar}
                        variant={snackbarVariant}
                        message={snackbarMessage}
                    />
                ) : null}
                {openDialogNoveltiesInspection ? (
                    <DialogNoveltiesInspection
                        open={openDialogNoveltiesInspection}
                        title={NOVELTIES}
                        handleClose={this.handleOnClose}
                        id={_id}
                    />
                ) : null}
            </div>
        );
    }
}

VehicleInspections.propTypes = {
    classes: PropTypes.object.isRequired,
    setToolbar: PropTypes.func.isRequired
};

const mapStateToProps = state => ({});

export default compose(
    withStyles(styles),
    connect(
        mapStateToProps,
        { setToolbar }
    ),
    withRouter
)(VehicleInspections);
