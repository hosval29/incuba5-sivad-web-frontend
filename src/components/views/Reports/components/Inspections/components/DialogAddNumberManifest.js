//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material-IU
import {
    Dialog,
    Button,
    IconButton,
    Typography,
    withStyles,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    TextField
} from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Material-IU Icon
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';

//Utils
import {
    BUTTONACEPT,
    CRUDADD,
    CRUDUPDATE,
    CRUDDELETE,
    CRUDACTIVAR,
    CHANGESTATUSVEHICLE,
    MESSAGEDELETE,
    MESSAGEACTIVAR,
    CHANGESTATUS,
    MESSAGECHANGESTATUS,
    CLOSEDIALOGNUMBERMANIFEST
} from '../../../../../../utils/constants.js';
import http from '../../../../../../utils/http';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        marginButtom: theme.spacing.unit + 2
    },
    iconTitleDialog: {
        marginRight: 10
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit
    }
}))(MuiDialogActions);

class DialogAddNumberManifest extends Component {

    state = {
        numberManifest: '',

        errors: {}
    }

    componentWillMount = () => {
        this.getData(this.props.id)
    }

    getData = id => {

        this._isMounted = true;
        http
            .get(`/api/vehiclesinspections/numbermanifest/${id}`)
            .then(result => {
                console.log("OUTPUT: DialogAddNumberManifest -> result", result)
                if (this._isMounted) {
                    this.setState({ numberManifest: result.data.data.numberManifest === null || '' ? '' :  result.data.data.numberManifest });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    handleOnChange = (e) => {
        const name = e.target.name
        const value = e.target.value

        const errors = this.state.errors;

        switch (name) {
            case 'numberManifest':
                delete errors.numberManifest;
                this.setState({ errors: errors, numberManifest: value })
                break;
            default:
                break;
        }
    }

    handleSubmit = (id) => (e) => {
        console.log("OUTPUT: DialogAddNumberManifest -> handleSubmit -> id", id)

        e.preventDefault()

        const dataAdd = {
            numberManifest: this.state.numberManifest
        }

        http.put(`/api/vehiclesinspections/numbermanifest/${id}`, dataAdd)
            .then((result) => {
                if (result) {
                    this.handleOnClose()
                    this.props.backAfterInsert(CRUDUPDATE, result.data.message)
                }

            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    handleOnClose = () => {
        this.setState({ numberManifest: '', errors: {} })
        this.props.handleOnClose(CLOSEDIALOGNUMBERMANIFEST)
    }

    render() {
        const { classes, id, title, open } = this.props;
        const { numberManifest, errors } = this.state
        return (
            <Dialog
                id="dialogEdit"
                fullWidth
                maxWidth="xs"
                open={open}
                onClose={this.handleOnClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleOnClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />
                    {CRUDADD + " " + title}
                </DialogTitle>
                <DialogContent>

                    <TextField
                        margin="dense"
                        id="numberManifest"
                        name="numberManifest"
                        value={numberManifest}
                        label="Número de Manifiesto"
                        error={errors.numberManifest ? true : false}
                        fullWidth
                        onChange={this.handleOnChange}
                        required
                    />
                    <Typography color="error" variant="caption">
                        {errors.numberManifest}
                    </Typography>

                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleSubmit(id)} color="secondary">
                        {BUTTONACEPT}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogAddNumberManifest.propTypes = {
    classes: PropTypes.object.isRequired,
    id: PropTypes.number.isRequired,
    open: PropTypes.bool.isRequired,
    backAfterInsert: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    handleOnClose: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogAddNumberManifest);
