//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

//Material UI
import {
    createMuiTheme,
    MuiThemeProvider,
    withStyles,
    Dialog,
    Typography,
    IconButton
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline, Delete } from 'mdi-material-ui';

//Utils
import { CLOSEDIALOGNOVELTIESINSPECTION } from '../../../../../../utils/constants';
import http from '../../../../../../utils/http';


const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 8
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    },
    iconTitleDialog: {
        marginRight: 10
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2,
        flexGrow: 1
    }
}))(MuiDialogContent);

class DialogNoveltiesInspection extends Component {
    state = {
        data: [],
        message: ''
    };

    componentWillMount = () => {
        this.getData(this.props.id);
    };

    componentWillUnmount() {
        this._isMounted = false;
    }

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            }
        });

    getData = idInspection => {
        if (idInspection) {
            this._isMounted = true;

            http
                .get(`/api/vehiclesinspections/novelties/${idInspection}`)
                .then(result => {
                    if (this._isMounted) {
                        if (result.data.data) {
                            this.setState({ data: result.data.data });
                        } else {
                            this.setState({ message: result.data.message });
                        }
                    }
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ message: err.response.data.message });
                            break;
                        case 404:
                            this.setState({ message: err.response.data.message });
                            break;
                        case 500:
                                this.setState({ message: err.response.data.message });
                            break;
                        default:
                                this.setState({ message: 'Error al comunicarse con el servidor' });
                            break;
                    }
                });
        }
    };

    handleClose = () => {
        this.props.handleClose(CLOSEDIALOGNOVELTIESINSPECTION);
    };

    render() {
        const { classes, title, open, id } = this.props;
        const { data, message } = this.state;

        let dataArrayNovelties = []

        if(data){
            data.map(item => {
                item.data.map(itemNovelties => {
                    dataArrayNovelties.push(itemNovelties)
                })
            })
        }

        const columns = [
            { name: 'dateNoveltie', label: 'Fecha Novedad' },
            { name: 'timeNoveltie', label: 'Hora Novedad' },
            { name: 'descriptionNoveltie', label: 'Descripción' },
            { name: 'observationNoveltie', label: 'Observación' }
        ];

        const options = {
            filterType: 'dropdown',
            responsive: 'scroll',
            selectableRows: false,
            isRowSelectable: false,
            rowsSelected: false,
            download: false,
            print: false,
            textLabels: {
                body: {
                    noMatch: 'Lo sentimos, no se encontraron registros coincidentes',
                    toolTip: "Ordenar",
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                },
                filter: {
                    all: 'Todos',
                    title: 'Filtrar',
                    reset: 'Limpiar Filtro'
                },
                viewColumns: {
                    title: 'Columas Visibles'
                }
            }
        };
        return (
            <Dialog
                fullWidth
                maxWidth="md"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />
                    {title} || Inspección : {id}
                </DialogTitle>

                <DialogContent>
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                        <MUIDataTable data={dataArrayNovelties} columns={columns} options={options} />
                    </MuiThemeProvider>
                </DialogContent>

                {message ? (
                    <Typography align="center" color="error" variant="caption">
                        {message}
                    </Typography>
                ) : null}
            </Dialog>
        );
    }
}

DialogNoveltiesInspection.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    handleClose: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired
};

export default withStyles(styles)(DialogNoveltiesInspection);
