import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { PDFExport } from '@progress/kendo-react-pdf';

//Material UI
import {
    Typography,
    IconButton,
    Slide,
    Dialog,
    AppBar,
    Toolbar,
    Grid,
    Paper,
    Table,
    TableRow,
    TableBody,
    TableCell,
    List,
    ListSubheader,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    Tooltip,
    withStyles,
    Avatar
} from '@material-ui/core';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import SaveAltIcon from '@material-ui/icons/SaveAlt';

//Constants
import {
    ID,
    DATE,
    USERGESTOR,
    PLATEVEHICLE,
    DETAILINSPECTION,
    CLOSEDIALOGDETAILINSPECTION
} from '../../../../../../utils/constants';

//Utils
import http from '../../../../../../utils/http';

//Media
import Logo from '../../../../../../assets/images/logo.png'

const styles = theme => ({

    flex: {
        flex: 1
    },
    body: {
        padding: '1rem'
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary
    },
    tableCell: {
        backgroundColor: '#68debd'
    },
    listItems: {
        padding: '0px',
        width: '100%'
    },
    listItem: {
        display: 'flex',
        borderBottom: '1px solid #9e9e9e',
        alignItems: 'center'
    },
    titleItem: {
        position: 'relative'
    },
    listItemText: {
        padding: 0
    },
    listItemTextListSubItem: {
        fontSize: '0.75rem',
        color: '#9e9e9e'
    },
    bigAvatar: {
        width: 120,
        height: 120,
    },
    imgAvatar: {
        width: '80%',
        height: 'auto'
    },
    numberManifest: {
        display: 'flex',
        flexDirection: 'column',
        alignSelf: 'center'
    }
});

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class DialogDetailInspection extends React.Component {
    pdfExportComponent;

    state = {
        open: false,
        data: {},
        formats: ['bold'],

        errors: {}
    };

    componentWillMount = () => {
        this.getData(this.props._id);
    };

    getData = id => {

        this._isMounted = true;
        http
            .get(`/api/vehiclesinspections/${id}`)
            .then(result => {
            console.log("OUTPUT: DialogDetailInspection -> result", result.data.data)
                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    handleClose = () => {
        this.props.handleClose(CLOSEDIALOGDETAILINSPECTION);
    };

    exportPDFWithComponent = () => {
        this.pdfExportComponent.save();
    };

    render() {
        const { classes, open } = this.props;
        const { data, formats } = this.state;
        let plateVehicle = ''
        const messages = [
            {
                id: 1,
                primary: 'Incuba5 SAS',
                secondary: "Nit: 111-111-111-0",
                logo: Logo,
            }
        ];

        return (
            <Dialog
                fullScreen
                open={open}
                onClose={this.handleClose}
                TransitionComponent={Transition}
            >
                <AppBar position="relative" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            onClick={this.handleClose}
                            aria-label="Close"
                        >
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" className={classes.flex}>
                            {DETAILINSPECTION}
                        </Typography>
                        <div className={classes.row}>
                            <Tooltip title="Exportar" placement="top">
                                <IconButton
                                    aria-haspopup="true"
                                    onClick={this.exportPDFWithComponent}
                                    color="inherit"
                                >
                                    <SaveAltIcon />
                                </IconButton>
                            </Tooltip>
                        </div>
                    </Toolbar>
                </AppBar>
                <PDFExport
                    paperSize="A4"
                    scale={0.45}
                    fileName={`INSPECCION(${data.id})-VEHICULO(${plateVehicle}).pdf`}
                    ref={component => (this.pdfExportComponent = component)}
                >
                    {JSON.stringify(data) != '{}' ? (
                        <div className={classes.body}>
                            <Grid container spacing={24}>
                                <Grid item style={{backgroundColor: '#eee'}} xs={12} sm={12} md={12}>
                                    <Paper>
                                        <Grid container>

                                            <Grid item xs={12} sm={6} md={6}>

                                                <List className={classes.list}>
                                                    {messages.map(({ id, primary, secondary, logo }) => (
                                                        <Fragment key={id}>
                                                            <ListItem button>
                                                                <Avatar alt="Profile Picture" classes={{ img : classes.imgAvatar }} src={logo} className={classes.bigAvatar} />
                                                                <ListItemText primary={primary} secondary={secondary} />
                                                            </ListItem>
                                                        </Fragment>
                                                    ))}
                                                </List>

                                            </Grid>

                                            <Grid item xs={12} sm={6} md={6} className={classes.numberManifest}>

                                                <Typography
                                                    property
                                                    variant="h4"
                                                    color="inherit"
                                                    className={classes.flex}>
                                                    Inspección Vehículo Detallada
                                                </Typography>

                                                <div style={{display: 'flex'}}>
                                                    <Typography
                                                        variant="h5"
                                                        color="inherit"
                                                        noWrap
                                                        gutterBottom
                                                        style={{marginRight: 10}}
                                                        className={classes.subtitle}
                                                    >
                                                        Número de Manifiesto:
                                                </Typography>

                                                    <Typography
                                                        variant="h5"
                                                        color="error"
                                                        noWrap
                                                        gutterBottom
                                                        className={classes.subtitle}
                                                    >
                                                        {data.numberManifest ? data.numberManifest : 'Vacio'}
                                                    </Typography>
                                                </div>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12} sm={6} md={6}>
                                    <Paper className={classes.paper}>
                                        <Table className={classes.table}>
                                            <TableBody>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        {ID}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.id}
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        {DATE}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.dateRegister}
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12} sm={6} md={6}>
                                    <Paper className={classes.paper}>
                                        <Table className={classes.table}>
                                            <TableBody>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        {USERGESTOR}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.User.name}
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow>
                                                    <TableCell className={classes.tableCell}>
                                                        {PLATEVEHICLE}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {data.Vehicle.plate}
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </Paper>
                                </Grid>

                                {data.inspection.map((tab, index) => {
                                    return Object.entries(tab).map(([key, value]) => (
                                        <Grid key={key} item xs={12} sm={4} md={4}>
                                            <Paper className={classes.paper}>
                                                <Typography variant="h6" gutterBottom>
                                                    {key}
                                                </Typography>
                                                <List className={classes.listItems}>
                                                    {value.map((subItem, index) => {
                                                        return Object.entries(subItem).map(([subItemKey, subItemValue]) => {
                                                            return <ListItem key={subItem}>
                                                                <List className={classes.listItems}>
                                                                    <ListSubheader className={classes.titleItem}>
                                                                        {subItemKey}
                                                                    </ListSubheader>
                                                                    {Object.entries(subItemValue).map(([subItemValueKey, subItemValueValue]) => (
                                                                        <ListItem
                                                                            classes={{ container: classes.listItem }}
                                                                            key={subItemValueKey}
                                                                        >
                                                                            <ListItemText
                                                                                classes={{
                                                                                    primary: classes.listItemTextListSubItem
                                                                                }}
                                                                                style={{ padding: '0px' }}
                                                                                primary={subItemValueKey}
                                                                            />
                                                                            <ListItemSecondaryAction
                                                                                style={{
                                                                                    transform: 'translateY(0)',
                                                                                    position: 'relative',
                                                                                    right: '0px'
                                                                                }}
                                                                            >
                                                                                <ToggleButtonGroup value={formats}>
                                                                                    <ToggleButton value="bold">
                                                                                        {subItemValueValue === '0' ? 'B' : subItemValueValue === '1' ? 'R' : 'M'}
                                                                                    </ToggleButton>
                                                                                </ToggleButtonGroup>

                                                                            </ListItemSecondaryAction>
                                                                        </ListItem>
                                                                    )
                                                                    )}
                                                                </List>
                                                            </ListItem>
                                                        })
                                                    })}
                                                </List>
                                            </Paper>
                                        </Grid>
                                    ))
                                })}
                            </Grid>
                        </div>
                    ) : null}
                </PDFExport>
            </Dialog>
        );
    }
}

DialogDetailInspection.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    _id: PropTypes.number.isRequired
};

export default withStyles(styles)(DialogDetailInspection);
