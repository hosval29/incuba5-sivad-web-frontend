import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DASHBOARD } from '../../../utils/constants'
import { setToolbar } from '../../../store/actions/authActions'
import { compose } from 'recompose';
import moment from 'moment'
import 'moment/locale/es'


import Chart from "react-apexcharts";

import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import { Typography, Grid, CardContent, Card, Paper } from '@material-ui/core';

import { CarTractionControl, CarOff, ClipboardAlertOutline, Alert } from 'mdi-material-ui'
import http from '../../../store/actions/http';

moment.locale('es')

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 4
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    },

    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    cardContent: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    cardSubTitle: {
        color: '#9e9e9e'
    },
    colorFirst: {
        color: '#24d2b5'
    },
    colorSecond: {
        color: '#ff9800'
    },
    colorThree: {
        color: '#6772e5'
    },
    colorFour: {
        color: '#ff5c6c'
    },
    paperCharCircle: {
        position: 'relative',
        display: 'flex',
        flexFlow: 'column',
        alignItems: 'center',
        justifyItems: 'center',
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
});

class DashBoard extends Component {

    state = {
        optionsCir: {
            labels: ["Inspecciones"],
            title: {
                text: 'Inspecciones KPI vs Inspecciones Realizadas (Día)',
                align: 'center',
                margin: 10,
                offsetX: 0,
                offsetY: 0,
                floating: false,
                style: {
                    fontSize: '14px',
                    color: '#9e9e9e'
                },
            },
            plotOptions: {
                radialBar: {
                    dataLabels: {
                        show: true,
                        value: {
                            show: true,
                            fontSize: '24px',
                            fontFamily: 'Roboto',
                            color: '#404e67',
                            offsetY: 16,
                            formatter: function (val) {
                                return val + '%'
                            }
                        }
                    }
                }
            },
        },

        seriesCir: [0],
        series: [],
        dataXaxis: {},
        data: {},
        error: ''
    };

    componentWillMount = () => {
        const toolbar = {
            title: DASHBOARD,
            icon: ''
        }
        this.props.setToolbar(toolbar)
        this.getData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getData = () => {
        this._isMounted = true;
        http
            .get('/api/dashboard')
            .then(result => {
                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                    const data = this.state.data

                    const noveltiesByMonth = data.noveltiesInspectionsByMonth
                    let dataNoveltiesByMonth = []
                    let months = moment.months()
                    var monthCurrent = moment().format('MMMM')
                 
                    //Este codigo crea los meses anteriores al actual
                    var idx = months.indexOf(monthCurrent)
                    while (idx !== -1) {
                        idx++
                        months.splice(idx)
                        months.map(item => {
                            let dataObjectNoveltiesByMonth = {}
                            dataObjectNoveltiesByMonth.month = item.toUpperCase()
                            dataObjectNoveltiesByMonth.count = "0"
                            dataNoveltiesByMonth.push(dataObjectNoveltiesByMonth)
                            idx = months.indexOf(monthCurrent, idx + 1)
                        });
                    }

                    noveltiesByMonth.map(item => {
                        var numberMonth = item.date
                        let dataObjectNoveltiesByMonth = {}
                        dataObjectNoveltiesByMonth.month = moment().month(numberMonth - 1).format('MMMM').toUpperCase()
                        dataObjectNoveltiesByMonth.count = item.count
                        var idx = dataNoveltiesByMonth.findIndex(x => x.month == dataObjectNoveltiesByMonth.month)
                        while (idx != -1) {
                            dataNoveltiesByMonth[idx] = dataObjectNoveltiesByMonth
                            idx = dataNoveltiesByMonth.indexOf(dataObjectNoveltiesByMonth, idx + 1);
                        }
                    })

                    const monthsXaxisCategories = dataNoveltiesByMonth.map(item => item.month)
                    const dataXaxis = {}
                    Object.assign(dataXaxis, { categories: monthsXaxisCategories })
                    const countSeries = dataNoveltiesByMonth.map(item => parseInt(item.count))

                    const dataSeries = [
                        {
                            name: "Novedades",
                            data: countSeries
                        }
                    ]

                    this.setState({ dataXaxis: dataXaxis, series: dataSeries })

                    const vehiclesInspections = data.vehiclesInspections
                    const Kpi = data.vehiclesOperando
                    const percentageKpi = (vehiclesInspections * 100) / Kpi
                    this.setState({ seriesCir: [Math.round(percentageKpi)] })
                }
            })
            .catch(err => {
            console.log("OUTPUT: DashBoard -> getData -> err", err.response)
    
                switch(err.response.status) {
                    case 400:
                        this.setState({ data: {}, error: err.response.data })
                        break;
                    case 404:
                        this.setState({ data: {}, error: err.response.data })
                        break;
                    case 500:
                        this.setState({ data: {}, error: err.response.data })
                        break;
                    default:
                        this.setState({ data: {}, error: [] })
                        break;
                }
                
            });
    };

    render() {
        const { classes } = this.props;
        const { data, dataXaxis } = this.state
        const options = {
            chart: {
                id: "basic-bar"
            },
            title: {
                text: 'Novedades (mes)',
                align: 'center',
                margin: 10,
                offsetX: 0,
                offsetY: 0,
                floating: false,
                style: {
                    fontSize: '14px',
                    color: '#9e9e9e'
                },
            },
            xaxis: {},
            yaxis: {
                title: {
                    text: 'Novedades'
                }
            },
        }
        options.xaxis = dataXaxis
        return (
            <div className={classes.root}>

                <Grid container spacing={24}>
                    <Grid item xs={12} sm={3}>
                        <Card className={classes.card}>
                            <CardContent className={classes.cardContent}>
                                <div>
                                    <Typography variant="h4" color="textSecondary">
                                        {data.vehiclesOperando ? data.vehiclesOperando : '0'}
                                    </Typography>
                                    <Typography variant="subtitle2" className={classes.cardSubTitle} gutterBottom>
                                        Vehiculos Operando
                                    </Typography>
                                </div>
                                <div>
                                    <CarTractionControl fontSize="large" className={classes.colorFirst} />
                                </div>
                            </CardContent>
                        </Card>

                    </Grid>
                    <Grid item xs={12} sm={3}>

                        <Card className={classes.card}>
                            <CardContent className={classes.cardContent}>
                                <div>
                                    <Typography variant="h4" color="textSecondary">
                                        {data.vehiclesNoOperando ? data.vehiclesNoOperando : '0'}
                                    </Typography>
                                    <Typography variant="subtitle2" className={classes.cardSubTitle} gutterBottom>
                                        Vehiculos No Operando
                                    </Typography>
                                </div>
                                <div>
                                    <CarOff fontSize="large" className={classes.colorSecond} />
                                </div>
                            </CardContent>
                        </Card>

                    </Grid>
                    <Grid item xs={12} sm={3}>

                        <Card className={classes.card}>
                            <CardContent className={classes.cardContent}>
                                <div>
                                    <Typography variant="h4" color="textSecondary">
                                        {data.vehiclesInspectionsNovelties ? data.vehiclesInspectionsNovelties : '0'}
                                    </Typography>
                                    <Typography variant="subtitle2" className={classes.cardSubTitle} gutterBottom>
                                        Novedades
                                    </Typography>
                                </div>
                                <div>
                                    <Alert fontSize="large" className={classes.colorThree} />
                                </div>
                            </CardContent>

                        </Card>

                    </Grid>
                    <Grid item xs={12} sm={3}>

                        <Card className={classes.card}>
                            <CardContent className={classes.cardContent}>
                                <div>
                                    <Typography variant="h4" color="textSecondary">
                                        {data.vehiclesNotifications || data.alertsInspections ? data.vehiclesNotifications + data.alertsInspections : '0'}
                                    </Typography>
                                    <Typography variant="subtitle2" className={classes.cardSubTitle} gutterBottom>
                                        Alertas
                                    </Typography>
                                </div>
                                <div>
                                    <ClipboardAlertOutline fontSize="large" className={classes.colorFour} />
                                </div>
                            </CardContent>

                        </Card>

                    </Grid>

                    <Grid item xs={12} sm={7}>
                        <Paper style={{ 'height': '360px' }} className={classes.paper}>
                            <Chart
                                options={options}
                                series={this.state.series}
                                type="bar"
                                width="500"
                            />
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={5}>
                        <Paper style={{ 'height': '360px' }} className={classes.paperCharCircle}>

                            <Chart
                                options={this.state.optionsCir}
                                series={this.state.seriesCir}
                                type="radialBar"
                                width="100%"
                                height="310"
                            />

                            <Typography variant="h6" color="textSecondary">
                                {`KPI: ${data.vehiclesOperando ? data.vehiclesOperando : '0'} `}
                            </Typography>

                        </Paper>
                    </Grid>
                </Grid>
            </div>

        )
    }
}

DashBoard.propTypes = {
    classes: PropTypes.object.isRequired,
    setToolbar: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    toolbar: state.toolbar
});

export default compose(
    withStyles(styles),
    connect(mapStateToProps, { setToolbar }),
    withRouter
)(DashBoard);
