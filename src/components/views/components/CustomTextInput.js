import React from "react";
import PropTypes from 'prop-types';
// import Tooltip from "@material-ui/core/Tooltip";
// import AddIcon from "@material-ui/icons/Add";
import { withStyles } from "@material-ui/core/styles";
import { TextField, Typography } from "@material-ui/core";

const styles = theme => ({
    fab: {
        marginLeft: theme.spacing.unit,
        color: theme.palette.background.paper
    }
})

class CustomTextInput extends React.Component {

    handleOnChange = e => {
        this.props.handleOnChange(e)
    }

    render() {
        const { name, label, focus, value, error } = this.props;

        return (
            <React.Fragment>
                <TextField
                    autoFocus={focus}
                    margin="dense"
                    name={name}
                    label={label}
                    error={error ? true : false}
                    fullWidth
                    onChange={this.handleOnChange}
                    required
                    value={value}
                />
                <Typography color="error" variant="caption">
                    {error}
                </Typography>
            </React.Fragment>
        );
    }

}

CustomTextInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    focus: PropTypes.bool.isRequired,
    value: PropTypes.any.isRequired,
    handleOnChange: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired
}

export default withStyles(styles, { name: "CustomTextInput" })(CustomTextInput);
