import React from "react";
import PropTypes from 'prop-types';
import Tooltip from "@material-ui/core/Tooltip";
import AddIcon from "@material-ui/icons/Add";
import { withStyles } from "@material-ui/core/styles";
import { Fab, TextField, Typography, FormControl, InputLabel, Select, MenuItem } from "@material-ui/core";

const styles = theme => ({
    fab: {
        marginLeft: theme.spacing.unit,
        color: theme.palette.background.paper
    }
})

class CustomSelectInput extends React.Component {

    handleOnChange = e => {
        this.props.handleOnChange(e)
    }

    render() {
        const { classes, name, label, value, data, error } = this.props;
        return (
            <React.Fragment>
                <FormControl
                    error={error ? true : false}
                    fullWidth
                    required
                >
                    <InputLabel
                        required
                        error={error ? true : false}
                        htmlFor={name}
                    >
                        {label}
                    </InputLabel>
                    <Select
                        error={error ? true : false}
                        value={value}
                        onChange={this.handleOnChange}
                        name={name}
                        id={name}
                        inputProps={{
                            id: name
                        }}
                        className={classes.selectEmpty}
                    >
                        <MenuItem value="">
                            <em>Ninguno</em>
                        </MenuItem>
                        {data.map((item, index) => (
                            <MenuItem key={index} value={item}>
                                {item}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <Typography color="error" variant="caption">
                    {error}
                </Typography>
            </React.Fragment>
        );
    }
}

CustomSelectInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    data: PropTypes.array.isRequired,
    handleOnChange: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired
}

export default withStyles(styles, { name: "CustomTextInput" })(CustomSelectInput);
