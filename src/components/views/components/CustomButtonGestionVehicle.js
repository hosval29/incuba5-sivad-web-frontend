//Dependencies
import React, { Component, Fragment } from "react";
import { toRenderProps, withState } from 'recompose';

//Meterial UI
import { withStyles } from "@material-ui/core/styles";
import { IconButton, Menu, MenuItem } from '@material-ui/core';

//Meterial UI || Icons
import MoreIcon from '@material-ui/icons/MoreVert'
import { GESTIONBUTTONVEHICLE } from "../../../utils/constants";

const styles = theme => ({
    fab: {
        marginLeft: theme.spacing.unit,
        color: theme.palette.background.paper
    }
})

const WithState = toRenderProps(withState('anchorEl', 'updateAnchorEl', null));

class CustomButtonGestion extends Component {

    handleClose = (e) => {
        this.props.handleClickAdd(encodeURI)
        //console.log("clicked on icon!");
    }

    handleClickButtonGestion = (gestion, id) => {
        this.props.handleClickButtonGestion(gestion, id)
    }

    render() {
        const { id } = this.props;

        return (
            <Fragment>
                <WithState>
                    {({ anchorEl, updateAnchorEl }) => {
                        const open = Boolean(anchorEl);
                        const handleClose = (menuItem) => {
                            this.handleClickButtonGestion(menuItem, id)
                            updateAnchorEl(null);
                        };

                        return (
                            <Fragment>
                           
                                    <IconButton
                                        aria-label="More"
                                        onClick={event => {
                                            updateAnchorEl(event.currentTarget);
                                        }}
                                        aria-owns={open ? 'long-menu' : undefined}
                                        aria-haspopup="true"
                                    >
                                        <MoreIcon />
                                    </IconButton>
               
                                <Menu
                                    id="render-props-menu"
                                    anchorEl={anchorEl}
                                    open={open}
                                    onClose={handleClose}
                                >
                                    {GESTIONBUTTONVEHICLE.map(menuItem => (
                                        <MenuItem
                                            key={menuItem}
                                            style={{ fontSize: '12px' }}
                                            onClick={() => handleClose(menuItem)}                                        >
                                            {menuItem}
                                        </MenuItem>
                                    ))}
                                </Menu>
                            </Fragment>
                        );
                    }}
                </WithState>
            </Fragment>
        );
    }

}

export default withStyles(styles, { name: "CustomButtonGestion" })(CustomButtonGestion);