import React from "react";
import Tooltip from "@material-ui/core/Tooltip";
import AddIcon from "@material-ui/icons/Add";
import { withStyles } from "@material-ui/core/styles";
import { Fab } from "@material-ui/core";

const styles = theme => ({
    fab: {
        marginLeft: theme.spacing.unit,
        color: theme.palette.background.paper
    }
})

class CustomToolbar extends React.Component {

    handleClickAdd = (e) => {
        this.props.handleClickAdd(encodeURI)
        //console.log("clicked on icon!");
    }

    render() {
        const { classes, title } = this.props;

        return (
            <React.Fragment>
                <Tooltip
                    title={`Agregar ${title}`}
                    aria-label="Agregar Rol"
                    placement="bottom-start"
                >
                    <Fab
                        color="secondary"
                        className={classes.fab}
                        size="small"
                        onClick={this.handleClickAdd}
                    >
                        <AddIcon />
                    </Fab>
                </Tooltip>
            </React.Fragment>
        );
    }

}

export default withStyles(styles, { name: "CustomToolbar" })(CustomToolbar);