//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

//Material UI
import {
    createMuiTheme,
    MuiThemeProvider,
    withStyles,
    Typography,
    Divider
} from '@material-ui/core';

//Components
import CustomToolbar from '../../../components/CustomToolbar';
import CustomButtonGestion from '../../../components/CustomButtonGestion';
import DialogAddAndUpdate from './components/DialogAddAndUpdate';
import DialogDelete from './components/DialogDelete';
import SnackbarInfo from '../../../../../utils/snackbars/components/Snackbar/SnackbarInfo';

//Utils
import http from '../../../../../store/actions/http';
import {
    STATUSINACTIVO,
    STATUSACTIVO,
    CRUDDELETE,
    CRUDUPDATE,
    CRUDADD,
    ROLE,
    ROLES,
    GESTIONBUTTON
} from '../../../../../utils/constants';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 8
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    }
});

class Roles extends Component {
    state = {
        data: [],
        status: '',
        open: false,
        openDialogEdit: false,
        _id: '',
        snackbarOpen: false,
        snackbarMessage: '',
        snackbarVariant: ''
    };

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            },
            overrides: {
                MuiToolbar: {
                    root: {
                        backgroundColor: '#f5f5f5',
                        marginTop: 10,
                        marginBottom: 10,
                        boxShadow:
                            '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)'
                    }
                },
                MUIDataTableHeadCell: {
                    fixedHeader: {
                        color: '#FFFFFF',
                        backgroundColor: '#00587c'
                    }
                }
            }
        });

    componentWillMount() {
        this.getData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getData = () => {
        this._isMounted = true;
        http
            .get('/api/roles')
            .then(result => {

                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    };

    handleClickAdd = e => {
        this.setState({ open: !this.state.open });
    };

    handleClose = () => {
        this.setState({ open: !this.state.open, _id: '', });
    }

    handleClickButtonGestion = (gestion, idUser) => {
        switch (gestion) {
            case GESTIONBUTTON[1]:
                this.setState({
                    openDialogEdit: !this.state.openDialogEdit,
                    status: 0
                });
                break;
            case GESTIONBUTTON[2]:
                this.setState({
                    openDialogEdit: !this.state.openDialogEdit,
                    status: 1
                });
                break;
            case GESTIONBUTTON[0]:
                this.setState({ open: !this.state.open });
                break;
            default:
                break;
        }

        this.setState({ _id: idUser });
    };

    backAfterInsert = (action, message) => {
        switch (action) {
            case CRUDADD:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'success'
                });
                break;
            case CRUDUPDATE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;
            case CRUDDELETE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'warning'
                });
                break;
        }

        this.setState({
            data: [],
            open: false,
            openDialogEdit: false,
            _id: ''
        });

        this.getData();
    };

    handleCloseSnackbar = e => {
        this.setState({ snackbarOpen: !this.state.snackbarOpen });
    };

    handleCloseDialogDelete = () => {
        this.setState({
            openDialogEdit: !this.state.openDialogEdit,
            status: ''
        })
    }

    render() {
        const { classes } = this.props;
        const { data, open, openDialogEdit, snackbarOpen } = this.state;

        const columns = [
            { name: 'id', label: 'Id' },
            { name: 'role', label: 'Rol' },
            {
                name: 'status',
                label: 'Estado',
                options: {
                    isRowSelectable: null,
                    customBodyRender: (value) =>
                        value === 1 ? (
                            <Typography color="secondary" variant="caption">
                                {STATUSACTIVO}
                            </Typography>
                        ) : (
                                <Typography color="error" variant="caption">
                                    {STATUSINACTIVO}
                                </Typography>
                            )
                }
            },
            {
                name: 'id',
                label: 'Gestión',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: (value) => {
                        return (
                            <CustomButtonGestion
                                id={value}
                                handleClickButtonGestion={this.handleClickButtonGestion}
                            />
                        );
                    }
                }
            }
        ];

        const options = {
            filterType: "dropdown",
            responsive: "scroll",
            selectableRows: false,
            textLabels: {
                body: {
                    noMatch: 'Lo sentimos, no se encontraron registros coincidentes',
                    toolTip: "Ordenar",
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                }
            },
            customToolbar: () => {
                return (
                    <CustomToolbar title={ROLE} handleClickAdd={this.handleClickAdd} />
                );
            }
        };

        return (
            <div className={classes.root}>
                <Typography
                    component="h1"
                    variant="h4"
                    color="textSecondary"
                    noWrap
                    className="title"
                >
                    {ROLES}
                </Typography>

                <Divider className={classes.divider} />

                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable data={data} columns={columns} options={options} />
                </MuiThemeProvider>
                {open ? (
                    <DialogAddAndUpdate
                        title={ROLE}
                        open={open}
                        handleCloseDialogAddAndUpdate={this.handleClose}
                        backAfterInsert={this.backAfterInsert}
                        id={this.state._id}
                    />
                ) : null}
                {openDialogEdit ? (
                    <DialogDelete
                        open={this.state.openDialogEdit}
                        handleClose={this.handleCloseDialogDelete}
                        backAfterInsert={this.backAfterInsert}
                        id={this.state._id}
                        status={this.state.status}
                    />
                ) : null}
                {snackbarOpen ? (
                    <SnackbarInfo
                        open={this.state.snackbarOpen}
                        onClose={this.handleCloseSnackbar}
                        variant={this.state.snackbarVariant}
                        message={this.state.snackbarMessage}
                    />
                ) : null}
            </div>
        );
    }
}

Roles.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Roles);
