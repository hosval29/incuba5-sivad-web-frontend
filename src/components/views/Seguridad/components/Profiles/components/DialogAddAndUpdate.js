//Dependecies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material UI
import {
    withStyles,
    Dialog,
    FormControl,
    InputLabel,
    FormHelperText,
    TextField,
    FormControlLabel,
    Divider,
    Switch,
    Button,
    Typography,
    IconButton
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';

//Utils
import http from '../../../../../../store/actions/http';
import {
    CAMPOSOBLIGATORIOS,
    PROFILE,
    CRUDADD,
    CRUDUPDATE,
    BUTTONADD
} from '../../../../../../utils/constants';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    },
    iconTitleDialog: {
        marginRight: 10
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
        justifyContent: 'space-between'
    }
}))(MuiDialogActions);

export class DialogAdd extends Component {
    state = {
        status: true,
        profile: '',
        errors: {}
    };

    componentWillMount = () => {
        this.getData(this.props.id);
    };

    getData = id => {
        if (id !== '') {
            http
                .get(`/api/profiles/${id}`)
                .then(result => {
                    const dataUpdate = result.data.data;
                    let status = false;
                    if (dataUpdate.status === 1) {
                        status = true;
                    }
                    this.setState({ profile: dataUpdate.profile, status: status });
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    handleChange = e => {
        if (e.target.id == 'status') {
            this.setState({ status: e.target.checked });
        }

        const errors = this.state.errors

        switch (e.target.name) {
            case 'profile':
                delete errors.profile
                break;
            default:
                break;
        }
        this.setState({ errors: errors, [e.target.name]: e.target.value })
    };

    handleClose = () => {
        this.props.handleClose();
    };

    handleSubmit = e => {
        const status = this.state.status ? 1 : 0;
        const dataAdd = {
            profile: this.state.profile.toString(),
            status: status
        };

        if (!this.props.id) {
            http
                .post('/api/profiles', dataAdd)
                .then(result => {
                    this.handleClose();
                    this.props.backAfterInsert(CRUDADD, result.data.message);
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });

        } else {
            http
                .put(`/api/profiles/${this.props.id}`, dataAdd)
                .then(result => {
                    if (result) {
                        this.handleClose();
                        this.props.backAfterInsert(CRUDUPDATE, result.data.message);
                    }
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    render() {
        const { classes, open, title, id } = this.props;
        const { status, profile, errors } = this.state;
        return (
            <Dialog
                fullWidth
                maxWidth="xs"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />
                    {id ? CRUDUPDATE : CRUDADD} {title}
                </DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="profile"
                        name="profile"
                        value={profile}
                        label={PROFILE}
                        error={
                            errors.profile ? true : false
                        }
                        fullWidth
                        onChange={this.handleChange}
                        required
                    />
                    <Typography color="error" variant="caption">
                        {errors.profile}
                    </Typography>
                    <br />

                    <FormControlLabel
                        control={
                            <Switch
                                required
                                id="status"
                                checked={this.state.status}
                                onChange={this.handleChange}
                                value="status"
                            />
                        }
                        label="Estado"
                    />

                    <br />
                    <Typography align="center" color="error" variant="caption">
                        {errors.message}
                    </Typography>

                </DialogContent>
                <DialogActions>
                    <FormHelperText>{CAMPOSOBLIGATORIOS}</FormHelperText>
                    <Button onClick={this.handleSubmit} color="secondary">
                        {BUTTONADD}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogAdd.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    handleClose: PropTypes.func.isRequired,
    backAfterInsert: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired
};

export default withStyles(styles)(DialogAdd);
