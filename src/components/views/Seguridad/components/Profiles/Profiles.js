//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

//Material UI
import { withStyles, createMuiTheme, MuiThemeProvider, Divider, Typography } from '@material-ui/core';

//Components
import CustomToolbar from '../../../components/CustomToolbar'
import CustomButtonGestion from '../../../components/CustomButtonGestion';
import DialogAddAndUpdate from './components/DialogAddAndUpdate'
import DialogDelete from './components/DialogDelete'
import SnackbarInfo from '../../../../../utils/snackbars/components/Snackbar/SnackbarInfo';

//Utils
import http from '../../../../../store/actions/http';
import {
    PROFILES,
    STATUSINACTIVO,
    STATUSACTIVO,
    CRUDDELETE,
    CRUDUPDATE,
    CRUDADD,
    PROFILE,
    GESTIONBUTTON,
} from '../../../../../utils/constants'


const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 8
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    }
});

class Profiles extends Component {
    state = {
        data: [],
        status: '',
        open: false,
        openDialogDelete: false,
        _id: '',
        snackbarOpen: false,
        snackbarMessage: '',
        snackbarVariant: '',
    }

    componentWillMount() {
        this.getData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getData = () => {
        this._isMounted = true;
        http
            .get('/api/profiles')
            .then(result => {

                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    handleClickAdd = e => {
        this.setState({ open: !this.state.open });
    };

    handleCloseDialogAddAndUpdate = () => {
        this.setState({ open: !this.state.open, _id: '', });
    }

    handleClickButtonGestion = (gestion, id) => {
        switch (gestion) {
            case GESTIONBUTTON[1]:
                this.setState({
                    openDialogDelete: !this.state.openDialogDelete,
                    status: 0
                });
                break;
            case GESTIONBUTTON[2]:
                this.setState({
                    openDialogDelete: !this.state.openDialogDelete,
                    status: 1
                });
                break;
            case GESTIONBUTTON[0]:
                this.setState({ open: !this.state.open });
                break;
            default:
                break;
        }

        this.setState({ _id: id });
    };

    handleCloseDialogDelete = () => {
        this.setState({
            openDialogDelete: !this.state.openDialogDelete,
        });
    }

    backAfterInsert = (action, message) => {
        switch (action) {
            case CRUDADD:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'success'
                });
                break;
            case CRUDUPDATE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;

            case CRUDDELETE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'warning'
                });
                break;
        }

        this.setState({
            _id: '',
        })

        this.getData();
    }

    handleCloseSnackbar = e => {
        this.setState({ snackbarOpen: !this.state.snackbarOpen });
    };

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            },
            overrides: {
                MuiToolbar: {
                    root: {
                        backgroundColor: '#f5f5f5',
                        marginTop: 10,
                        marginBottom: 10,
                        boxShadow:
                            '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)'
                    }
                },
                MUIDataTableHeadCell: {
                    fixedHeader: {
                        color: '#FFFFFF',
                        backgroundColor: '#00587c'
                    }
                }
            }
        });

    render() {
        const { classes } = this.props;
        const { data, open, openDialogDelete, _id, status } = this.state;

        const columns = [
            { name: 'id', label: 'Id' },
            { name: 'profile', label: 'Perfil' },
            {
                name: 'status',
                label: 'Estado',
                options: {
                    isRowSelectable: null,
                    customBodyRender: (value) =>
                        value === 1 ? (
                            <Typography color="secondary" variant="caption">
                                {STATUSACTIVO}
                            </Typography>
                        ) : (
                                <Typography color="error" variant="caption">
                                    {STATUSINACTIVO}
                                </Typography>
                            )
                }
            },
            {
                name: 'id',
                label: 'Gestión',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: (value) => {
                        return (
                            <CustomButtonGestion
                                id={value}
                                handleClickButtonGestion={this.handleClickButtonGestion}
                            />
                        );
                    }
                }
            }
        ];

        const options = {
            filterType: 'dropdown',
            responsive: 'scroll',
            selectableRows: false,
            isRowSelectable: false,
            rowsSelected: false,
            textLabels: {
                body: {
                    noMatch: 'Lo sentimos, no se encontraron registros coincidentes',
                    toolTip: "Ordenar",
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                }
            },
            customToolbar: () => {
                return (
                    <CustomToolbar title={PROFILE} handleClickAdd={this.handleClickAdd} />
                );
            }
        };

        // donde esta el boton 
        return (
            <div className={classes.root}>
                <Typography
                    component="h1"
                    variant="h4"
                    color="textSecondary"
                    noWrap
                    className="title"
                >
                    {PROFILES}
                </Typography>

                <Divider className={classes.divider} />

                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable data={data} columns={columns} options={options} />
                </MuiThemeProvider>
                {open ? (
                    <DialogAddAndUpdate
                        open={open}
                        title={PROFILE}
                        handleClose={this.handleCloseDialogAddAndUpdate}
                        backAfterInsert={this.backAfterInsert}
                        id={_id}
                    />
                ) : null}
                {openDialogDelete ? (
                    <DialogDelete
                        open={openDialogDelete}
                        title={PROFILE}
                        handleClose={this.handleCloseDialogDelete}
                        backAfterInsert={this.backAfterInsert}
                        id={_id}
                        status={status}
                    />
                ) : null}
                {this.state.snackbarOpen ? (<SnackbarInfo
                    open={this.state.snackbarOpen}
                    onClose={this.handleCloseSnackbar}
                    variant={this.state.snackbarVariant}
                    message={this.state.snackbarMessage}
                />) : null}
            </div>
        );
    }
}

Profiles.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Profiles);
