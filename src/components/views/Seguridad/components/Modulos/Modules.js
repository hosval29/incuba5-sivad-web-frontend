//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import MUIDataTable from 'mui-datatables';

//Material UI
import { withStyles, createMuiTheme, MuiThemeProvider, Typography, Divider, Button, Fab, IconButton } from '@material-ui/core';

//Icons
import { Sitemap, AccountBoxOutline } from 'mdi-material-ui';

//Components
import CustomToolbar from '../../../components/CustomToolbar';
import DialogAdd from './components/DialogAdd';
import DialogItems from './components/DialogItems'
import DialogProfiles from './components/DialogProfiles';

//Utils
import http from '../../../../../utils/http';
import {
    MODULES,
    MODULE,
    STATUSACTIVO,
    STATUSINACTIVO,
    ITEMSMODULES,
    PROFILES
} from '../../../../../utils/constants';


const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 8
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    }
});

const customStyles = {
    headTh: {
        '& th': { backgroundColor: '#F00' }
    },
    NameCell: {
        fontWeight: 900,
        backgroundColor: 'red'
    }
};

class Modules extends Component {
    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            },
            overrides: {
                MuiToolbar: {
                    root: {
                        backgroundColor: '#f5f5f5',
                        marginTop: 10,
                        marginBottom: 10,
                        boxShadow:
                            '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)'
                    }
                },
                MUIDataTableHeadCell: {
                    fixedHeader: {
                        color: '#FFFFFF',
                        backgroundColor: '#00587c'
                    }
                }
            }
        });

    state = {
        data: [],
        open: false,
        errors: {},
        module: '',
        status: true,
        profiles: [],
        profilesObject: [],
        profile: [],
        itemModule: '',
        itemsModules: [],
        openDialogItemsModules: false,
        items: [],
        idModule: '',
        openDialogProfiles: false
    };

    componentWillMount() {
        this.getData();
    }

    getData = () => {
        http
            .get('/api/modules')
            .then(result => {
                this.setState({ data: result.data.data });
            })
            .catch(error => { });
    };

    handleClickAdd = e => {
        this.setState({ open: !this.state.open });
        this.getDataProfiles();
    };

    getDataProfiles = () => {
        http
            .get('/api/profiles')
            .then(result => {
                if (result) {
                    this.setState({ profilesObject: result.data.profiles });
                }
                const profiles = this.state.profilesObject.map(item => item.profile);
                this.setState({ profiles: profiles });
            })
            .catch(error => {
                console.log('OUTPUT: Modules -> getDataProfiles -> error', error);
            });
    };

    handleClickClose = e => {
        this.setState({ open: !this.state.open });
    };

    handleChange = e => {
        if (e.target.name === 'profile') {
            this.setState({ [e.target.name]: e.target.value });
        }

        this.setState({ [e.target.id]: e.target.value });
    };

    handleClickAddItemModule = e => {
        e.preventDefault();
        if (this.state.itemModule) {
            let validateItemModule = false;
            validateItemModule = this.state.itemsModules.find(this.isItemModule);

            if (!validateItemModule) {
                const itemModuleList = {
                    itemModule: this.state.itemModule
                };

                if (this.state.itemsModules.length <= 0) {
                    const itemsModulesTemp = [];
                    itemsModulesTemp.push(itemModuleList);
                    this.setState({ itemsModules: itemsModulesTemp });
                } else {
                    const itemsModulesTemp = this.state.itemsModules;
                    itemsModulesTemp.push(itemModuleList);
                    this.setState({ itemsModules: itemsModulesTemp });
                }
                this.setState({ itemModule: '' });
                return;
            } else {
                const errorsObject = {
                    itemModule: 'Este item ya se encuentra registrado'
                };
                this.setState({ errors: errorsObject });
                return;
            }
        } else {
            const errorsObject = {
                itemModule: 'Campo Item Módulo obligatorio'
            };
            this.setState({ errors: errorsObject });
            return;
        }
    };

    handleOnClickDeleteItemModule = index => e => {
        const itemsModulesTemp1 = this.state.itemsModules;
        itemsModulesTemp1.splice(index, 1);

        if (itemsModulesTemp1.length <= 0) {
            this.setState({ itemsModules: [] });
        } else {
            this.setState({ itemsModules: itemsModulesTemp1 });
        }
    };

    isItemModule = itemModule => {
        return itemModule.itemModule === this.state.itemModule;
    };

    handleClickSubmit = e => {
        e.preventDefault();
        const status = this.state.status ? 1 : 0;
        const profilesAdd = [];
        let itemIdProfile = {};
        this.state.profile.map(profile => {
            const arrayItem = Object.values(this.state.profilesObject).filter(
                item => item.profile === profile
            );
            let idProfileAddArray = arrayItem.map(item => item.id);
            if (idProfileAddArray[0]) {
                itemIdProfile = { idProfile: idProfileAddArray[0] };
                profilesAdd.push(itemIdProfile);
            }
        });

        const dataAdd = {
            modulo: this.state.module,
            status: status,
            profiles: profilesAdd,
            items: this.state.itemsModules
        };

        http
            .post('/api/modules', dataAdd)
            .then(result => {
     
                this.setState({
                    data: [],
                    open: false,
                    errors: {},
                    module: '',
                    status: true,
                    profiles: [],
                    profilesObject: [],
                    profile: [],
                    itemModule: '',
                    itemsModules: []
                });
                this.getData();
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    handleClickItemsModules = value => e => {
       
        e.preventDefault()
        this.setState({ openDialogItemsModules: !this.state.openDialogItemsModules, idMudule: value })
    }

    handleCloseDialogItems = () => {
        this.setState({ openDialogItemsModules: !this.state.openDialogItemsModules })
    }

    handleCloseDialogProfiles = () => {
        this.setState({ openDialogProfiles: !this.state.openDialogProfiles })
    }

    render() {
        const { classes } = this.props;
        const { data, open, status, profiles, openDialogProfiles, itemsModules, openDialogItemsModules, idModule } = this.state;

        const columns = [
            { name: 'id', label: 'Id' },
            { name: 'module', label: 'Modulo' },
            {
                name: 'status',
                label: 'Estado',
                options: {
                    isRowSelectable: null,
                    customBodyRender: (value) =>
                        value === 1 ? (
                            <Typography color="secondary" variant="caption">
                                {STATUSACTIVO}
                            </Typography>
                        ) : (
                                <Typography color="error" variant="caption">
                                    {STATUSINACTIVO}
                                </Typography>
                            )
                }
            },
            {
                name: 'id',
                label: 'Items',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <IconButton onClick={() => (this.setState({ openDialogItemsModules: !this.state.openDialogItemsModules, idModule: value }))} >
                                <Sitemap />
                            </IconButton>

                        );
                    }
                }
            },
            {
                name: 'id',
                label: 'Perfiles',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <IconButton onClick={() => (this.setState({ openDialogProfiles: !this.state.openDialogProfiles, idModule: value }))} >
                                <AccountBoxOutline />
                            </IconButton>
                        );
                    }
                }
            }
        ];

        const options = {
            filterType: 'dropdown',
            responsive: 'scroll',
            selectableRows: false,
            isRowSelectable: false,
            rowsSelected: false,
            textLabels: {
                body: {
                    noMatch: 'Lo sentimos, no se encontraron registros coincidentes',
                    toolTip: "Ordenar",
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                }
            },
            customToolbar: () => {
                return (
                    <CustomToolbar title={MODULE} handleClickAdd={this.handleClickAdd} />
                );
            }
        };

        return (
            <div className={classes.root}>
                <Typography
                    component="h1"
                    variant="h5"
                    color="textSecondary"
                    noWrap
                    className="title"
                >
                    {MODULES}
                </Typography>

                <Divider className={classes.divider} />

                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable data={data} columns={columns} options={options} />
                </MuiThemeProvider>
                {open ? (
                    <DialogAdd
                        open={open}
                        title={MODULE}
                        checked={this.state.status}
                        errors={this.state.errors}
                        profiles={this.state.profiles}
                        profile={this.state.profile}
                        valueItemModule={this.state.itemModule}
                        itemsModules={itemsModules}
                        handleClickAddItemModule={this.handleClickAddItemModule}
                        handleOnClickDeleteItemModule={this.handleOnClickDeleteItemModule}
                        handleClose={this.handleClickClose}
                        handleChange={this.handleChange}
                        handleClickSubmit={this.handleClickSubmit}
                    />
                ) : null}
                {openDialogItemsModules ? (
                    <DialogItems
                        open={openDialogItemsModules}
                        title={ITEMSMODULES}
                        idModule={idModule}
                        handleClose={this.handleCloseDialogItems}
                    />
                ) : null}
                {openDialogProfiles ? (
                    <DialogProfiles
                        open={openDialogProfiles}
                        title={PROFILES}
                        idModule={this.state.idModule}
                        handleClose={this.handleCloseDialogProfiles}
                    />
                ) : null}
            </div>
        );
    }
}

Modules.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles, customStyles)(Modules);
