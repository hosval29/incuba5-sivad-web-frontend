//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material UI
import {
    withStyles,
    Dialog,
    FormHelperText,
    TextField,
    FormControlLabel,
    Divider,
    Switch,
    Button,
    Typography,
    IconButton,
    Grid,
    FormControl,
    InputLabel,
    Select,
    Fab,
    Paper,
    MenuItem,
    List,
    ListItem,
    ListItemAvatar,
    Avatar,
    ListItemText,
    ListItemSecondaryAction,
    Input,
    Chip
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';
import { SettingsOutline } from 'mdi-material-ui';

//Utils
import {
    CRUDUPDATE,
    CRUDADD,
    ROLE,
    BUTTONADD,
    MODULE,
    ITEMMODULE,
    PROFILE,
    CAMPOSOBLIGATORIOS,
    PROFILES
} from '../../../../../../utils/constants';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    },
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2
    },
    rootFab: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginBottom: theme.spacing.unit + 2
    },
    fab: {
        color: theme.palette.background.paper
    },
    divider: {
        marginBottom: theme.spacing.unit + 2
    },
    demo: {
        backgroundColor: theme.palette.background.paper
    },
    title: {
        margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        maxWidth: 300
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    chip: {
        margin: theme.spacing.unit / 4
    },
    noLabel: {
        marginTop: theme.spacing.unit * 3
    },
    iconTitleDialog: {
        marginRight: 10
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2,
        flexGrow: 1
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
        justifyContent: 'space-between'
    }
}))(MuiDialogActions);

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250
        }
    }
};

function getStyles(profile, that) {
    return {
        fontWeight:
            that.props.profile.indexOf(profile) === -1
                ? that.props.theme.typography.fontWeightRegular
                : that.props.theme.typography.fontWeightMedium
    };
}

class DialogAdd extends Component {
    handleClose = () => {
        this.props.handleClose();
    };

    render() {
        const {
            classes,
            open,
            title,
            id,
            errors,
            profiles,
            profile,
            itemsModules,
            valueItemModule,
            valueModule,
            handleChange,
            handleClickSubmit,
            handleClickAddItemModule,
            handleOnClickDeleteItemModule
        } = this.props;

        return (
            <Dialog
                fullWidth
                maxWidth="sm"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />
                    {id ? CRUDUPDATE : CRUDADD} {title}
                </DialogTitle>
                <DialogContent>
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="module"
                                name="module"
                                value={valueModule}
                                label={MODULE}
                                error={errors.modulo ? true : false}
                                fullWidth
                                onChange={handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.modulo || errors.message}
                            </Typography>
                            <br />

                            <FormControlLabel
                                control={
                                    <Switch
                                        required
                                        id="status"
                                        checked={this.props.checked}
                                        onChange={handleChange}
                                        value="checked"
                                    />
                                }
                                label="Estado"
                                style={{ marginBottom: '10' }}
                            />

                            <br />

                            <FormControl className={classes.formControl}>
                                <InputLabel
                                    required
                                    error={errors.profiles ? true : false}
                                    htmlFor="profile"
                                >
                                    {PROFILE}
                                </InputLabel>
                                <Select
                                    multiple
                                    id="profile"
                                    name="profile"
                                    required
                                    error={errors.profiles ? true : false}
                                    value={this.props.profile}
                                    onChange={this.props.handleChange}
                                    input={<Input id="profile" name="profile" />}
                                    renderValue={selected => (
                                        <div className={classes.chips}>
                                            {selected.map(value => (
                                                <Chip
                                                    key={value}
                                                    label={value}
                                                    className={classes.chip}
                                                />
                                            ))}
                                        </div>
                                    )}
                                    MenuProps={MenuProps}
                                >
                                    {this.props.profiles.map(name => (
                                        <MenuItem
                                            key={name}
                                            value={name}
                                            style={getStyles(name, this)}
                                        >
                                            {name}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <Typography color="error" variant="caption">
                                {errors.profiles}
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={6} style={{ backgroundColor: '#F0F0F0' }}>
                            <Paper className={classes.paper} elevation={1}>
                                <TextField
                                    autoFocus
                                    margin="dense"
                                    id="itemModule"
                                    name={ITEMMODULE}
                                    value={valueItemModule}
                                    label={ITEMMODULE}
                                    error={
                                        errors.items ? true : false || errors.message ? true : false
                                    }
                                    fullWidth
                                    onChange={handleChange}
                                    required
                                />
                                <Typography color="error" variant="caption">
                                    {errors.items}
                                </Typography>

                                <div className={classes.rootFab}>
                                    <Fab
                                        color="secondary"
                                        className={classes.fab}
                                        size="small"
                                        onClick={handleClickAddItemModule}
                                    >
                                        <AddIcon />
                                    </Fab>
                                </div>

                                <Divider className={classes.divider} />

                                <div>
                                    <Typography variant="h6" className={classes.title}>
                                        Listado de Items Modulos
                  </Typography>
                                    <div className={classes.demo}>
                                        <List>
                                            {itemsModules.map((itemModule, index) => (
                                                <ListItem key={index}>
                                                    <ListItemAvatar>
                                                        <Avatar>
                                                            <FolderIcon />
                                                        </Avatar>
                                                    </ListItemAvatar>
                                                    <ListItemText primary={itemModule.itemModule} />
                                                    <ListItemSecondaryAction>
                                                        <IconButton
                                                            onClick={handleOnClickDeleteItemModule(index)}
                                                            aria-label="Delete"
                                                        >
                                                            <DeleteIcon />
                                                        </IconButton>
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                            ))}
                                        </List>
                                    </div>
                                </div>
                            </Paper>
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <FormHelperText>{CAMPOSOBLIGATORIOS}</FormHelperText>
                    <Button onClick={handleClickSubmit} color="secondary">
                        {BUTTONADD}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogAdd.propTypes = {
    checked: PropTypes.bool.isRequired,
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    errors: PropTypes.object.isRequired,
    profiles: PropTypes.array.isRequired,
    handleClose: PropTypes.func.isRequired,
    handleClickSubmit: PropTypes.func.isRequired,
    handleClickAddItemModule: PropTypes.func.isRequired
};

export default withStyles(styles, { withTheme: true })(DialogAdd);
