//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

//Material UI
import {
    Dialog,
    Typography,
    IconButton,
    createMuiTheme,
    MuiThemeProvider,
    withStyles
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';

//Utils
import {
    PROFILES,
    STATUSACTIVO,
    STATUSINACTIVO,
    GESTIONBUTTON,
    CRUDADD,
    CRUDUPDATE,
    CRUDDELETE,
    PROFILE
} from '../../../../../../utils/constants';
import http from '../../../../../../utils/http';

//Components
import DialogDeleteProfileModule from './DialogDeleteProfileModule';
import SnackbarInfo from '../../../../../../utils/snackbars/components/Snackbar/SnackbarInfo';
import CustomButtonGestion from '../../../../components/CustomButtonGestion';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 8
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    },
    iconTitleDialog: {
        marginRight: 10
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2,
        flexGrow: 1
    }
}))(MuiDialogContent);

class ProfilesModules extends Component {
    state = {
        data: [],
        openDialogDeleteProfilesModules: false,
        _id: '',
        status: '',
        snackbarOpen: false,
        snackbarMessage: '',
        snackbarVariant: ''
    };

    componentWillMount = () => {
        this.getData(this.props.idModule);
    };

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            }
        });

    getData = idModule => {
        if (idModule) {
            http
                .get(`/api/modules/profiles/${idModule}`)
                .then(result => {
                    if (result) {
                        this.setState({ data: result.data.data });
                    }
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    handleClose = e => {
        e.preventDefault();
        this.props.handleClose();
    };

    handleCloseDialogDeleteProfileModule = () => {
        this.setState({
            openDialogDeleteProfilesModules: !this.state.openDialogDeleteProfilesModules
        });
    };

    handleClickButtonGestion = (gestion, idProfile) => {
        switch (gestion) {
            case GESTIONBUTTON[1]:
                this.setState({
                    openDialogDeleteProfilesModules: !this.state.openDialogDeleteProfilesModules,
                    status: 0
                });
                break;
            case GESTIONBUTTON[2]:
                this.setState({
                    openDialogDeleteProfilesModules: !this.state.openDialogDeleteProfilesModules,
                    status: 1
                });
                break;
            case GESTIONBUTTON[0]:
                this.setState({
                    openDialogDeleteProfilesModules: !this.state.openDialogDeleteProfilesModules,
                    status: 2
                });
                break;
            default:
                break;
        }

        this.setState({ _id: idProfile });
    }

    handleBackAfterInsert = (action, message) => {

        switch (action) {
            case CRUDADD:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'success'
                });
                break;
            case CRUDUPDATE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;
            case CRUDDELETE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'warning'
                });
                break;
        }

        this.getData(this.props.idModule);
    }

    handleCloseSnackbar = e => {
        this.setState({ snackbarOpen: !this.state.snackbarOpen });
    };

    render() {
        const {
            data,
            _id,
            status,
            openDialogDeleteProfilesModules,
            snackbarOpen,
            snackbarMessage,
            snackbarVariant
        } = this.state;

        const { classes, open, title, idModule } = this.props

        let Modulo = '';
        data.map(item =>
            item.modules.filter(itemModule => (Modulo = itemModule.module))
        );

        const columns = [
            { name: 'id', label: 'Id' },
            { name: 'profile', label: 'Perfil' },
            {
                name: "modules",
                label: 'Estado',
                options: {
                    isRowSelectable: null,
                    customBodyRender: (value) => {
                        let status = ''
                        value.map(item => (status = item.ModulesProfiles.status))

                        if (status === 1) {
                            return <Typography color="secondary" variant="caption">
                                {STATUSACTIVO}
                            </Typography>
                        } else {
                            return <Typography color="error" variant="caption">
                                {STATUSINACTIVO}
                            </Typography>
                        }
                    }
                }
            },
            {
                name: 'id',
                label: 'Gestión',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: (value) => {
                        return (
                            <CustomButtonGestion
                                id={value}
                                handleClickButtonGestion={this.handleClickButtonGestion}
                            />
                        );
                    }
                }
            }
        ];

        const options = {
            filterType: "dropdown",
            responsive: "scroll",
            selectableRows: false,
            print: false,
            download: false,
            rowsPerPage: 5,
            textLabels: {
                toolbar: {
                    search: 'Buscar...',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                }
            }
        };
        return (
            <Dialog
                fullWidth
                maxWidth="md"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />
                    {title + ' || ' + Modulo}
                </DialogTitle>

                <DialogContent>
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                        <MUIDataTable data={data} columns={columns} options={options} />
                    </MuiThemeProvider>
                </DialogContent>
                {openDialogDeleteProfilesModules ?
                    <DialogDeleteProfileModule
                        title={PROFILE}
                        open={openDialogDeleteProfilesModules}
                        handleClose={this.handleCloseDialogDeleteProfileModule}
                        backAfterInsert={this.handleBackAfterInsert}
                        id={_id}
                        idModule={idModule}
                        status={status}
                    /> : null}
                {snackbarOpen ? (
                    <SnackbarInfo
                        open={snackbarOpen}
                        onClose={this.handleCloseSnackbar}
                        variant={snackbarVariant}
                        message={snackbarMessage}
                    />
                ) : null}
            </Dialog>
        );
    }
}

ProfilesModules.propTypes = {
    classes: PropTypes.object.isRequired,
    idModule: PropTypes.number.isRequired,
    handleClose: PropTypes.func.isRequired
};

export default withStyles(styles)(ProfilesModules);
