//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

//Material UI
import {
    withStyles,
    createMuiTheme,
    MuiThemeProvider,
    Typography,
    Divider
} from '@material-ui/core';

//Utils
import http from '../../../../../store/actions/http';
import { HISTORYLOG } from '../../../../../utils/constants';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 8
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    }
});

class HistorySession extends Component {
    state = {
        data: [],
    };

    componentWillMount() {
        this.getData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            },
            overrides: {
                MuiToolbar: {
                    root: {
                        backgroundColor: '#f5f5f5',
                        marginTop: 10,
                        marginBottom: 10,
                        boxShadow:
                            '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)'
                    }
                },
                MUIDataTableHeadCell: {
                    fixedHeader: {
                        color: '#FFFFFF',
                        backgroundColor: '#00587c'
                    }
                }
            }
        });

    getData = () => {
        this._isMounted = true;
        http
            .get('/api/users/history/all')
            .then(result => {
                if (this._isMounted) {
                    this.setState({ data: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    render() {
        const { classes } = this.props;
        const { data } = this.state;
        const columns = [
            { name: 'user_session', label: 'Id', options: {
                customBodyRender: (value) => {
                    return value.historylogs.id
                }
            } },
            { name: 'name', label: 'Usuario' },
            {
                name: 'user_session', label: 'Fecha Inicio Sesión', options: {
                    customBodyRender: (value) => {
						//console.log("OUTPUT: render -> value", value)
                        return value.historylogs.dateStartSession === null ? 'Vacio' : value.historylogs.dateStartSession
                }}
            },
            {
                name: 'user_session', label: 'Fecha Fin Sesión', options: {
                    customBodyRender: (value) => {
						//console.log("OUTPUT: HistorySession -> render -> value", value)
                        return value.historylogs.dateEndSession === null ? 'Vacio' : value.historylogs.dateEndSession
                    }
                }
            }
        ];

        const options = {
            filterType: "dropdown",
            responsive: "scroll",
            selectableRows: false,
            isRowSelectable: false,
            rowsSelected: false,
            textLabels: {
                body: {
                    noMatch: 'Lo sentimos, no se encontraron registros coincidentes',
                    toolTip: "Ordenar",
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                }
            }
        }

        return (
            <div className={classes.root}>
                <Typography
                    component="h1"
                    variant="h4"
                    color="textSecondary"
                    noWrap
                    className="title"
                >
                    {HISTORYLOG}
                </Typography>

                <Divider className={classes.divider} />

                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable data={data} columns={columns} options={options} />
                </MuiThemeProvider>

            </div>
        );
    }
}

HistorySession.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(HistorySession);
