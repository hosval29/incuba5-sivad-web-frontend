import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';
import { toRenderProps, withState } from 'recompose';

//Styles
import { withStyles } from '@material-ui/core/styles';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Typography, Divider, IconButton, MenuItem, Menu } from '@material-ui/core';
import { AccountTie } from 'mdi-material-ui';
import MoreIcon from '@material-ui/icons/MoreVert'

//Components
import CustomToolbar from '../../../components/CustomToolbar'
import DialogAddAndUpdate from './components/DialogAddAndUpdate'
import SnackbarInfo from '../../../../../utils/snackbars/components/Snackbar/SnackbarInfo';
import DialogRolesUser from './components/DialogRolesUser'
import DialogDelete from './components/DialogDelete';

//Utils
import http from '../../../../../store/actions/http';
import {
    PROFILES,
    STATUSINACTIVO,
    STATUSACTIVO,
    CRUDDELETE,
    CRUDUPDATE,
    CRUDADD,
    GESTIONBUTTON,
    USER,
    GESTION_USER,
    ROLES,
    CRUDERROR,
} from '../../../../../utils/constants'

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 8
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    }
});

const WithState = toRenderProps(withState('anchorEl', 'updateAnchorEl', null));

class UserManagement extends Component {
    state = {
        data: [],
        open: false,
        openDialogDelete: false,
        _id: '',
        status: '',
        openMenuGestion: false,
        openDialogRoles: false,
        snackbarOpen: false,
        snackbarMessage: '',
        snackbarVariant: '',
        anchorEl: null,
    }

    componentWillMount() {
        this.getData()
        console.log("estoy aqui", "componentWillMount")
    }

    componentWillUnmount() {
        this._isMounted = false;
        console.log("estoy aqui", "componentWillUnmount")
    }

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            },
            overrides: {
                MuiToolbar: {
                    root: {
                        backgroundColor: '#f5f5f5',
                        marginTop: 10,
                        marginBottom: 10,
                        boxShadow:
                            '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)'
                    }
                },
                MUIDataTableHeadCell: {
                    fixedHeader: {
                        color: '#FFFFFF',
                        backgroundColor: '#00587c'
                    }
                }
            }
        });

    getData = () => {
        this._isMounted = true;
        http.get('/api/users').then((result) => {

            if (this._isMounted) {
                this.setState({ data: result.data.data })
            }
        })
        .catch(err => {
            switch(err.response.status) {
                case 400:
                    this.setState({ errors: err.response.data })
                    break;
                case 404:
                    this.setState({ errors: err.response.data })
                    break;
                case 500:
                    this.setState({ errors: err.response.data })
                    break;
                default:
                    this.setState({ errors: {} })
                    break;
            }
        });
    }

    handleClickDialogAdd = e => {
        this.setState({ open: !this.state.open });
    };

    handleCloseDialogAdd = () => {
        this.setState({ open: !this.state.open });
        this.setterStatesAll()
    }

    backAfterInsert = (action, message) => {
        switch (action) {
            case CRUDADD:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'success'
                });
                break;
            case CRUDUPDATE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;
            case CRUDDELETE:
                this.setState({
                    status: '',
                    openDialogDelete: !this.state.openDialogDelete,
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'warning'
                });
                break;
            case CRUDERROR:
                this.setState({
                    status: '',
                    openDialogDelete: !this.state.openDialogDelete,
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'error'
                });
                break;
            default:
                break
        }

        this.setState({ _id: '' })

        this.getData();
    }

    handleCloseSnackbar = e => {
        this.setState({
            snackbarOpen: !this.state.snackbarOpen,
            snackbarMessage: '',
            snackbarVariant: '',
        });
    };

    handleClickButtonGestion = (gestion, idUser) => {
        switch (gestion) {
            case GESTIONBUTTON[1]:
                this.setState({ openDialogDelete: !this.state.openDialogDelete, status: 0 })
                break;
            case GESTIONBUTTON[2]:
                this.setState({ openDialogDelete: !this.state.openDialogDelete, status: 1 })
                break;

            case GESTIONBUTTON[0]:
                this.setState({ open: !this.state.open })
                break;
            default:
                break
        }

        this.setState({ _id: idUser })
    }

    handleCloseMenuGestion = () => {
        this.setState({ anchorEl: null, openDialogDelete: !this.state.openDialogDelete });
    }

    handleCloseDialogRoles = () => {
        this.setState({ openDialogRoles: !this.state.openDialogRoles })
        this.getData();
    }

    handleClosDialogDelete = () => {
        this.setState({ openDialogDelete: !this.state.openDialogDelete, status: '' })
    }

    setterStatesAll = () => {
        this.setState({
            _id: '',
        })
    }

    render() {
        const { classes } = this.props;
        const { data, open, openDialogDelete, openDialogRoles, openMenuGestion, _id, status } = this.state;

        const columns = [
            { name: 'id', label: 'Id' },
            { name: 'name', label: 'Nombre' },
            { name: 'email', label: 'Correo' },
            { name: 'phone', label: 'Teléfono' },
            {
                name: 'Profile',
                label: 'Perfil',
                options: {
                    customBodyRender: (value) => value.profile
                }
            },

            {
                name: 'id',
                label: 'Roles',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <IconButton onClick={() => (this.setState({ openDialogRoles: !this.state.openDialogRoles, _id: value }))} >
                                <AccountTie />
                            </IconButton>

                        );
                    }
                }
            },
            {
                name: 'status',
                label: 'Estado',
                options: {
                    isRowSelectable: null,
                    customBodyRender: (value, tableMeta, updateValue) =>
                        (value === 1 ? <Typography color="secondary" variant="caption">
                            {STATUSACTIVO}
                        </Typography> : <Typography color="error" variant="caption">
                                {STATUSINACTIVO}
                            </Typography>)
                }
            },
            {
                name: 'id',
                label: 'Gestión',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <WithState>
                                {({ anchorEl, updateAnchorEl }) => {
                                    const open = Boolean(anchorEl);
                                    const handleClose = (menuItem, value) => {
                                        this.handleClickButtonGestion(menuItem, value)
                                        updateAnchorEl(null);
                                    };

                                    return (
                                        <Fragment>
                                            <IconButton
                                                aria-label="More"
                                                onClick={event => {
                                                    updateAnchorEl(event.currentTarget);
                                                }}
                                                aria-owns={open ? 'long-menu' : undefined}
                                                aria-haspopup="true"
                                            >
                                                <MoreIcon />
                                            </IconButton>
                                            <Menu
                                                id="render-props-menu"
                                                anchorEl={anchorEl}
                                                open={open}
                                                onClose={handleClose}
                                            >
                                                {GESTIONBUTTON.map(menuItem => (
                                                    <MenuItem
                                                        key={menuItem}
                                                        style={{ fontSize: '12px' }}
                                                        onClick={() => handleClose(menuItem, value) //this.handleClickButtonGestion(menuItem, value)
                                                        }
                                                    >
                                                        {menuItem}
                                                    </MenuItem>
                                                ))}
                                            </Menu>
                                        </Fragment>
                                    );
                                }}
                            </WithState>
                        );
                    }
                }
            },
        ];

        const options = {
            filterType: "dropdown",
            responsive: "scroll",
            selectableRows: false,
            textLabels: {
                body: {
                    noMatch: 'Lo sentimos, no se encontraron registros coincidentes',
                    toolTip: "Ordenar",
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                },
                filter: {
                    all: 'Todos',
                    title: 'Filtrar',
                    reset: 'Limpiar Filtro'
                },
                viewColumns: {
                    title: "Columas Visibles"
                },
            },
            customToolbar: () => {
                return (
                    <CustomToolbar title={USER} handleClickAdd={this.handleClickDialogAdd} />
                );
            }
        };

        return (
            <div className={classes.root}>
                <Typography
                    component="h1"
                    variant="h4"
                    color="textSecondary"
                    noWrap
                    className="title"
                >
                    {GESTION_USER}
                </Typography>

                <Divider className={classes.divider} />

                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable data={data} columns={columns} options={options} />
                </MuiThemeProvider>

                {this.state.open ? (
                    <DialogAddAndUpdate
                        title={USER}
                        open={this.state.open}
                        backAfterInsert={this.backAfterInsert}
                        handleClose={this.handleCloseDialogAdd}
                        idUser={this.state._id}
                    />
                ) : null}

                {openDialogRoles ? (
                    <DialogRolesUser
                        open={openDialogRoles}
                        title={ROLES}
                        handleCloseDialogRoles={this.handleCloseDialogRoles}
                        id={_id}
                    />
                ) : null}

                {openDialogDelete ? (
                    <DialogDelete
                        open={openDialogDelete}
                        handleClose={this.handleClosDialogDelete}
                        backAfterInsert={this.backAfterInsert}
                        idUser={_id}
                        status={status}
                    />) : null}

                {this.state.snackbarOpen ? (<SnackbarInfo
                    open={this.state.snackbarOpen}
                    onClose={this.handleCloseSnackbar}
                    variant={this.state.snackbarVariant}
                    message={this.state.snackbarMessage}
                />) : null}
            </div>
        )
    }
}

UserManagement.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserManagement);