//Dependencias React
import React, { Component } from 'react';

import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

//Dependencias Material-IU
import {
    Dialog,
    Button,
    IconButton,
    Typography
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Dependencias Material-IU Icon
import CloseIcon from '@material-ui/icons/Close';
import {
    BUTTONACEPT,
    CRUDUPDATE,
    CRUDDELETE,
    CRUDACTIVAR,
    MESSAGECONTINUAR,
    MESSAGEACTIVAR,
    MESSAGEDELETE,
    CRUDERROR
} from '../../../../../../utils/constants';
import http from '../../../../../../store/actions/http';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography color="secondary" variant="h6">
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit
    }
}))(MuiDialogActions);

class DialogEdit extends Component {

    handleSubmit = (id) => (e) => {

        e.preventDefault()
        const dataDelete = {
            status: this.props.status
        }
        console.log("OUTPUT: DialogEdit -> handleSubmit -> dataDelete", dataDelete)
        http.delete(`/api/users/${id}`, { data: dataDelete }).then((result) => {
            console.log("OUTPUT: DialogEdit -> handleSubmit -> result", result.data)
            this.handleClose()
            if(this.props.status === 0){
                this.props.backAfterInsert(CRUDDELETE, result.data.message)
            }else{
                this.props.backAfterInsert(CRUDUPDATE, result.data.message)
            }
           
        })
        .catch(err => {
            switch(err.response.status) {
                case 400:
                    this.props.backAfterInsert(CRUDERROR, err.response.data.message)
                    console.log("OUTPUT: DialogEdit -> handleSubmit -> error", err.response.data)
                    break;
                case 404:
                    this.props.backAfterInsert(CRUDERROR, err.response.data.message)
                    console.log("OUTPUT: DialogEdit -> handleSubmit -> error", err.response.data)
                    break;
                case 500:
                    this.props.backAfterInsert(CRUDERROR, err.response.data.message)
                    console.log("OUTPUT: DialogEdit -> handleSubmit -> error", err.response.data)
                    break;
                default:
                    this.props.backAfterInsert(CRUDERROR, err.response.data.message)
                    console.log("OUTPUT: DialogEdit -> handleSubmit -> error", err.response.data)
                    break;
            }
        });
    };

    handleClose = () => {
        this.props.handleClose()
    }

    render() {
        const { idUser, status, open } = this.props;
    
        return (
            <Dialog
                id="dialogEdit"
                fullWidth
                maxWidth="xs"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="alert-dialog-slide-title" onClose={this.handleClose}>
                    {status === 0 ? CRUDDELETE : CRUDACTIVAR} Usuario
                </DialogTitle>
                <DialogContent>
                    <Typography component="div">
                       {status === 0 ? MESSAGEDELETE : MESSAGEACTIVAR + 'Usuario'}
                </Typography>
                    <Typography component="div">{MESSAGECONTINUAR}</Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleSubmit(idUser)} color="secondary">
                        {BUTTONACEPT}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogEdit.propTypes = {
    classes: PropTypes.object.isRequired,
    idUser: PropTypes.number.isRequired,
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    status: PropTypes.number.isRequired,
    backAfterInsert: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogEdit);
