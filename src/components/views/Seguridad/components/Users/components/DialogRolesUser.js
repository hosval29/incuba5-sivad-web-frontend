//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

//Material UI
import {
    createMuiTheme,
    MuiThemeProvider,
    withStyles,
    Dialog,
    Typography,
    IconButton
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';

//Icons
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline, Delete } from 'mdi-material-ui';

//Utils
import {
    STATUSACTIVO,
    STATUSINACTIVO,
    GESTIONBUTTON,
    ROLE,
    CRUDADD,
    CRUDUPDATE,
    CRUDDELETE
} from '../../../../../../utils/constants';
import http from '../../../../../../utils/http';

//Components
import CustomButtonGestion from '../../../../components/CustomButtonGestion';
import DialogDeleteRoleUser from './DialogDeleteRoleUser';
import SnackbarInfo from '../../../../../../utils/snackbars/components/Snackbar/SnackbarInfo';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 8
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    },
    iconTitleDialog: {
        marginRight: 10
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary" variant="h6">
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2,
        flexGrow: 1
    }
}))(MuiDialogContent);

class DialogRolesUser extends Component {
    state = {
        data: [],
        idRole: '',
        status: '',
        openDialogDeleteRoleUser: false,
        snackbarOpen: false,
        snackbarMessage: '',
        snackbarVariant: ''
    };

    componentWillMount = () => {
        this.getData(this.props.id);
    };

    getMuiTheme = () =>
        createMuiTheme({
            palette: {
                primary: {
                    main: '#2096c6',
                    dark: '#00587c'
                },
                secondary: {
                    light: '#68debd',
                    main: '#2eac8d',
                    dark: '#005337'
                },
                error: {
                    dark: '#b00020',
                    main: '#b00020'
                },
                background: {
                    default: '#F0F0F0',
                    paper: '#FFFFFF'
                },
                text: {
                    primary: '#000000',
                    secondary: '#404e67',
                    disabled: '#212121',
                    hint: '#404e67'
                }
            },
            typography: {
                useNextVariants: true,
                fontFamily: ['"Oswald"', 'sans-serif'].join(',')
            }
        });

    getData = idUser => {
        if (idUser) {
            http
                .get(`/api/users/roles/${idUser}`)
                .then(result => {

                    if (result) {
                        this.setState({ data: result.data.data });
                    }
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    handleClose = () => {
        this.props.handleCloseDialogRoles()
    }

    handleCloseDialogDeleteRoleUser = () => {
        this.setState({ openDialogDeleteRoleUser: !this.state.openDialogDeleteRoleUser, status: '' })
    }

    handleClickButtonGestion = (gestion, idRole) => {
        switch (gestion) {
            case GESTIONBUTTON[1]:
                this.setState({
                    openDialogDeleteRoleUser: !this.state.openDialogDeleteRoleUser,
                    status: 0
                });
                break;
            case GESTIONBUTTON[2]:
                this.setState({
                    openDialogDeleteRoleUser: !this.state.openDialogDeleteRoleUser,
                    status: 1
                });
                break;
            case GESTIONBUTTON[0]:
                this.setState({
                    openDialogDeleteRoleUser: !this.state.openDialogDeleteRoleUser,
                    status: 2
                });
                break;
            default:
                break;
        }

        this.setState({ idRole: idRole });
    };

    handleBackAfterInsert = (action, data) => {

        switch (action) {
            case CRUDADD:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: data.message,
                    snackbarVariant: 'success'
                });
                break;
            case CRUDUPDATE:

                if (!data.messageUserInhabilitado) {
                    this.setState({
                        snackbarOpen: !this.state.snackbarOpen,
                        snackbarMessage: data.message,
                        snackbarVariant: 'info'
                    });
                } else {
                    this.setState({
                        snackbarOpen: !this.state.snackbarOpen,
                        snackbarMessage: data.message + " " + data.messageUserInhabilitado,
                        snackbarVariant: 'error'
                    });
                }

                break;
            case CRUDDELETE:
                if (!data.messageUserInhabilitado) {
                    this.setState({
                        snackbarOpen: !this.state.snackbarOpen,
                        snackbarMessage: data.message,
                        snackbarVariant: 'info'
                    });
                } else {
                    this.setState({
                        snackbarOpen: !this.state.snackbarOpen,
                        snackbarMessage: data.message + " " + data.messageUserInhabilitado,
                        snackbarVariant: 'error'
                    });
                }
                break;
        }

        this.getData(this.props.id);
    };

    handleCloseSnackbar = e => {
        this.setState({ snackbarOpen: !this.state.snackbarOpen });
    };

    render() {
        const { classes, title, open, id } = this.props
        const { data, openDialogDeleteRoleUser, status, idRole, snackbarOpen } = this.state;

        const columns = [
            { name: 'id', label: 'Id' },
            { name: 'role', label: 'Role' },
            {
                name: "users",
                label: 'Estado',
                options: {
                    isRowSelectable: null,
                    customBodyRender: (value) => {
                        let status = ''
                        value.map(item => (status = item.UsersRoles.status))

                        if (status === 1) {
                            return <Typography color="secondary" variant="caption">
                                {STATUSACTIVO}
                            </Typography>
                        } else {
                            return <Typography color="error" variant="caption">
                                {STATUSINACTIVO}
                            </Typography>
                        }
                    }
                }
            },
            {
                name: 'id',
                label: 'Gestión',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    isRowSelectable: null,
                    customBodyRender: value => {
                        return (
                            <CustomButtonGestion
                                id={value}
                                handleClickButtonGestion={this.handleClickButtonGestion}
                            />
                        );
                    }
                }
            }
        ];

        const options = {
            filterType: "dropdown",
            responsive: "scroll",
            selectableRows: false,
            isRowSelectable: false,
            rowsSelected: false,
            download: false,
            print: false,
            textLabels: {
                body: {
                    noMatch: "Lo sentimos, no se encontraron registros coincidentes"
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                },
                filter: {
                    all: 'Todos',
                    title: 'Filtrar',
                    reset: 'Limpiar Filtro'
                },
                viewColumns: {
                    title: "Columas Visibles"
                },
            },
        };
        return (
            <Dialog
                fullWidth
                maxWidth="md"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />
                    {title}
                </DialogTitle>

                <DialogContent>
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                        <MUIDataTable data={data} columns={columns} options={options} />
                    </MuiThemeProvider>
                </DialogContent>

                {openDialogDeleteRoleUser ? (
                    <DialogDeleteRoleUser
                        open={openDialogDeleteRoleUser}
                        title={ROLE}
                        idUser={id}
                        idRole={idRole}
                        status={status}
                        handleClose={this.handleCloseDialogDeleteRoleUser}
                        backAfterInsert={this.handleBackAfterInsert}
                    />
                ) : null}
                {snackbarOpen ? (
                    <SnackbarInfo
                        open={this.state.snackbarOpen}
                        onClose={this.handleCloseSnackbar}
                        variant={this.state.snackbarVariant}
                        message={this.state.snackbarMessage}
                    />
                ) : null}
            </Dialog>
        );
    }
}

DialogRolesUser.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    handleCloseDialogRoles: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired
};

export default withStyles(styles)(DialogRolesUser);
