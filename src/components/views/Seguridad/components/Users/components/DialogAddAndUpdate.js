//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material UI
import {
    withStyles,
    Dialog,
    FormControl,
    InputLabel,
    FormHelperText,
    TextField,
    FormControlLabel,
    Switch,
    Button,
    Typography,
    IconButton,
    Grid,
    Select,
    Input,
    Chip,
    MenuItem
} from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Icnons
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';

//Utils'
import http from '../../../../../../store/actions/http';
import {
    CAMPOSOBLIGATORIOS,
    PROFILE,
    CRUDADD,
    NAME,
    APELLIDO,
    PHONE,
    EMAIL,
    PASS,
    PASSCONFIRM,
    ROLES,
    CRUDUPDATE,
    BUTTONADD
} from '../../../../../../utils/constants';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },

    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        maxWidth: 300
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    chip: {
        margin: theme.spacing.unit / 4
    },
    noLabel: {
        marginTop: theme.spacing.unit * 3
    },
    iconTitleDialog: {
        marginRight: 10
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250
        }
    }
};

function getStyles(role, that) {
    return {
        fontWeight:
            that.state.dataRoles.indexOf(role) === -1
                ? that.props.theme.typography.fontWeightRegular
                : that.props.theme.typography.fontWeightMedium
    };
}

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
        justifyContent: 'space-between'
    }
}))(MuiDialogActions);

class DialogAdd extends Component {
    state = {
        status: true,
        errors: {},
        name: '',
        lastName: '',
        email: '',
        phone: '',
        pass: '',
        passConfirm: '',
        dataProfiles: [],
        idProfile: '',
        profile: '',
        dataRoles: [],
        roles: [],
        role: [],
        idUser: this.props.idUser
    };

    componentWillMount() {
        if (this.props.idUser) {
            this.getDataUser(this.props.idUser);
        }

        this.getDataProfiles();
        this.getDataRoles();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getDataProfiles = () => {
        this._isMounted = true;
        http
            .get('/api/profiles/profilesuser')
            .then(result => {
                if (this._isMounted) {
                    this.setState({ dataProfiles: result.data.data });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    getDataRoles = () => {
        this._isMounted = true;
        http
            .get('/api/roles/rolesuser')
            .then(result => {
                if (this._isMounted) {
                    this.setState({ dataRoles: result.data.data });
                    const roles = this.state.dataRoles.map(item => item.role);
                    this.setState({ roles: roles });
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    getDataUser = idUser => {
        if (idUser) {
            http
                .get(`/api/users/${idUser}`)
                .then(result => {
                    const user = result.data.data;
                    const role = user.roles.map(item => item.role);
                  
                    let status = false;
                    if (user.status === 1) {
                        status = true;
                    }
                    this.setState({
                        name: user.name,
                        lastName: user.lastName,
                        email: user.email,
                        phone: user.phone,
                        profile: user.Profile.profile,
                        role: role,
                        status: status
                    });
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    handleChange = e => {
        if (e.target.id === 'status') {
            this.setState({ [e.target.id]: e.target.checked });
        }

        const errors = this.state.errors;

        switch (e.target.name) {
            case 'name':
                delete errors.name;
                break;
            case 'email':
                delete errors.email;
                break;

            case 'phone':
                delete errors.phone;
                break;

            case 'pass':
                delete errors.password;
                break;

            case 'passConfirm':
                delete errors.passwordConfirm;
                break;

            case 'profile':
                delete errors.idProfile;
                break;

            case 'role':
                delete errors.roles;
                break;
            default:
                break;
        }
        this.setState({ errors: errors, [e.target.name]: e.target.value });
    };

    handleClose = () => {
        this.props.handleClose();
        this.setterStatesAll();
    };

    handleSubmit = e => {
        let idProfile = '';
        const status = this.state.status ? 1 : 0;
        const profile = this.state.dataProfiles.filter(
            item => item.profile === this.state.profile
        );

        if (profile.length > 0) {
            idProfile = profile[0].id;
        }

        const rolesAdd = [];
        let itemIdRole = {};
        this.state.role.map(role => {
            const arrayItem = Object.values(this.state.dataRoles).filter(
                item => item.role === role
            );
            let idRoleAddArray = arrayItem.map(item => item.id);
            if (idRoleAddArray[0]) {
                itemIdRole = { idRole: idRoleAddArray[0], status: 1 };
                rolesAdd.push(itemIdRole);
            }
        });
        const dataAdd = {
            name: this.state.name.toString(),
            password: this.state.pass.toString(),
            lastName: this.state.lastName.toString(),
            email: this.state.email.toString(),
            phone: this.state.phone.toString(),
            idProfile: idProfile.toString(),
            passwordConfirm: this.state.passConfirm.toString(),
            roles: rolesAdd,
            status: status.toString(),
            idUser: this.props.idUser.toString()
        };
        console.log('OUTPUT: handleSubmit -> dataAdd', dataAdd);

        if (!dataAdd.idUser) {
            http
                .post('/api/users', dataAdd)
                .then(result => {
                    this.handleClose();
                    this.props.backAfterInsert(CRUDADD, result.data.message);
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        } else {
            http
                .put(`/api/users/${dataAdd.idUser}`, dataAdd)
                .then(result => {
                    this.handleClose();
                    this.props.backAfterInsert(CRUDUPDATE, result.data.message);
                })
                .catch(err => {
                    switch(err.response.status) {
                        case 400:
                            this.setState({ errors: err.response.data })
                            break;
                        case 404:
                            this.setState({ errors: err.response.data })
                            break;
                        case 500:
                            this.setState({ errors: err.response.data })
                            break;
                        default:
                            this.setState({ errors: {} })
                            break;
                    }
                });
        }
    };

    setterStatesAll = () => {
        this.setState({
            status: true,
            errors: {},
            name: '',
            lastName: '',
            email: '',
            phone: '',
            pass: '',
            passConfirm: '',
            dataProfiles: [],
            idProfile: '',
            profile: '',
            dataRoles: [],
            roles: [],
            role: [],
            idUser: ''
        });
    };

    render() {
        const { classes, idUser, open, title } = this.props;
        const {
            errors,
            name,
            lastName,
            email,
            phone,
            pass,
            passConfirm,
            dataProfiles,
            profile,
            roles,
            role,
            status
        } = this.state;
        return (
            <Dialog
                fullWidth
                maxWidth="sm"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />{' '}
                    {idUser ? CRUDUPDATE : CRUDADD} {title}
                </DialogTitle>
                <DialogContent>
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                name="name"
                                value={name}
                                label={NAME}
                                error={errors.name ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.name}
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                margin="dense"
                                id="lastName"
                                name="lastName"
                                value={lastName}
                                label={APELLIDO}
                                error={errors.lastName ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                            />
                            <Typography color="error" variant="caption">
                                {errors.lastName}
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                margin="dense"
                                id="email"
                                name="email"
                                value={email}
                                label={EMAIL}
                                error={errors.email ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.email}
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                margin="dense"
                                id="phone"
                                name="phone"
                                value={phone}
                                label={PHONE}
                                error={errors.phone ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                            />
                            <Typography color="error" variant="caption">
                                {errors.phone}
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                margin="dense"
                                id="pass"
                                name="pass"
                                value={pass}
                                label={PASS}
                                disabled={idUser ? true : false}
                                error={errors.password ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                                type="password"
                            />
                            <Typography color="error" variant="caption">
                                {errors.password}
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                margin="dense"
                                id="passConfirm"
                                name="passConfirm"
                                value={passConfirm}
                                label={PASSCONFIRM}
                                disabled={idUser ? true : false}
                                error={errors.passwordConfirm ? true : false}
                                fullWidth
                                onChange={this.handleChange}
                                required
                                type="password"
                            />
                            <Typography color="error" variant="caption">
                                {errors.passwordConfirm}
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <FormControl
                                error={errors.idProfile ? true : false}
                                fullWidth
                                required
                                className={classes.formControl}
                            >
                                <InputLabel
                                    required
                                    error={errors.idProfile ? true : false}
                                    htmlFor="profile"
                                >
                                    {PROFILE}
                                </InputLabel>
                                <Select
                                    error={errors.idProfile ? true : false}
                                    value={profile}
                                    onChange={this.handleChange}
                                    name="profile"
                                    id="profile"
                                    inputProps={{
                                        id: 'profile'
                                    }}
                                    className={classes.selectEmpty}
                                >
                                    <MenuItem value="">
                                        <em>Ninguno</em>
                                    </MenuItem>
                                    {dataProfiles.map(profile => (
                                        <MenuItem key={profile.id} value={profile.profile}>
                                            {profile.profile}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <Typography color="error" variant="caption">
                                {errors.idProfile}
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <FormControl disabled={idUser ? true : false} fullWidth className={classes.formControl}>
                                <InputLabel
                                    required
                                    error={errors.roles ? true : false}
                                    htmlFor="role"
                                >
                                    {ROLES}
                                </InputLabel>
                                <Select
                                    disabled={idUser ? true : false}
                                    multiple
                                    id="role"
                                    name="role"
                                    required
                                    error={errors.roles ? true : false}
                                    value={role}
                                    onChange={this.handleChange}
                                    input={<Input id="role" name="role" />}
                                    renderValue={selected => (
                                        <div className={classes.chips}>
                                            {selected.map(value => (
                                                <Chip
                                                    key={value}
                                                    label={value}
                                                    className={classes.chip}
                                                />
                                            ))}
                                        </div>
                                    )}
                                    MenuProps={MenuProps}
                                >
                                    {roles.map(name => (
                                        <MenuItem
                                            key={name}
                                            value={name}
                                            style={getStyles(name, this)}
                                        >
                                            {name}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <Typography color="error" variant="caption">
                                {errors.roles}
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <FormControlLabel
                                control={
                                    <Switch
                                        required
                                        id="status"
                                        checked={this.state.status}
                                        onChange={this.handleChange}
                                        value="status"
                                    />
                                }
                                label="Estado"
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                {errors.message ? (
                    <Typography align="center" color="error" variant="caption">
                        {errors.message}
                    </Typography>
                ) : null}
                <DialogActions>
                    <FormHelperText>{CAMPOSOBLIGATORIOS}</FormHelperText>
                    <Button onClick={this.handleSubmit} color="secondary">
                        {BUTTONADD}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogAdd.propTypes = {
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    backAfterInsert: PropTypes.func.isRequired
};

export default withStyles(styles, { withTheme: true })(DialogAdd);
