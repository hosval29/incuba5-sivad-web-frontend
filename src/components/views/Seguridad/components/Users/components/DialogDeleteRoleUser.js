//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material-IU
import {
    Dialog,
    Button,
    IconButton,
    Typography,
    withStyles
} from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Material-IU Icon
import CloseIcon from '@material-ui/icons/Close';

//Utils
import {
    BUTTONACEPT,
    CRUDADD,
    CRUDUPDATE,
    CRUDDELETE,
    CRUDACTIVAR,
    MESSAGEDELETE,
    MESSAGEACTIVAR,
    MESSAGEDELETEROLE,
    MESSAGEOPTIONINVALID,
    CRUDALERTNOACTIVE
} from '../../../../../../utils/constants';
import http from '../../../../../../store/actions/http';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    }
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography color="secondary" variant="h6">
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit
    }
}))(MuiDialogActions);

class DialogDeleteRoleUser extends Component {

    handleSubmit = (idUser, idRole, status) => (e) => {

        e.preventDefault()

        const dataDelete = { status: status }

        http.delete(`/api/users/userrole/${idUser}/${idRole}`, { data: dataDelete })
            .then((result) => {

                this.handleClose()
                if (status === 0) {
                    this.props.backAfterInsert(CRUDDELETE, result.data)
                } else {
                    this.props.backAfterInsert(CRUDUPDATE, result.data)
                }

            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    handleClose = () => {
        this.props.handleClose()
    }

    render() {
        const { idUser, idRole, open, title, status } = this.props;
        return (
            <Dialog
                id="dialogEdit"
                fullWidth
                maxWidth="xs"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="alert-dialog-slide-title" onClose={this.handleClose}>
                    {status === 0 ? CRUDDELETE : status === 1 ? CRUDACTIVAR : CRUDALERTNOACTIVE} {title}
                </DialogTitle>
                <DialogContent>
                    <Typography component="div">
                        {status === 0 ? MESSAGEDELETE : status === 1 ? MESSAGEACTIVAR + title : MESSAGEOPTIONINVALID}
                    </Typography>
                    <Typography component="div">¡Aceptar para continuar!.</Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleSubmit(idUser, idRole, status)} color="secondary">
                        {BUTTONACEPT}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogDeleteRoleUser.propTypes = {
    classes: PropTypes.object.isRequired,
    idUser: PropTypes.number.isRequired,
    idRole: PropTypes.number.isRequired,
    status: PropTypes.number.isRequired,
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    handleClose: PropTypes.func.isRequired,
    backAfterInsert: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogDeleteRoleUser);
