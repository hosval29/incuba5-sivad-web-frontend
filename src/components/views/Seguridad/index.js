//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

//Material UI
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Tabs, Tab } from '@material-ui/core';

//Components
import UserManagement from './components/Users/UserManagement';
import Roles from './components/Roles/Roles'
import Profiles from './components/Profiles/Profiles'
import Modules from './components/Modulos/Modules'
import HistorySession from './components/HistoryLog/HistoryLog'
//Utils
import { setToolbar } from '../../../store/actions/authActions'
import { SEGURIDAD, ROLES, PROFILES, MODULES, HISTORYLOG } from '../../../utils/constants'

console.log("OUTPUT: HistorySession", HistorySession)
console.log("OUTPUT: Modules", Modules)

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    appBar: {
        backgroundColor: theme.palette.background.paper,
        color: '#e0e0e0',
        position: 'fixed'
    }
});

function TabContainer(props) {
    console.log("OUTPUT: TabContainer -> props", props)

    switch (props.children) {
        case ROLES:
            return <Roles />;
            break;
        case PROFILES:
            return <Profiles />;
            break;
        case MODULES:
            return <Modules />;
            break;
        case HISTORYLOG:
            return <HistorySession />;
            break;
        default:
            return <UserManagement />;
            break;
    }
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired
};

class Seguridad extends Component {
    state = {
        value: 0
    };
    handleChange = (event, value) => {
        this.setState({ value });
    };

    componentWillMount = () => {
        const toolbar = {
            title: SEGURIDAD,
            icon: ''
        }
        this.props.setToolbar(toolbar)
    }
    render() {
        const { classes, auth, match: { url } } = this.props;
        const { value } = this.state;

        const { user } = auth;
        const modules = user.profiles.modules;
        const menuSeguridad = modules.find(itemMenu => itemMenu.module === 'Seguridad');
        const itemsMenuSeguridad = menuSeguridad.items;

        return (
            <div className={classes.root}>
                <AppBar className={classes.appBar} position="static">
                    <Tabs
                        variant="scrollable"
                        scrollButtons="auto"
                        textColor="secondary"
                        value={value}
                        onChange={this.handleChange}
                    >
                        {itemsMenuSeguridad.sort((a,b) => a.id-b.id).map((itemMenuSuridad, index) => (
                            <Tab
                                key={itemMenuSuridad.id}
                                value={index}
                                label={itemMenuSuridad.itemModule}
                            />
                        ))}
                    </Tabs>
                </AppBar>
                {itemsMenuSeguridad.map(
                    (itemMenuSuridad, index) =>
                        value === index && (
                            <TabContainer key={itemMenuSuridad.id}>
                                {itemMenuSuridad.itemModule}
                            </TabContainer>
                        )
                )}
            </div>
        );
    }
}

Seguridad.propTypes = {
    classes: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    setToolbar: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default compose(
    withStyles(styles),
    connect(mapStateToProps, { setToolbar }),
    withRouter
)(Seguridad);
