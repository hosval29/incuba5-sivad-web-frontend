import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ALERTREPORTS, DETAILLAERT } from '../../../utils/constants'
import { setToolbar } from '../../../store/actions/authActions'
import { compose } from 'recompose';
// import { Route, Link } from "react-router-dom";
// import Chart from "react-apexcharts";
import MUIDataTable from 'mui-datatables';

import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import { Grid, IconButton } from '@material-ui/core';

// import { CarTractionControl, CarOff, ClipboardAlertOutline, Alert } from 'mdi-material-ui'
import http from '../../../utils/http';
//Icons
import { FileEyeOutline } from 'mdi-material-ui';
import DialogDetailAlert from './components/DialogDetailAlert';


const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingTop: theme.spacing.unit * 4
    },
    divider: {
        marginBottom: theme.spacing.unit * 2
    },

    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    cardContent: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    cardSubTitle: {
        color: '#9e9e9e'
    },
    colorFirst: {
        color: '#24d2b5'
    },
    colorSecond: {
        color: '#ff9800'
    },
    colorThree: {
        color: '#6772e5'
    },
    colorFour: {
        color: '#ff5c6c'
    },
    paperCharCircle: {
        position: 'relative',
        display: 'flex',
        flexFlow: 'column',
        alignItems: 'center',
        justifyItems: 'center',
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    tableCell: {
        backgroundColor: '#68debd'
    },
    tableCellTd: {
        backgroundColor: '#eeeeee',
        width: '50%'
    },
    tableCellTh: {
        width: '50%'
    },
});

class AlertReports extends Component {

    state = {
        openDialogDetailAlert: false,

        dataNotificationsInspections: [],
        dataNotificationsVehicles: [],

        data: [],
        dataDetailAlert: {},
        errors: {}
    }

    componentWillMount = () => {
        const toolbar = {
            title: ALERTREPORTS,
            icon: ''
        }
        this.props.setToolbar(toolbar)
        this.getData()
        this.getDataAlertsInspectionItemBad()
    }


    componentWillUnmount() {
        this._isMounted = false;
    }

    getData = () => {
        this._isMounted = true;
        http
            .get('/api/notifications')
            .then(result => {

                if (this._isMounted) {
                    if (result.data.data) this.setState({ dataNotificationsVehicles: result.data.data })
                }
            })
            .catch(err => {
            console.log("OUTPUT: AlertReports -> getData -> err", err)
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    getDataAlertsInspectionItemBad = () => {
        this._isMounted = true;
        http
            .get('/api/alertsinspectionitembad')
            .then(result => {

                if (this._isMounted) {
                    if (result.data.data) {
                        this.setState({ dataNotificationsInspections: result.data.data })
                    }
                }
            })
            .catch(err => {
            console.log("OUTPUT: AlertReports -> getDataAlertsInspectionItemBad -> err", err)
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    handleClose = () => {
        this.setState({ openDialogDetailAlert: !this.state.openDialogDetailAlert })
    }

    render() {
        const { classes } = this.props;
        const { dataNotificationsInspections, dataNotificationsVehicles, dataDetailAlert, openDialogDetailAlert } = this.state

        const columns = [
            { name: 'createdAt', label: 'Fecha Notificación' },
            {
                name: 'data', label: 'Placa Vehículo', options: {
                    customBodyRender: (value) => value.plateVehicle
                }
            },
            {
                name: 'data', label: 'Fecha Expiración', options: {
                    customBodyRender: (value) => value.dateExpiration
                }
            },
            {
                name: 'data', label: 'Alerta', options: {
                    customBodyRender: (value) => value.typeAlert
                }
            },
            {
                name: 'id',
                label: 'Detalle Alerta',
                options: {
                    filter: true,
                    sort: false,
                    empty: true,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <IconButton
                                onClick={() => {
                                    // console.log(value)
                                    let dataRowAlert = {}
                                    const dataArrayAlert = this.state.dataNotificationsVehicles.filter(item => item.id === value)
                                  
                                    dataArrayAlert.map(item => {
                                        dataRowAlert = item.data.dataDetail

                                        return this.setState({ dataDetailAlert: dataRowAlert, openDialogDetailAlert: !this.state.openDialogDetailAlert })
                                    })
                                }
                                }
                            >
                                <FileEyeOutline />
                            </IconButton>
                        );
                    }
                }
            },
        ];

        const options = {
            filterType: "dropdown",
            selectableRows: false,
            isRowSelectable: false,
            rowsSelected: false,
            responsive: 'scroll',
            print: false,
            download: false,
            textLabels: {
                body: {
                    noMatch: "Lo sentimos, no se encontraron registros coincidentes",
                    toolTip: "Ordenar",
                },
                toolbar: {
                    search: 'Buscar...',
                    downloadCsv: 'Descargar CSV',
                    print: 'Imprimir',
                    viewColumns: 'Columnas Visibles',
                    filterTable: 'Filtro'
                },
                pagination: {
                    next: 'Página Siguiente',
                    previous: 'Página Anterior',
                    rowsPerPage: 'Filas por paginas:',
                    displayRows: 'de'
                },
                filter: {
                    all: 'Todos',
                    title: 'Filtrar',
                    reset: 'Limpiar Filtro'
                },
                viewColumns: {
                    title: "Columas Visibles"
                },
            }
        };


        const columnsTableAlertsInspection = [
            { name: 'dateRegister', label: 'Fecha Alerta' },
            {
                name: 'data', label: 'Placa Vehículo', options: {
                    customBodyRender: (value) => value.plateVehicle
                }
            },
            {
                name: 'data', label: 'Usuario', options: {
                    customBodyRender: (value) => value.nameUser
                }
            },
            {
                name: 'data', label: 'Aspecto', options: {
                    customBodyRender: (value) => value.nameFragment
                }
            },
            {
                name: 'data', label: 'Item', options: {
                    customBodyRender: (value) => value.nameToggle
                }
            },
        ]

        return (
            <div className={classes.root}>

                <Grid container spacing={24}>

                    <Grid item xs={12} sm={12}>
                        <MUIDataTable
                            title="Notificaciones Vehículos"
                            data={dataNotificationsVehicles}
                            columns={columns}
                            options={options} />
                        {openDialogDetailAlert ? (
                            <DialogDetailAlert
                                title={DETAILLAERT}
                                data={dataDetailAlert}
                                open={openDialogDetailAlert}
                                handleClose={this.handleClose}
                            />
                        ) : null}
                    </Grid>

                    <Grid item xs={12} sm={12}>

                        <MUIDataTable
                            title="Notificaciones Inspección"
                            data={dataNotificationsInspections}
                            columns={columnsTableAlertsInspection}
                            options={options} />

                    </Grid>

                </Grid>
            </div>
        )
    }
}

AlertReports.propTypes = {
    classes: PropTypes.object.isRequired,
    setToolbar: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    toolbar: state.toolbar
});

export default compose(
    withStyles(styles),
    connect(mapStateToProps, { setToolbar }),
    withRouter
)(AlertReports);
