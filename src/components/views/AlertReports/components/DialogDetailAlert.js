//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Material-IU
import {
    Dialog,
    Button,
    IconButton,
    Typography,
    withStyles,
    Paper,
    Table,
    TableBody,
    TableRow,
    TableCell
} from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

//Material-IU Icon
import CloseIcon from '@material-ui/icons/Close';
import { SettingsOutline } from 'mdi-material-ui';


import { BUTTONOK } from '../../../../utils/constants';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    },
    iconTitleDialog: {
        marginRight: 10
    },
    tableCell: {
        backgroundColor: '#68debd'
    },
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500]
    },
    titleDialog: {
        display: 'flex',
        alignItems: 'center'
    }
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography
                className={classes.titleDialog}
                color="textPrimary"
                variant="h6"
            >
                {children}
            </Typography>
            {onClose ? (
                <IconButton
                    aria-label="Close"
                    className={classes.closeButton}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2
    }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit
    }
}))(MuiDialogActions);

class DialogDetailAlert extends Component {

    handleClose = () => {
        this.props.handleClose()
    }

    render() {
        const { classes, open, title, data } = this.props;

        return (
            <Dialog
                id="dialogEdit"
                fullWidth
                maxWidth="sm"
                open={open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogTitle id="form-dialog-title" onClose={this.handleClose}>
                    <SettingsOutline className={classes.iconTitleDialog} />
                    {title}
                </DialogTitle>
                <DialogContent>
                    <Paper className={classes.paper}>
                        <Table className={classes.table}>
                            <TableBody>
                                {Object.entries(data).map(([key, value]) =>
                                    Object.entries(value).map(([subKey, subValue]) =>
                                        <TableRow key={key}>
                                            <TableCell className={classes.tableCell}>
                                                {subValue.inputTitle}
                                            </TableCell>
                                            <TableCell component="th" scope="row">
                                                {subValue.value === 'true' ? 'Programada' : subValue.value}
                                            </TableCell>
                                        </TableRow>)
                                )}
                            </TableBody>
                        </Table>
                    </Paper>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} color="secondary">
                        {BUTTONOK}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogDetailAlert.propTypes = {
    classes: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    handleClose: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogDetailAlert);
