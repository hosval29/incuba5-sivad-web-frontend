import React, { Component, Fragment } from 'react'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";

import { connect } from "react-redux";
import compose from 'recompose/compose'
import { loginUser } from '../../store/actions/authActions'

import classnames from "classnames";

import classNames from 'classnames';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
// import TextField from '@material-ui/core/TextField';
// import AccountCircle from '@material-ui/icons/AccountCircle';
import Hidden from '@material-ui/core/Hidden';
import Fab from '@material-ui/core/Fab';
import Web from '@material-ui/icons/Web';
import Email from '@material-ui/icons/Email';
import Password from '@material-ui/icons/LockOpen';
// import color from '@material-ui/core/colors/yellow';

const styles = theme => ({

    '@global': {
        'html, body, #app': {
            height: '100%'
        }
    },

    root: {
        width: '100%',
        height: '100vh',
    },

    signin: {
        height: '70vh',
        [theme.breakpoints.up('md')]: {
            height: '100vh',
        }

    },

    signinPaper: {
        width: '90%',
        [theme.breakpoints.up('sm')]: {
            width: '80%',
            margin: 'auto',

        },

        [theme.breakpoints.up('md')]: {
            height: '70%',
            margin: 'auto',
            justifyContent: 'center',
            display: 'flex',
            flexDirection: 'column'
        },

    },
    signinInfo: {
        width: '100%',
        marginBottom: theme.spacing.unit + 2,
        marginTop: theme.spacing.unit + 2,
        padding: theme.spacing.unit
    },

    signinForm: {
        width: '100%', // Fix IE 11 issue.
        padding: theme.spacing.unit,
        display: 'flex',
        alignItems: 'center',
        justifyItems: 'center',
        flexDirection: 'column'
    },

    signinInput: {
        color: '#000000'
    },

    signinCaption: {
        color: '#757575',
        marginTop: theme.spacing.unit + 2,
        marginBottom: theme.spacing.unit + 2,
    },

    signinButton: {
        width: '10rem',
        borderRadius: '10rem',
        border: `1px solid ${theme.palette.secondary}`,
        color: theme.palette.background.paper,
        marginBottom: theme.spacing.unit + 2,
    },

    landing: {
        width: '100%',
        height: '30vh',
        backgroundColor: theme.palette.secondary.main,
        display: 'flex',
        alignItems: 'center',
        justifyItems: 'center',
        flexDirection: 'column',
        [theme.breakpoints.up('sm')]: {
            justifyContent: 'space-around'
        },
        [theme.breakpoints.up('md')]: {
            height: '100vh',
        }
    },

    landingPaper: {
        width: '90%',
        backgroundColor: 'transparent',
        padding: theme.spacing.unit + 2
    },

    landingH3: {
        fontSize: '1rem',
        fontWeight: '500',
        color: theme.palette.background.paper,
        [theme.breakpoints.up('md')]: {
            fontSize: '1.8rem',
            marginBottom: '1rem'
        }

    },

    landingH5: {
        fontSize: '1rem',
        fontWeight: '500',
        textTransform: 'uppercase',
        color: '#efb810',
        [theme.breakpoints.up('md')]: {
            fontSize: '1.3rem'
        }
    },

    landingSubtitle2: {
        fontSize: '.5rem',
        color: theme.palette.background.paper,
        [theme.breakpoints.up('sm')]: {
            fontSize: '0.8rem'
        }
    },

    landingFab: {
        fontSize: '8px !important',
        color: '#efb810',
        border: '1px solid',
        background: 'transparent',
        height: '34px',
        marginTop: '10px'
    },
});

class SignIn extends Component {

    state = {
        email: '',
        password: '',
        errors: {},
        token: ''
    };

    componentDidMount() {
        if (this.props.auth.isAuthenticated) {
            this.props.history.push("/layout");
        }
        
        //this.setState({ token : token})
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.props.history.push("/layout")
        }
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    handleChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };

    handleSubmit = e => {
   
        e.preventDefault()
        const dataUser = {
            //tokenFirebase: this.props.tokenFirebase,
            email: this.state.email,
            password: this.state.password,
            viaAcceso: 'Web'
        }
        //console.log("OUTPUT: SignIn -> dataUser", dataUser)
        this.props.loginUser(dataUser); // since we handle the redirect within our component, we don't need to pass in this.props.history as a parameter

    };

    render() {
        const { classes } = this.props;
     
        const { errors, email, password } = this.state

        return (
            <Fragment>
                <CssBaseline />
                <Grid container className={classes.root}>

                    <Grid container item xs={12} md={8} alignItems="center" justify="center" className={classes.signin}>

                        <Paper className={classes.signinPaper}>

                            <div className={classes.signinInfo}>
                                <Hidden smUp>
                                    <Typography variant="h5" align="center" color="secondary" gutterBottom>
                                        Inicio de Sesión en SIVAD
                                    </Typography>
                                </Hidden>
                                <Hidden xsDown>
                                    <Typography variant="h4" align="center" color="secondary" gutterBottom>
                                        Inicio de Sesión en SIVAD
                                    </Typography>
                                </Hidden>

                                <Typography align="center" variant="caption" className={classes.signinCaption} gutterBottom>
                                    Ingresa tu email y contraseña
                                </Typography>
                            </div>
                            <form noValidate onSubmit={this.handleSubmit} className={classes.signinForm}>

                                <div>
                                    <FormControl margin="dense" >
                                        <InputLabel htmlFor="email">Email</InputLabel>
                                        <Input
                                            fullWidth
                                            id="email"
                                            type="email"
                                            value={email}
                                            onChange={this.handleChange}
                                            error={errors.email ? true : false}
                                            className={classnames("", {
                                                invalid: errors.email
                                            }, classes.signinInput)}
                                            startAdornment={
                                                <InputAdornment position="start">
                                                    <Email color="secondary" />
                                                </InputAdornment>
                                            }

                                        />
                                        <Typography color="error" variant="caption">
                                            {errors.email}
                                        </Typography>
                                    </FormControl>
                                </div>
                                <div>
                                    <FormControl margin="dense" >
                                        <InputLabel htmlFor="password">Contraseña</InputLabel>
                                        <Input
                                            id="password"
                                            type="password"
                                            fullWidth
                                            onChange={this.handleChange}
                                            value={password}
                                            error={errors.password ? true : false || errors.error}
                                            className={classnames("", {
                                                invalid: errors.password || errors.passwordincorrect
                                            }, classes.signinInput)}
                                            startAdornment={
                                                <InputAdornment position="start">
                                                    <Password color="secondary" />
                                                </InputAdornment>
                                            }
                                        />
                                        <Typography color="error">
                                            {errors.password}
                                            {errors.message}
                                        </Typography>
                                    </FormControl>
                                </div>

                                <div>
                                    <Typography variant="caption" align="center" gutterBottom className={classes.signinCaption}>
                                        ¿Olvidaste tu contraseña?
                                    </Typography>
                                </div>

                                <div>
                                    <Button type="submit" variant="contained" color="secondary" className={classes.signinButton}>
                                        Iniciar Sesión
                                    </Button>
                                </div>

                            </form>
                        </Paper>
                    </Grid>

                    <Grid container item xs={12} md={4} justify="center" alignItems="center" className={classes.landing}>
                        <Hidden smUp>
                            <Paper className={classNames(classes.landingPaper)}>

                                <div>
                                    <Typography variant="h3" align="center" gutterBottom className={classes.landingH3}>
                                        Bienvenido..!
                                    </Typography>
                                    <Typography variant="h5" align="center" gutterBottom className={classes.landingH5}>
                                        Imcodel
                                    </Typography>
                                </div>
                                <div>
                                    <Typography variant="subtitle2" align="justify" gutterBottom className={classes.landingSubtitle2}>
                                        Es un gusto tenerte con nosotros. SIVAD WEB, es una plataforma
                                        donde podras visualizar todo la información sobre las Inspecciones Preoperacionales
                                        de tus vehículos
                                    </Typography>
                                </div>

                            </Paper>


                            <Fab variant="extended" aria-label="Delete" className={classes.landingFab}>
                                <Web />
                                SIVAD WEB V-1.0.0
                            </Fab>

                        </Hidden>

                        <Hidden only='xs'>
                            <Paper className={classNames(classes.landingPaper)}>
                                <Grid container spacing={24}>
                                    <Grid item sm={5} md={12}>
                                        <Typography variant="h3" align="center" gutterBottom className={classes.landingH3}>
                                            Bienvenido..!
                                    </Typography>
                                        <Typography variant="h5" align="center" gutterBottom className={classes.landingH5} >
                                            Imcodel
                                    </Typography>
                                    </Grid>
                                    <Grid item sm={7} md={12}>
                                        <Typography variant="subtitle2" align="justify" gutterBottom className={classes.landingSubtitle2}>
                                            Es un gusto tenerte con nosotros. SIVAD WEB, es una plataforma
                                            donde podras visualizar todo la información sobre las Inspecciones Preoperacionales
                                            de tus vehículos
                                    </Typography>
                                    </Grid>
                                </Grid>
                            </Paper>
                            <Fab variant="extended" aria-label="Delete" className={classes.landingFab}>
                                <Web />
                                SIVAD WEB V-1.0.0
                        </Fab>

                        </Hidden>
                    </Grid>
                </Grid>
            </Fragment>
        );
    }
}

SignIn.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default compose(
    withStyles(styles),
    connect(mapStateToProps, { loginUser }),withRouter)(SignIn);

