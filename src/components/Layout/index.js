import React, { Component } from 'react';
import { messaging } from '../../init-fcm';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';


import NotificationsIcon from '@material-ui/icons/Notifications';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import { Avatar, InputBase, Badge, Menu, MenuItem, ListSubheader, Collapse, MenuList, Paper, Popper, Grow, ClickAwayListener } from '@material-ui/core';
import { fade } from '@material-ui/core/styles/colorManipulator';

import { Route, Link } from "react-router-dom";

import Logo from '../../assets/images/sivad_logo.svg'
import Seguridad from '../views/Seguridad'
import Operacional from '../views/Operacional'
import DashBoard from '../views/Dashboard'
import Sistema from '../views/Sistema'
import AlertReports from '../views/AlertReports'

import Owners from '../views/GestionVehiculos/components/Owners/Owners'
import Vehicles from '../views/GestionVehiculos/components/Vehicles/Vehicles';
import Drivers from '../views/GestionVehiculos/components/Drivers/Drivers'
import CreateVehicle from '../views/GestionVehiculos/components/CreateVehicle/CreateVehicle'
import MileageUpdate from '../views/Reports/components/MileageUpdate/MileageUpdate'
import VehiclesInspections from '../views//Reports/components/Inspections/VehicleInspections'


import DashboardIcon from '@material-ui/icons/Dashboard';
import AssignmentIcon from '@material-ui/icons/Assignment';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import SecurityIcon from '@material-ui/icons/Security'
import OperationIcon from '@material-ui/icons/SettingsApplications'
import { TableSettings, CheckNetworkOutline, Logout, AccountOutline } from 'mdi-material-ui'
import StarBorder from '@material-ui/icons/StarBorder';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';


import { DASHBOARD, SEGURIDAD, GESTION_VEHICULOS, OPERACIONAL, OWNERS, DRIVERS, VEHICLES, SISTEMA, CREATEVEHICLE, REPORTES, ALERTREPORTS, VEHICLESINSPECTIONS, MILEAGEUPDATE, CRUDUPDATE } from '../../utils/constants'
import { withRouter } from 'react-router'
import { compose } from 'recompose';
import { connect } from 'react-redux'

import { logoutUser, setToolbar } from '../../store/actions/authActions'

//Components
import Alertas from './components/Alertas'
import Account from '../views/Seguridad/components/Users/components/Account'
import http from '../../utils/http';
import SnackbarInfo from '../../utils/snackbars/components/Snackbar/SnackbarInfo';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
        height: '100vh'
    },
    //Style Sidebar
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar
    },
    margin: {
        margin: theme.spacing.unit
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        height: '100vh',
        width: drawerWidth,
        backgroundColor: theme.palette.primary.dark,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9
        }
    },

    drawerIcon: {
        color: theme.palette.secondary.main
    },

    drawerSubheader: {
        color: theme.palette.secondary.main
    },

    listItemText: {
        color: 'white'
    },

    listItemTextSubList: {
        color: '#e0e0e0',
        fontSize: '0.9rem'
    },

    listItemIcon: {
        color: 'white'
    },

    nested: {
        paddingLeft: theme.spacing.unit * 3,
    },

    logo: {
        width: '70%'
    },

    iconButtonChev: {
        color: 'white'
    },

    //Style Header
    toolbar: {
        paddingRight: 24 // keep right padding when drawer closed
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },

    badge: {
        '&:hover': { cursor: 'pointer' }
    },

    menuButton: {
        marginLeft: 12,
        marginRight: 36,
        color: 'white'
    },
    menuButtonHidden: {
        display: 'none',
        color: 'white'
    },
    title: {
        flexGrow: 1,
        marginLeft: '0.5rem'
    },
    h5: {
        marginBottom: theme.spacing.unit * 2
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25)
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing.unit,
            width: 'auto'
        }
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%'
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: 120,
            '&:focus': {
                width: 200
            }
        }
    },
    rootButtonAppBar: {
        marginLeft: '1rem'
    },

    menuItem: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& $primary, & $icon': {
                color: theme.palette.common.white,
            },
        },
    },
    primary: {},
    icon: {},

    avatar: {
        backgroundColor: theme.palette.secondary.main
    },
    iconButtonAvatar: {
        padding: '0px',
        marginLeft: '1rem'
    },

    //Style Main
    main: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
        paddingTop: '64px'
    },
    pulse: {
        borderRadius: '50%',
        boxShadow: '0 0 0 rgba(232, 76, 61, 0.7)',
        animation: 'pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1)',
    },
    '@keyframes pulse': {
        'to': { boxShadow: '0 0 0 20px rgba(232, 76, 61, 0)' }
    }
});

let token = ''

class Layout extends Component {
    state = {
        textSearch: '',
        anchorEl: null,
        errors: [],
        openTextSearch: false,
        open: true,
        openMenu: true,
        openMenuGestionVehicle: true,
        menuProfile: null,
        openCollapese: true,
        openMenuAlerts: null,
        openDialogAccount: false,
        isNotifications: false,
        tokenFirebase: '',
        nackbarOpen: false,
        snackbarMessage: '',
        snackbarVariant: '',
    };

    componentWillMount = () => {
        this.getDataNotifications()

        /*messaging.requestPermission()
            .then(async function () {
                await messaging.getToken().then((result) => {
                    //console.log("OUTPUT: Layout -> componentWillMount -> result", result)
                    token = result
                    //console.log("OUTPUT: Layout -> componentWillMount -> token", token)
                }).catch((err) => {

                })
            })
            .catch(function (err) {
                console.log("Unable to get permission to notify.=======", err)
            })

        //console.log("OUTPUT: Layout -> componentWillMount -> token", token)
        this.setState({ tokenFirebase: token })
        navigator.serviceWorker.addEventListener("message", (message) =>
            console.log('message', message)
        );*/

        messaging.onMessage(function (payload) {
            //console.log('=====', payload)
            const notificationTitle = payload.data.title;
            const notificationOptions = {
                body: payload.data.body,
                icon: payload.data.icon,
            };

            if (!("Notification" in window)) {
                console.log("This browser does not support system notifications");
            }
            // Let's check whether notification permissions have already been granted
            else if (Notification.permission === "granted") {
                // If it's okay let's create a notification
                var notification = new Notification(notificationTitle, notificationOptions);
                notification.onclick = function (event) {
                    event.preventDefault(); // prevent the browser from focusing the Notification's tab
                    window.open(payload.data.link, '_blank');
                    notification.close();
                }
            }
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    async componentDidMount() {
        messaging.requestPermission()
            .then(async function () {
                const token = await messaging.getToken();
                console.log("OUTPUT: Layout -> componentDidMount -> token", token)
                localStorage.setItem('token', token)
            })
            .catch(function (err) {
                console.log("Unable to get permission to notify.", err);
            });
        navigator.serviceWorker.addEventListener("message", (message) => console.log(message));

        this.putTokenFirebaseUser()
    }

    getDataNotifications = () => {
        this._isMounted = true;
        http
            .get('/api/notifications')
            .then(result => {

                if (this._isMounted) {
                    const data = result.data.data
                    if (data) this.setState({ isNotifications: !this.state.isNotifications })
                }
            })
            .catch(err => {
                switch (err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    };

    putTokenFirebaseUser = () => {

        const idUser = this.props.auth.user.id
        const dataUpdate = {
            tokenFirebase: localStorage.getItem('token')
        }
        console.log("OUTPUT: Layout -> putTokenFirebaseUser -> dataUpdate", dataUpdate)
        http
            .put(`/api/notifications/${idUser}`, dataUpdate)
            .then(result => {
                console.log("OUTPUT: Layout -> putTokenFirebaseUser -> result", result)
            })
            .catch(err => {
                switch (err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    }

    handleDrawerOpen = () => {
        this.setState({ open: true });
    };

    handleDrawerClose = () => {
        this.setState({ open: false });
    };

    handleMenuOpenProfile = e => {

        this.setState({ menuProfile: e.currentTarget });
    }

    handleLoguot = userData => {

        //e.preventDefault()
        this.setState({ menuProfile: null });
        this.props.logoutUser(userData);
    }

    handleClick = () => {
        this.setState(state => ({ openCollapese: !state.openCollapese }));
    };

    filtraPorModuleUp = (ob) => {
        if ('module' in ob && ob.module === DASHBOARD || ob.module === REPORTES || ob.module === GESTION_VEHICULOS) {
            return true
        } else {
            return false
        }
    }

    filtraPorModuleDown = (ob) => {
        if ('module' in ob && ob.module === OPERACIONAL || ob.module === SEGURIDAD || ob.module === SISTEMA) {
            return true
        } else {
            return false
        }
    }

    handleClickOpenMenuAlerts = (e) => {
        this.setState({ openMenuAlerts: e.currentTarget });
    }

    handleCloseMenuAlerts = () => {
        this.setState({ openMenuAlerts: null });
    }

    handleClickDialogAccount = () => {
        this.setState({ openDialogAccount: !this.state.openDialogAccount, menuProfile: null })
    }

    handleCloseDialogAccount = () => {
        this.setState({ openDialogAccount: !this.state.openDialogAccount })
    }

    handleCloseTextSearch = (event) => {
        this.setState({
            anchorEl: event.currentTarget,
            openTextSearch: false,
            textSearch: '',
        });
    }

    handleOnChangeTextSearch = event => {
        this.setState({
            anchorEl: event.currentTarget,
            textSearch: event.target.value,
            openTextSearch: true,
        });
    };

    backAfterInsert = (action, message) => {
        switch (action) {
            case CRUDUPDATE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;
            default:
                break
        }

        this.getDataNotifications();
    }

    handleCloseSnackbar = e => {
        this.setState({
            snackbarOpen: !this.state.snackbarOpen,
            snackbarMessage: '',
            snackbarVariant: '',
        });
    };

    handleClickExpandedMenu = module => {
        console.log("OUTPUT: Layout -> module", module)
        if (module === REPORTES) {
            this.setState({ openMenu: !this.state.openMenu })
        } else if (module === GESTION_VEHICULOS) {
            this.setState({ openMenuGestionVehicle: !this.state.openMenuGestionVehicle })
        }
    }

    backAfterInsert = (action, message) => {
        switch (action) {
            case CRUDUPDATE:
                this.setState({
                    snackbarOpen: !this.state.snackbarOpen,
                    snackbarMessage: message,
                    snackbarVariant: 'info'
                });
                break;
            default:
                break
        }

        this.getDataNotifications();
    }

    handleCloseSnackbar = e => {
        this.setState({
            snackbarOpen: !this.state.snackbarOpen,
            snackbarMessage: '',
            snackbarVariant: '',
        });
    };

    render() {
        const { classes, theme, auth, toolbar } = this.props;

        const { open, menuProfile, openMenuAlerts, openDialogAccount, isNotifications, tokenFirebase } = this.state
        //console.log("OUTPUT: Layout -> render -> tokenFirebase", tokenFirebase)
        const isMenuOpen = Boolean(menuProfile);
        const { user } = auth;
        const modules = user.profiles.modules

        //Saco la data para el deslogueo
        let dataSignUp = {}
        if (user.id) {
            Object.assign(dataSignUp, { idUser: user.id })
        }

        if (user.idUserSession) {
            Object.assign(dataSignUp, { idUserSession: user.idUserSession })
        }

        if (user.idHistoryLog) {
            Object.assign(dataSignUp, { idHistoryLog: user.idHistoryLog })
        }

        /**
         * Code Sidebar
         */
        const menuUp = modules.filter(this.filtraPorModuleUp).sort((a, b) => {
            if (a.module < b.module) return -1;
            else if (a.module > b.module) return 1;
            return 0;
        })

        const menuDown = modules.filter(this.filtraPorModuleDown)

        /**
         * Code Main
         */

        const routes = [];

        modules.map((itemMenu) => {

            if (itemMenu.module === SEGURIDAD) {
                const itemRoutePush = { id: itemMenu.id, icon: '', path: itemMenu.path, component: Seguridad }
                if (!routes.find(itemRoute => (
                    itemRoute.id === itemRoutePush.id
                ))) {
                    routes.push(itemRoutePush)
                }

            }

            if (itemMenu.module === OPERACIONAL) {
                const itemRoutePush = { id: itemMenu.id, icon: '', path: itemMenu.path, component: Operacional }
                if (!routes.find(itemRoute => (
                    itemRoute.id === itemRoutePush.id
                ))) {
                    routes.push(itemRoutePush)

                }
            }

            if (itemMenu.module === SISTEMA) {
                const itemRoutePush = { id: itemMenu.id, icon: '', path: itemMenu.path, component: Sistema }
                if (!routes.find(itemRoute => (
                    itemRoute.id === itemRoutePush.id
                ))) {
                    routes.push(itemRoutePush)

                }
            }

            if (itemMenu.module === DASHBOARD) {
                const itemRoutePush = { id: itemMenu.id, icon: '', exact: true, path: itemMenu.path, component: DashBoard }
                if (!routes.find(itemRoute => (
                    itemRoute.id === itemRoutePush.id
                ))) {
                    routes.push(itemRoutePush)

                }
            }

            if (itemMenu.module === GESTION_VEHICULOS) {

                const items = itemMenu.items


                if (!routes.find(itemRoute => (
                    //itemRoute.id === itemRoutePush.id
                    null
                ))) {
                    //routes.push(itemRoutePush)

                    items.map(item => {
                        if (item.itemModule === DRIVERS) {
                            let itemRoutePush = { id: item.id, path: 'configuracion/conductores', component: Drivers }
                            if (!routes.find(itemRoute => (itemRoute.id === itemRoutePush.id))) {
                                routes.push(itemRoutePush)
                            }
                        }

                        if (item.itemModule === VEHICLES) {
                            let itemRoutePush = { id: item.id, path: 'configuracion/vehiculos', component: Vehicles }
                            if (!routes.find(itemRoute => (itemRoute.id === itemRoutePush.id))) {
                                routes.push(itemRoutePush)
                            }
                        }

                        if (item.itemModule === OWNERS) {
                            let itemRoutePush = { id: item.id, path: 'configuracion/propietarios', component: Owners }
                            if (!routes.find(itemRoute => (itemRoute.id === itemRoutePush.id))) {
                                routes.push(itemRoutePush)
                            }
                        }

                        if (item.itemModule === CREATEVEHICLE) {
                            let itemRoutePush = { id: item.id, path: 'configuracion/crearvehiculo', component: CreateVehicle }
                            if (!routes.find(itemRoute => (itemRoute.id === itemRoutePush.id))) {
                                routes.push(itemRoutePush)
                            }
                        }
                    })



                }
            }

            if (itemMenu.module === REPORTES) {

                const items = itemMenu.items

                if (!routes.find(itemRoute => (
                    //itemRoute.id === itemRoutePush.id
                    null
                ))) {
                    //routes.push(itemRoutePush)

                    items.map(item => {
                        if (item.itemModule === VEHICLESINSPECTIONS) {
                            let itemRoutePush = { id: item.id, path: 'reportes/inspeccionesvehiculares', component: VehiclesInspections }
                            if (!routes.find(itemRoute => (itemRoute.id === itemRoutePush.id))) {
                                routes.push(itemRoutePush)
                            }
                        }

                        if (item.itemModule === MILEAGEUPDATE) {
                            let itemRoutePush = { id: item.id, path: 'reportes/actualizacionkilometraje', component: MileageUpdate }
                            if (!routes.find(itemRoute => (itemRoute.id === itemRoutePush.id))) {
                                routes.push(itemRoutePush)
                            }
                        }
                    })
                }
            }

            if (itemMenu.module === ALERTREPORTS) {
                const itemRoutePush = { id: itemMenu.id, icon: '', path: itemMenu.path, component: AlertReports }
                if (!routes.find(itemRoute => (
                    itemRoute.id === itemRoutePush.id
                ))) {
                    routes.push(itemRoutePush)
                }
            }
        })

        const renderMenu = (
            <Menu
                anchorEl={menuProfile}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={isMenuOpen}
                onClose={this.handleClose}
            >
                <MenuItem onClick={this.handleClickDialogAccount}>
                    <ListItemIcon className={classes.icon}>
                        <AccountOutline />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.primary }} inset primary="Mi Cuenta" />
                </MenuItem>
                <MenuItem onClick={() => this.handleLoguot(dataSignUp)}>
                    <ListItemIcon className={classes.icon}>
                        <Logout />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.primary }} inset primary="Cerrar SesiÃ³n" />
                </MenuItem>
            </Menu>
        );

        return (

            <div className={classes.root}>
                <CssBaseline />
                {/** Code Header */}
                <AppBar
                    position="fixed"
                    className={classNames(
                        classes.appBar,
                        open && classes.appBarShift
                    )}
                >
                    <Toolbar
                        disableGutters={!open}
                        className={classNames(classes.toolbar, 'wrapper-toolbar')}
                    >
                        <IconButton

                            aria-label="Open drawer"
                            onClick={this.handleDrawerOpen}
                            className={classNames(
                                classes.menuButton,
                                open && classes.menuButtonHidden
                            )}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Avatar color="secondary" className={classes.avatar}>
                            {toolbar.title === DASHBOARD ? <DashboardIcon />
                                : toolbar.title === OPERACIONAL ? <OperationIcon />
                                    : toolbar.title === SEGURIDAD ? <SecurityIcon />
                                        : toolbar.title === REPORTES ? <AssignmentIcon />
                                            : toolbar.title === SISTEMA ? <TableSettings />
                                                : toolbar.title === ALERTREPORTS ? <NotificationsIcon />
                                                    : <LocalShippingIcon />}
                        </Avatar>

                        <Typography
                            component="h1"
                            variant="h6"
                            color="inherit"
                            noWrap
                            className={classNames(classes.title, 'title-dash')}
                        >
                            {toolbar.title}
                        </Typography>
                        <div className={classes.grow} />
                        <div className={classes.search}>
                            <div className={classes.searchIcon}>
                                <SearchIcon />
                            </div>
                            <InputBase
                                placeholder="Buscar..."
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput
                                }}
                            />
                            <Popper open={this.state.openTextSearch} anchorEl={this.state.anchorEl} keepMounted transition >
                                {({ TransitionProps, placement }) => (
                                    <Grow
                                        {...TransitionProps}
                                        style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom', width: 250 }}
                                    >
                                        <Paper id="menu-list-grow">
                                            <ClickAwayListener onClickAway={this.handleCloseTextSearch}>
                                                <List>
                                                    {menuDown.map(item =>
                                                        <ListItem key={item.id} component={Link} to={this.props.match.url + '/' + item.url} onClick={this.handleCloseTextSearch} button>
                                                            <ListItemText classes={{ primary: 'black' }}>{item.label}</ListItemText>
                                                        </ListItem>
                                                    )}
                                                </List>
                                            </ClickAwayListener>
                                        </Paper>
                                    </Grow>
                                )}
                            </Popper>
                        </div>
                        <div className={classes.rootButtonAppBar}>
                            <IconButton
                                aria-haspopup="true"
                                color="inherit"
                                onClick={this.handleClickOpenMenuAlerts}
                            >
                                {isNotifications ? (<Badge
                                    variant="dot" color="error"
                                    classes={{ badge: classes.pulse }} >
                                    <NotificationsIcon />
                                </Badge>) : (<NotificationsIcon />)}
                            </IconButton>


                            <IconButton
                                aria-haspopup="true"
                                onClick={this.handleMenuOpenProfile}
                                color="secondary"
                                className={classes.iconButtonAvatar}
                            >
                                <Avatar className={classes.avatar}>I</Avatar>
                            </IconButton>
                        </div>
                    </Toolbar>
                </AppBar>
                {renderMenu}
                {openMenuAlerts ?
                    (<Alertas
                        anchorEl={openMenuAlerts}
                        handleCloseMenuAlerts={this.handleCloseMenuAlerts}
                    />)
                    : null}
                {openDialogAccount ? (
                    <Account
                        idUser={dataSignUp.idUser}
                        open={openDialogAccount}
                        title="Cuenta || ConfiguraciÃ³n"
                        handleClose={this.handleCloseDialogAccount}
                        backAfterInsert={this.backAfterInsert}
                    />)
                    : null}

                { /* Code Sidebar */}
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: classNames(
                            classes.drawerPaper,
                            'wrapper-sidebar',
                            !open && classes.drawerPaperClose
                        )
                    }}
                    open={open}
                >
                    <div className={classes.toolbarIcon}>
                        <img src={Logo} alt="Avatar" className={classes.logo} />
                        <IconButton className={classes.iconButtonChev} onClick={this.handleDrawerClose}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <List>
                        {menuUp.map(itemMenu => {
                            return (
                                <div key={itemMenu.id}>
                                    <ListItem component={itemMenu.module === REPORTES ? null : itemMenu.module === GESTION_VEHICULOS ? null : Link}
                                        to={this.props.match.url + '/' + itemMenu.path}
                                        button
                                        onClick={itemMenu.module === REPORTES || itemMenu.module === GESTION_VEHICULOS ? () => this.handleClickExpandedMenu(itemMenu.module) : null}>
                                        <ListItemIcon >
                                            {itemMenu.module === DASHBOARD ? <DashboardIcon className={classes.drawerIcon} />
                                                : itemMenu.module === REPORTES
                                                    ? <AssignmentIcon className={classes.drawerIcon} />
                                                    : <LocalShippingIcon className={classes.drawerIcon} />}
                                        </ListItemIcon>
                                        <ListItemText classes={{ primary: classes.listItemText }}>{itemMenu.module}</ListItemText>
                                        {itemMenu.module === REPORTES || itemMenu.module === GESTION_VEHICULOS ?
                                            (<IconButton
                                                className={classes.iconButtonChev}
                                                onClick={(e) => this.handleClickExpandedMenu(itemMenu.module)}
                                                aria-expanded={itemMenu.module === REPORTES ? this.state.openMenu : itemMenu.module === GESTION_VEHICULOS ? this.state.openMenuGestionVehicle : null}
                                                aria-label="Show more"
                                            >
                                                <ExpandMoreIcon />
                                            </IconButton>)
                                            : null}
                                    </ListItem>
                                    {itemMenu.items ? (
                                        <Collapse in={itemMenu.module === REPORTES ? this.state.openMenu : itemMenu.module === GESTION_VEHICULOS ? this.state.openMenuGestionVehicle : null} timeout="auto" >
                                            <List component="div" disablePadding>
                                                {itemMenu.items.map(item => (
                                                    <ListItem key={item.id} component={Link} to={this.props.match.url + '/' + itemMenu.path + '/' + item.path} button className={classes.nested}>
                                                        <ListItemText inset classes={{ primary: classes.listItemTextSubList }}>{item.itemModule}</ListItemText>
                                                    </ListItem>
                                                ))}
                                            </List>
                                        </Collapse>
                                    ) : null}</div>
                            )
                        })}
                    </List>
                    <Divider />
                    <List>
                        <ListSubheader className={classes.drawerSubheader} inset>
                            ConfiguraciÃ³n
                        </ListSubheader>
                        {menuDown.map(itemMenu => {

                            return <ListItem key={itemMenu.id} component={Link} to={this.props.match.url + '/' + itemMenu.path} button>
                                <ListItemIcon >
                                    {itemMenu.module === SEGURIDAD ? <SecurityIcon className={classes.drawerIcon} />
                                        : itemMenu.module === OPERACIONAL ? <OperationIcon className={classes.drawerIcon} /> : <TableSettings className={classes.drawerIcon} />}

                                </ListItemIcon>
                                <ListItemText classes={{ primary: classes.listItemText }}>{itemMenu.module}</ListItemText>
                            </ListItem>
                        })}
                    </List>
                </Drawer>

                { /** Code Main */}
                <main className={classes.main}>
                    {
                        this.props.location.pathname == '/layout' ? <DashBoard /> : (routes.map((route, index) => (

                            <Route
                                key={index}
                                path={this.props.match.url + '/' + route.path}
                                exact={route.exact}
                                component={route.component}
                            />
                        )))
                    }
                    {this.state.snackbarOpen ? (<SnackbarInfo
                        open={this.state.snackbarOpen}
                        onClose={this.handleCloseSnackbar}
                        variant={this.state.snackbarVariant}
                        message={this.state.snackbarMessage}
                    />) : null}
                </main>
            </div>

        );
    }
}

Layout.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    logoutUser: PropTypes.func.isRequired,
    setToolbar: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    toolbar: state.toolbar
})

export default compose(withStyles(styles, { withTheme: true }),
    connect(mapStateToProps, { logoutUser, setToolbar }), withRouter)
    (Layout);