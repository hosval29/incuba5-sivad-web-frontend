export const menuData = [
    {
        id: 1,
        label: 'dashboard',
        url: 'dashboard',
    },
    {
        id: 2,
        label: 'propietarios',
        url: 'configuracion/propietarios'
    },
    {
        id: 3,
        label: 'conductores',
        url: 'configuracion/conductores',
    },
    {
        id: 4,
        label: 'vehiculos',
        url: 'configuracion/vehiculos',
    },
    {
        id: 5,
        label: 'reportes',
        url: 'inspecciones',
    },
    {
        id: 6,
        label: 'operacional',
        url: 'operacional',
    },
    {
        id: 7,
        label: 'seguridad',
        url: 'seguridad',
    },
]