//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { withRouter } from 'react-router'
import { compose } from 'recompose';
import { connect } from 'react-redux'

//Material Ui
import {
    withStyles,
    List,
    ListItem,
    ListItemText,
    Avatar,
    Typography,
    Menu,
    ListSubheader,
    Divider,
    Tooltip,
    Fab
} from '@material-ui/core';

//Icons
// import ImageIcon from '@material-ui/icons/Image';
import { DatabaseRemove } from 'mdi-material-ui'
import ListAlt from '@material-ui/icons/ListAlt';
import http from '../../../utils/http';
import AssignmentIcon from '@material-ui/icons/Assignment';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';

//Styles
const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
    text: {
        paddingTop: theme.spacing.unit * 2,
        paddingLeft: theme.spacing.unit * 2,
        paddingRight: theme.spacing.unit * 2,
    },
    paper: {
        paddingBottom: 50,
    },
    list: {
        marginBottom: theme.spacing.unit * 2,
    },
    subHeader: {
        backgroundColor: theme.palette.background.paper,
    },
    bodyTitle: {

        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    listItemText: {
        color: '#2eac8d'
    },
    listItemTextSecond: {
        color: theme.palette.primary.main
    },
    fab: {
        marginRight: theme.spacing.unit,
        //margin: theme.spacing.unit,
        color: theme.palette.background.paper
    },
    listSection: {
        backgroundColor: 'inherit',
    },
    ul: {
        backgroundColor: 'inherit',
        padding: 0,
    },
});

const ITEM_HEIGHT = 67;

/* const alerts = [
    { id: 1, type: 'Soat', date: '2019/05/14', driver: 'Conductor Uno', plateVehicle: 'AAA-111', color: '#ff5c6c' },
    { id: 2, type: 'Licencia de Conducción', date: '2019/05/14', driver: 'Conductor Dos', plateVehicle: 'AAA-112', color: '#24d2b5' },
    { id: 3, type: 'Ejemplo Uno', date: '2019/05/15', driver: 'Conductor Tres', plateVehicle: 'AAA-113', color: '#20aee3' },
    { id: 4, type: 'Ejemplo Dos', date: '2019/05/115', driver: 'Conductor Cuatro', plateVehicle: 'AAA-114', color: '#ff5c6c' },
    { id: 5, type: 'Ejemplo Tres', date: '2019/05/16', driver: 'Conductor Cinco', plateVehicle: 'AAA-115', color: '#6772e5' },
    { id: 6, type: 'Ejemplo Cuatro', date: '2019/05/17', driver: 'Conductor Seis', plateVehicle: 'AAA-116', color: '#ff5c6c' },
] */

class Alertas extends Component {

    state = {
        dataNotificationsVehicles: [],
        dataNotificationsInspections: [],
        errors: {}
    }
    componentWillMount = () => {
        this.getData()
        this.getDataAlertsInspectionItemBad()
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getData = () => {
        this._isMounted = true;
        http
            .get('/api/notifications')
            .then(result => {

                if (this._isMounted) {
                    if (result.data.data) this.setState({ dataNotificationsVehicles: result.data.data })
                }
            })
            .catch(err => {
            console.log("OUTPUT: Alertas -> getData -> err", err)
                switch (err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: {} })
                        break;
                }
            });
    };

    getDataAlertsInspectionItemBad = () => {
        this._isMounted = true;
        http
            .get('/api/alertsinspectionitembad')
            .then(result => {

                if (this._isMounted) {
                    if (result.data.data) {
                        this.setState({ dataNotificationsInspections: result.data.data })
                    }
                }
            })
            .catch(err => {
                switch(err.response.status) {
                    case 400:
                        this.setState({ errors: err.response.data })
                        break;
                    case 404:
                        this.setState({ errors: err.response.data })
                        break;
                    case 500:
                        this.setState({ errors: err.response.data })
                        break;
                    default:
                        this.setState({ errors: [] })
                        break;
                }
            });
    };

    handleClose = () => {
        this.props.handleCloseMenuAlerts();
    };

    render() {
        const { classes, anchorEl } = this.props;
        const { dataNotificationsInspections, dataNotificationsVehicles } = this.state
        const open = Boolean(anchorEl);
        return (
            <Menu
                anchorEl={anchorEl}
                open={open}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                onClose={this.handleClose}
                title="Listado de Alertas"
                PaperProps={{

                    style: {
                        maxHeight: ITEM_HEIGHT * 5,
                        width: 320
                    },
                }}
            >

                <div>
                    <div className={classes.bodyTitle}>
                        <Typography color="textPrimary" className={classes.text} variant="h6" gutterBottom>
                            Listado de Notificaciones
                        </Typography>
                        <Tooltip
                            title="Ver Listado Notificaciones"
                            aria-label="Ver Listado Notificaciones"
                            placement="bottom"
                        >
                            <Fab
                                color="secondary"
                                className={classes.fab}
                                size="small"
                                component={Link}
                                to={this.props.match.url + '/' + 'notificaciones'}
                                onClick={this.handleClose}
                            >
                                <ListAlt />
                            </Fab>
                        </Tooltip>
                    </div>
                </div>
                <Divider />
                <List className={classes.root} subheader={<li />}>
                    {[0, 1].map(type => (
                        <li key={type} className={classes.listSection}>
                            <ul className={classes.ul}>
                                <ListSubheader>{type === 0 ? 'Vehículos' : 'Inspecciones'}</ListSubheader>
                                {type === 0 ? dataNotificationsVehicles.length !== 0 ? dataNotificationsVehicles.map(({ id, type, createdAt, data, color }) => (
                                    <div key={id}>
                                        <ListItem button>
                                            <Avatar style={{ color: 'white', backgroundColor: '#2eac8d' }}>
                                                <LocalShippingIcon />
                                            </Avatar>
                                            <ListItemText classes={{ primary: classes.listItemText }} primary={data.typeAlert} secondary={
                                                <React.Fragment>
                                                    <Typography component="span" className={classes.inline} color="textPrimary">
                                                        {data.dateExpiration}
                                                    </Typography>
                                                    <br />
                                                    <div style={{ display: 'flex', alignItems: 'left' }}>
                                                        <Typography component="span" className={classes.inline} color="textPrimary">
                                                            {'Placa Vehículo - '}
                                                        </Typography>
                                                        <Typography style={{ marginLeft: 5 }} variant="subtitle2" className={classes.inline} color="secondary">
                                                            {data.plateVehicle}
                                                        </Typography>
                                                    </div>
                                                </React.Fragment>} />
                                        </ListItem>
                                        <Divider />
                                    </div>
                                )) : <div style={{ display: 'flex', justifyContent: 'center' }}> <DatabaseRemove color="secondary" /> </div> : dataNotificationsInspections.length !== 0 ? dataNotificationsInspections.map(({ id, type, createdAt, data, color, dateRegister }) => (
                                    <div key={id}>
                                        <ListItem button>
                                            <Avatar style={{ color: 'white', backgroundColor: '#00587c' }}>
                                                <AssignmentIcon />
                                            </Avatar>
                                            <ListItemText classes={{ primary: classes.listItemTextSecond }} primary={data.nameFragment} secondary={
                                                <React.Fragment>
                                                    <Typography component="span" className={classes.inline} color="primary">
                                                        {data.nameToggle}
                                                    </Typography>
                                                    <br />
                                                    <Typography component="span" className={classes.inline} color="primary">
                                                        {dateRegister}
                                                    </Typography>
                                                    <br />
                                                    <div style={{ display: 'flex', alignItems: 'left' }}>
                                                        <Typography component="span" className={classes.inline} color="textPrimary">
                                                            {'Placa Vehículo - '}
                                                        </Typography>
                                                        <Typography style={{ marginLeft: 5 }} variant="subtitle2" className={classes.inline} color="primary">
                                                            {data.plateVehicle}
                                                        </Typography>
                                                    </div> 
                                                </React.Fragment>} />
                                        </ListItem>
                                        <Divider />
                                    </div>
                                )) : <div style={{ display: 'flex', justifyContent: 'center' }}> <DatabaseRemove color="secondary" /> </div>}
                            </ul>
                        </li>
                    ))}
                </List>
            </Menu>
        );
    }
}

Alertas.propTypes = {
    classes: PropTypes.object.isRequired,
    handleCloseMenuAlerts: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
})

export default compose(withStyles(styles, { withTheme: true }),
    connect(mapStateToProps), withRouter)(Alertas);
