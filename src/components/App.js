// import fs from 'fs'
import React from 'react';
import { Route, Router, Switch } from 'react-router-dom'

// import logo from '../logo.svg';
//import './App.css';
import { Provider } from 'react-redux'
import store from '../store'

import jwt_decode from "jwt-decode";
import setAuthToken from "../utils/setAuthToken";
import { setCurrentUser, logoutUser } from "../store/actions/authActions";

import Layout from './Layout'
import Signin from './Security/Signin'
import Error404 from './Error/404'

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import { createBrowserHistory } from 'history';
import PrivateRoute from '../utils/private-route/PrivateRoute'

const history = createBrowserHistory()


// Check for token to keep user logged in
if (localStorage.jwtToken) {
  // Set auth token header auth
  const token = localStorage.jwtToken;
  setAuthToken(token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(token);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // Redirect to login
    window.location.href = "./";
  }
}


const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2096c6',
      dark: '#00587c'
    },
    secondary: {
      light: '#68debd',
      main: '#2eac8d',
      dark: '#005337'
    },
    error: {
      dark: '#b00020',
      main: '#b00020',
    },
    background: {
      default: '#F0F0F0',
      paper: '#FFFFFF'
    },
    text: {
      primary: '#000000',
      secondary: '#404e67',
      disabled: '#212121',
      hint: '#404e67'
    }
  },
  typography: {
    useNextVariants: true,
    fontFamily: ['"Oswald"', 'sans-serif'].join(',')
  }
});


function App() {
  
  return (
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <Router history={history}>
          <div>
            <Switch>
              <Route path="/" exact component={Signin} />
              <PrivateRoute path="/layout" component={Layout} />
              <Route component={Error404} />
            </Switch>
          </div>
        </Router>
      </MuiThemeProvider>
    </Provider>
  );
}

export default App;
